//
//	ModelQrCodeData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelQrCodeData : NSObject, NSCoding{

	var address : String!
	var bloodGroup : String!
	var bloodPressure : String!
	var companyName : String!
	var createdAt : String!
	var dateOfBirth : String!
	var diabetes : String!
	var drugAllergy : String!
	var emergName : String!
	var emergName2 : String!
	var emergName3 : String!
	var emergPhone : String!
	var emergPhone2 : String!
	var emergPhone3 : String!
	var emergRelationship : String!
	var emergRelationship2 : String!
	var emergRelationship3 : String!
	var heartProblem : String!
	var height : String!
	var id : String!
	var isHealthInsurance : String!
	var isOrganDonor : String!
	var medication : String!
	var name : String!
	var otherAllergy : String!
	var otherDetails : String!
	var physicianName : String!
	var physicianPhone : String!
	var policyNumber : String!
	var updatedAt : String!
	var userId : String!
	var weight : String!
    var randomid : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		address = dictionary["address"] as? String
		bloodGroup = dictionary["blood_group"] as? String
		bloodPressure = dictionary["blood_pressure"] as? String
		companyName = dictionary["company_name"] as? String
		createdAt = dictionary["created_at"] as? String
		dateOfBirth = dictionary["date_of_birth"] as? String
		diabetes = dictionary["diabetes"] as? String
		drugAllergy = dictionary["drug_allergy"] as? String
		emergName = dictionary["emerg_name"] as? String
		emergName2 = dictionary["emerg_name_2"] as? String
		emergName3 = dictionary["emerg_name_3"] as? String
		emergPhone = dictionary["emerg_phone"] as? String
		emergPhone2 = dictionary["emerg_phone_2"] as? String
		emergPhone3 = dictionary["emerg_phone_3"] as? String
		emergRelationship = dictionary["emerg_relationship"] as? String
		emergRelationship2 = dictionary["emerg_relationship_2"] as? String
		emergRelationship3 = dictionary["emerg_relationship_3"] as? String
		heartProblem = dictionary["heart_problem"] as? String
		height = dictionary["height"] as? String
		id = dictionary["id"] as? String
		isHealthInsurance = dictionary["is_health_insurance"] as? String
		isOrganDonor = dictionary["is_organ_donor"] as? String
		medication = dictionary["medication"] as? String
		name = dictionary["name"] as? String
		otherAllergy = dictionary["other_allergy"] as? String
		otherDetails = dictionary["other_details"] as? String
		physicianName = dictionary["physician_name"] as? String
		physicianPhone = dictionary["physician_phone"] as? String
		policyNumber = dictionary["policy_number"] as? String
		updatedAt = dictionary["updated_at"] as? String
		userId = dictionary["user_id"] as? String
		weight = dictionary["weight"] as? String
        randomid = dictionary["random_id"] as? String
	}

    
    override init() {
        super.init()
        
        address = ""
        bloodGroup = ""
        bloodPressure = ""
        companyName = ""
        createdAt = ""
        dateOfBirth = ""
        diabetes = ""
        drugAllergy = ""
        emergName = ""
        emergName2 = ""
        emergName3 = ""
        emergPhone = ""
        emergPhone2 = ""
        emergPhone3 = ""
        emergRelationship = ""
        emergRelationship2 = ""
        emergRelationship3 = ""
        heartProblem = ""
        height = ""
        id = ""
        isHealthInsurance = ""
        isOrganDonor = ""
        medication = ""
        name = ""
        otherAllergy = ""
        otherDetails = ""
        physicianName = ""
        physicianPhone = ""
        policyNumber = ""
        updatedAt = ""
        userId = ""
        weight = ""
        randomid = ""
        
    }

    
    
	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if bloodGroup != nil{
			dictionary["blood_group"] = bloodGroup
		}
		if bloodPressure != nil{
			dictionary["blood_pressure"] = bloodPressure
		}
		if companyName != nil{
			dictionary["company_name"] = companyName
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if dateOfBirth != nil{
			dictionary["date_of_birth"] = dateOfBirth
		}
		if diabetes != nil{
			dictionary["diabetes"] = diabetes
		}
		if drugAllergy != nil{
			dictionary["drug_allergy"] = drugAllergy
		}
		if emergName != nil{
			dictionary["emerg_name"] = emergName
		}
		if emergName2 != nil{
			dictionary["emerg_name_2"] = emergName2
		}
		if emergName3 != nil{
			dictionary["emerg_name_3"] = emergName3
		}
		if emergPhone != nil{
			dictionary["emerg_phone"] = emergPhone
		}
		if emergPhone2 != nil{
			dictionary["emerg_phone_2"] = emergPhone2
		}
		if emergPhone3 != nil{
			dictionary["emerg_phone_3"] = emergPhone3
		}
		if emergRelationship != nil{
			dictionary["emerg_relationship"] = emergRelationship
		}
		if emergRelationship2 != nil{
			dictionary["emerg_relationship_2"] = emergRelationship2
		}
		if emergRelationship3 != nil{
			dictionary["emerg_relationship_3"] = emergRelationship3
		}
		if heartProblem != nil{
			dictionary["heart_problem"] = heartProblem
		}
		if height != nil{
			dictionary["height"] = height
		}
		if id != nil{
			dictionary["id"] = id
		}
		if isHealthInsurance != nil{
			dictionary["is_health_insurance"] = isHealthInsurance
		}
		if isOrganDonor != nil{
			dictionary["is_organ_donor"] = isOrganDonor
		}
		if medication != nil{
			dictionary["medication"] = medication
		}
		if name != nil{
			dictionary["name"] = name
		}
		if otherAllergy != nil{
			dictionary["other_allergy"] = otherAllergy
		}
		if otherDetails != nil{
			dictionary["other_details"] = otherDetails
		}
		if physicianName != nil{
			dictionary["physician_name"] = physicianName
		}
		if physicianPhone != nil{
			dictionary["physician_phone"] = physicianPhone
		}
		if policyNumber != nil{
			dictionary["policy_number"] = policyNumber
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if weight != nil{
			dictionary["weight"] = weight
		}
        if randomid != nil{
            dictionary["random_id"] = randomid
        }
         
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         bloodGroup = aDecoder.decodeObject(forKey: "blood_group") as? String
         bloodPressure = aDecoder.decodeObject(forKey: "blood_pressure") as? String
         companyName = aDecoder.decodeObject(forKey: "company_name") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         dateOfBirth = aDecoder.decodeObject(forKey: "date_of_birth") as? String
         diabetes = aDecoder.decodeObject(forKey: "diabetes") as? String
         drugAllergy = aDecoder.decodeObject(forKey: "drug_allergy") as? String
         emergName = aDecoder.decodeObject(forKey: "emerg_name") as? String
         emergName2 = aDecoder.decodeObject(forKey: "emerg_name_2") as? String
         emergName3 = aDecoder.decodeObject(forKey: "emerg_name_3") as? String
         emergPhone = aDecoder.decodeObject(forKey: "emerg_phone") as? String
         emergPhone2 = aDecoder.decodeObject(forKey: "emerg_phone_2") as? String
         emergPhone3 = aDecoder.decodeObject(forKey: "emerg_phone_3") as? String
         emergRelationship = aDecoder.decodeObject(forKey: "emerg_relationship") as? String
         emergRelationship2 = aDecoder.decodeObject(forKey: "emerg_relationship_2") as? String
         emergRelationship3 = aDecoder.decodeObject(forKey: "emerg_relationship_3") as? String
         heartProblem = aDecoder.decodeObject(forKey: "heart_problem") as? String
         height = aDecoder.decodeObject(forKey: "height") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         isHealthInsurance = aDecoder.decodeObject(forKey: "is_health_insurance") as? String
         isOrganDonor = aDecoder.decodeObject(forKey: "is_organ_donor") as? String
         medication = aDecoder.decodeObject(forKey: "medication") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         otherAllergy = aDecoder.decodeObject(forKey: "other_allergy") as? String
         otherDetails = aDecoder.decodeObject(forKey: "other_details") as? String
         physicianName = aDecoder.decodeObject(forKey: "physician_name") as? String
         physicianPhone = aDecoder.decodeObject(forKey: "physician_phone") as? String
         policyNumber = aDecoder.decodeObject(forKey: "policy_number") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
         userId = aDecoder.decodeObject(forKey: "user_id") as? String
         weight = aDecoder.decodeObject(forKey: "weight") as? String
         randomid = aDecoder.decodeObject(forKey: "random_id") as? String

	}
    

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if bloodGroup != nil{
			aCoder.encode(bloodGroup, forKey: "blood_group")
		}
		if bloodPressure != nil{
			aCoder.encode(bloodPressure, forKey: "blood_pressure")
		}
		if companyName != nil{
			aCoder.encode(companyName, forKey: "company_name")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if dateOfBirth != nil{
			aCoder.encode(dateOfBirth, forKey: "date_of_birth")
		}
		if diabetes != nil{
			aCoder.encode(diabetes, forKey: "diabetes")
		}
		if drugAllergy != nil{
			aCoder.encode(drugAllergy, forKey: "drug_allergy")
		}
		if emergName != nil{
			aCoder.encode(emergName, forKey: "emerg_name")
		}
		if emergName2 != nil{
			aCoder.encode(emergName2, forKey: "emerg_name_2")
		}
		if emergName3 != nil{
			aCoder.encode(emergName3, forKey: "emerg_name_3")
		}
		if emergPhone != nil{
			aCoder.encode(emergPhone, forKey: "emerg_phone")
		}
		if emergPhone2 != nil{
			aCoder.encode(emergPhone2, forKey: "emerg_phone_2")
		}
		if emergPhone3 != nil{
			aCoder.encode(emergPhone3, forKey: "emerg_phone_3")
		}
		if emergRelationship != nil{
			aCoder.encode(emergRelationship, forKey: "emerg_relationship")
		}
		if emergRelationship2 != nil{
			aCoder.encode(emergRelationship2, forKey: "emerg_relationship_2")
		}
		if emergRelationship3 != nil{
			aCoder.encode(emergRelationship3, forKey: "emerg_relationship_3")
		}
		if heartProblem != nil{
			aCoder.encode(heartProblem, forKey: "heart_problem")
		}
		if height != nil{
			aCoder.encode(height, forKey: "height")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isHealthInsurance != nil{
			aCoder.encode(isHealthInsurance, forKey: "is_health_insurance")
		}
		if isOrganDonor != nil{
			aCoder.encode(isOrganDonor, forKey: "is_organ_donor")
		}
		if medication != nil{
			aCoder.encode(medication, forKey: "medication")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if otherAllergy != nil{
			aCoder.encode(otherAllergy, forKey: "other_allergy")
		}
		if otherDetails != nil{
			aCoder.encode(otherDetails, forKey: "other_details")
		}
		if physicianName != nil{
			aCoder.encode(physicianName, forKey: "physician_name")
		}
		if physicianPhone != nil{
			aCoder.encode(physicianPhone, forKey: "physician_phone")
		}
		if policyNumber != nil{
			aCoder.encode(policyNumber, forKey: "policy_number")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if weight != nil{
			aCoder.encode(weight, forKey: "weight")
		}

        if randomid != nil{
            aCoder.encode(randomid, forKey: "random_id")
        }
        
	}
    
    
    func saveToUserDefaults() {
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        USERDEFAULTS.set(data, forKey: kModelUserQRData)
    }
    
    class func getUserDataFromUserDefaults() -> ModelQrCodeData {
        if let data = USERDEFAULTS.value(forKey: kModelUserQRData) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? ModelQrCodeData ?? ModelQrCodeData()
        }
        return ModelQrCodeData()
    }

}
