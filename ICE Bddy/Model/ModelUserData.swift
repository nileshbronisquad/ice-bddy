//
//	ModelUserData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelUserData : NSObject, NSCoding{

	var apiToken : String!
	var createdAt : String!
	var email : String!
	var id : String!
	var image : String!
	var mobile : String!
	var name : String!
	var rating : String!
	var status : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apiToken = dictionary["api_token"] as? String
		createdAt = dictionary["created_at"] as? String
		email = dictionary["email"] as? String
		id = dictionary["id"] as? String
		image = dictionary["image"] as? String
		mobile = dictionary["mobile"] as? String
		name = dictionary["name"] as? String
		rating = dictionary["rating"] as? String
		status = dictionary["status"] as? String
	}
    
    override init() {
        super.init()
        
        apiToken = ""
        createdAt = ""
        email = ""
        id = ""
        image = ""
        mobile = ""
        name = ""
        email = ""
        rating = ""
        status = ""
        
    }

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apiToken != nil{
			dictionary["api_token"] = apiToken
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if email != nil{
			dictionary["email"] = email
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if mobile != nil{
			dictionary["mobile"] = mobile
		}
		if name != nil{
			dictionary["name"] = name
		}
		if rating != nil{
			dictionary["rating"] = rating
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         apiToken = aDecoder.decodeObject(forKey: "api_token") as? String
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         rating = aDecoder.decodeObject(forKey: "rating") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if apiToken != nil{
			aCoder.encode(apiToken, forKey: "api_token")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if mobile != nil{
			aCoder.encode(mobile, forKey: "mobile")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}
    
    func saveToUserDefaults() {
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        USERDEFAULTS.set(data, forKey: kModelUserData)
    }
    
    class func getUserDataFromUserDefaults() -> ModelUserData {
        if let data = USERDEFAULTS.value(forKey: kModelUserData) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? ModelUserData ?? ModelUserData()
        }
        return ModelUserData()
    }

}
