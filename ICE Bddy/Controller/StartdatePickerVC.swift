//
//  StartdatePickerVC.swift
//  Glabal Praise
//
//  Created by wm-nilesh on 25/07/19.
//  Copyright © 2019 webmigrates-rushita. All rights reserved.
//

import UIKit

class StartdatePickerVC: UIViewController {

    @IBOutlet weak var theDatePicker: UIDatePicker!
    @IBOutlet weak var lblSelected: UILabel!
    var isFromDOB = false
    
    
    // MARK:- Variable Declaration
    var completionHandler: ((String)->())?
    var completionHandler1: ((String)->())?
    var completionHandler2: ((String)->())?
    var completionHandler3: ((String)->())?
    
    // MARK:- Create self View Controller object
    class func initVC() -> StartdatePickerVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "StartdatePickerVC") as! StartdatePickerVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theDatePicker.datePickerMode = .date
    }

    @IBAction func btnCancel1(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDone(_ sender: Any) {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MM-YYYY"
        
        let comp = theDatePicker.calendar.dateComponents([.day, .month, .year], from: theDatePicker.date)
        
        lblSelected.text = formatter.string(from: theDatePicker.date)
        
        if isFromDOB {
            
            let dateOfBirth = theDatePicker.date
            
            let today = NSDate()
            
            let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            
            let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
            
            if age.year! < 18 {
                
                let alert = UIAlertController(title: "Error", message: "You are not eligible", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                      switch action.style{
                      case .default:
                            print("default")

                      case .cancel:
                            print("cancel")

                      case .destructive:
                            print("destructive")


                }}))
                self.present(alert, animated: true, completion: nil)
            }
            else {
            
                if let block = completionHandler {
                    block(lblSelected.text!)
                }
                if let block = completionHandler1 {
                    block("\(comp.day!)")
                }
                if let block = completionHandler2 {
                    block("\(Utilities.getMonthString(month: comp.month!))")
                }
                if let block = completionHandler3 {
                    block("\(comp.year!)")
                }
                self.dismiss(animated: true, completion: nil)
            }
        }
        else {
            
            if let block = completionHandler {
                block(lblSelected.text!)
            }
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func btnCancel(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
}
