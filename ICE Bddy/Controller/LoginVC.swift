//
//  LoginVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import Toast_Swift
import FBSDKCoreKit
import FBSDKLoginKit




class LoginVC: UIViewController,UITextFieldDelegate {
    
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var appleView: UIView!
    @IBOutlet weak var faceBookView: UIView!
    @IBOutlet weak var googleView: UIView!
    @IBOutlet weak var lblAppName: UILabel!
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRememberMe: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var imgRememberMe: UIImageView!
    
    @IBOutlet weak var lblDontHaveAcc: UILabel!
    var isRememberMe : Bool = false
    
    var authProvider = ""
    var authID = ""
    var name = ""
    var email = ""
    
    
    
    private var socilalogin = SocialLoginManager()
    
    // MARK:- Create self View Controller object
    class func initVC() -> LoginVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
        self.socilalogin.viewController = self
        LoginManager().logOut()
    }
    
    // MARK:- UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword {
            textField.resignFirstResponder()
            btnSignIn_Click(UIButton())
        }
        
        return true
    }


    func initialConfig(){
        
        Utilities.dropShadow(view: mainView)
        scroll_View.layer.cornerRadius = 30
        scroll_View.contentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH, height: mainView.frame.size.height)
        
        loginView.roundCorners([.topLeft, .topRight], radius: 30.0, width: AppConstants.ScreenSize.SCREEN_WIDTH)
        
        Utilities.dropShadow(view: self.mainView)
        
        
        lblAppName.numberOfLines = 0
        lblAppName.text = "ICE Buddy"
//        A Friend In Case Of Emergency
        
        
        btnRememberMe.layer.masksToBounds = true
        btnRememberMe.layer.borderWidth = 2.0
        btnRememberMe.layer.borderColor = ColorConstants.GrayColor.cgColor
        
//        Don't Have an Account? Sign Up
        self.view.gestureRecognizers?.removeAll()
        
     

  
    }
    
    
    @IBAction func btnRememberMe_Click(_ sender: Any) {
        isRememberMe = !isRememberMe
        
        if isRememberMe {
            imgRememberMe.image = UIImage(named: "tick")
        }else{
            imgRememberMe.image = UIImage(named: "")
        }
    }
    
    @IBAction func btnForgotPassword_Click(_ sender: Any) {
        let vc = ForgotPasswordVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignIn_Click(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.callLoginAPI()

        }else{
            self.showToastMessage(message: msg)
        }
    }
    
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtEmail.text == "" { // Mobile
            msg = kEmailempty
        }
        else if txtEmail.text?.isValidEmail() ?? false == false {
            msg = kInvalidEmail
        }
        else if txtPassword.text == "" { // Mobile
            msg = kpassword
        }
        return msg
    }
    
    
    @IBAction func btnSignUp_Click(_ sender: Any) {
        let vc = SignUpVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnAppleSignIn_Click(_ sender: Any) {
        self.socilalogin.loginWithApple()
    }
    
    @IBAction func btnFaceBookSignIn_Click(_ sender: Any) {
        self.socilalogin.loginWithFacebook()
    }
    
    @IBAction func btnGoogleSignIn_Click(_ sender: Any) {
        self.socilalogin.loginWithGoogle()
    }
     
}

//MARK:- WebService Call Method
@available(iOS 10.0, *)
extension LoginVC {
    
    func callLoginAPI() {
        self.view.endEditing(true)
        
        let param : NSMutableDictionary = ["email":self.txtEmail.text!,
                                           "password":self.txtPassword.text!,
                                           "device_token":Constant.kDeviceToken,
                                           "device_type":"1"]
        
        
        HttpRequestManager.requestWithDefaultCalling(service: kLoginProfile_URL, parameters: param) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callLoginAPI()
                })
                return
            }
            else {
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    
                    let data = response?.value(forKey: "user_data") as! NSMutableDictionary
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: data as! [String : Any])
                    model.saveToUserDefaults()
                    
                    let QrData = response?.value(forKey: "qr_code_data") as! NSMutableDictionary
                    
                    let modelQR : ModelQrCodeData = ModelQrCodeData(fromDictionary: QrData as! [String : Any])
                    
                    modelQR.saveToUserDefaults()
                        
                    
                    let vc = HomeVC.initVC()
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else  if response?.value(forKey: kStatus) as? String ?? "" == kStatusNotVerify {
                    
                    let vc = otpVC.initVC()
                    vc.email = self.txtEmail.text!
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                    
                }
                else  if response?.value(forKey: kStatus) as? String ?? "" == kStatusFail {
                    
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                    
                }
                
            }
        }
    }
    
    func callLogin_SocialAPI(imageName:String = "", firstname : String,lastname : String,email : String, authProvider : String, socialuserid : String,mobile:String) {
        
       
        let param : NSMutableDictionary = [
                                           "firstName": firstname,
                                           "lastName": lastname,
                                           "email": email,
                                           "profile_image": imageName,
                                           "social_id": socialuserid,
                                           "auth_provider": authProvider,
                                           "mobile": mobile]
        
        HttpRequestManager.requestWithDefaultCalling(service: kSocialLogin_URL, parameters: param, showLoader : true) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callLogin_SocialAPI(firstname: firstname, lastname: lastname, email: email, authProvider: authProvider, socialuserid: socialuserid, mobile: mobile)
                })
                return
            }
            else {
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    let data = response?.value(forKey: "data") as! NSMutableDictionary
                    let model : ModelUserData = ModelUserData(fromDictionary: data as! [String : Any])
                    model.saveToUserDefaults()
                    
                    
                }
                else {
                    showMessage(response?.value(forKey: kMessage) as? String ?? "", themeStyle: .error)
                }
            }
        }
    }
    
}




extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat ,width : CGFloat,height : CGFloat = AppConstants.ScreenSize.SCREEN_HEIGHT) {
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        let path = UIBezierPath(roundedRect: frame, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    
}

