//
//  ProMemberVC1.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import SideMenu

class ProMemberVC1: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var proMenberView: UIView!
    @IBOutlet weak var scroll_View: UIScrollView!
    
    @IBOutlet weak var btnPromember: UIButton!
    
    @IBOutlet weak var btnCoffee: UIButton!
    @IBOutlet weak var btnLargeCoffee: UIButton!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ProMemberVC1 {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ProMemberVC1") as! ProMemberVC1
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        proMenberView.roundCorners([.topLeft, .topRight], radius: 30.0, width: AppConstants.ScreenSize.SCREEN_WIDTH)
               Utilities.dropShadow(view: mainView)
        scroll_View.layer.cornerRadius = 30
        scroll_View.contentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH, height: mainView.frame.size.height)
    }

    @IBAction func btnMenu_Click(_ sender: Any) {
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
    }
    @IBAction func btnPromember_Click(_ sender: Any) {
    
        btnPromember.setImage(UIImage(named: "checkIcon"), for: .normal)
        btnCoffee.setImage(UIImage(named: "unCheckIcon"), for: .normal)
        btnLargeCoffee.setImage(UIImage(named: "unCheckIcon"), for: .normal)
        
        
    }
    
    @IBAction func btnCoffee_Click(_ sender: Any) {
        
        btnPromember.setImage(UIImage(named: "unCheckIcon"), for: .normal)
        btnCoffee.setImage(UIImage(named: "checkIcon"), for: .normal)
        btnLargeCoffee.setImage(UIImage(named: "unCheckIcon"), for: .normal)
    }
    
    
    @IBAction func btnLargeCoffee_Click(_ sender: Any) {
        btnPromember.setImage(UIImage(named: "unCheckIcon"), for: .normal)
        btnCoffee.setImage(UIImage(named: "unCheckIcon"), for: .normal)
        btnLargeCoffee.setImage(UIImage(named: "checkIcon"), for: .normal)
        
        
    }
    
    
    @IBAction func btnBuyClick(_ sender: Any) {
    }
    
   

}
