//
//  QRInfoVC.swift
//  ICE Bddy
//
//  Created by iMac on 26/02/21.
//

import UIKit

class QRInfoVC: UIViewController {
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> QRInfoVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "QRInfoVC") as! QRInfoVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnCancel1(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
   

}
