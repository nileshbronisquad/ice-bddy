//
//  TermsVC.swift
//  BookFast_Rider
//
//  Created by iMac on 28/01/21.
//

import UIKit
import WebKit

class TermsVC: UIViewController {

    @IBOutlet weak var wk: WKWebView!
    @IBOutlet weak var lblTitle: UILabel!
    var Link = ""
    
    // MARK:- Create self View Controller object
    class func initVC() -> TermsVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.wk.load(URLRequest(url: URL(string: Link)!))
       
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
   

}
