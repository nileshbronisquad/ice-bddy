//
//  DownQrVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import SideMenu

class DownQrVC: UIViewController {
    
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tutorialView: UIView!
    @IBOutlet weak var qrView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var qrImageView: UIImageView!
    var id = ""
    
    // MARK:- Create self View Controller object
    class func initVC() -> DownQrVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "DownQrVC") as! DownQrVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let UDData = USERDEFAULTS.value(forKey: kModelUserQRData) as? Data,
            let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelQrCodeData {
                
            if model.id == nil {
                self.tutorialView.isHidden = false
                self.qrView.isHidden = true
                self.btnDownload.isHidden = true
            }
            else {
                self.tutorialView.isHidden = true
                let myString = Constant.ServerAPI.QR_CODE_URL + model.randomid
                
                let data = myString.data(using: String.Encoding.ascii)
                
                guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
                
                qrFilter.setValue(data, forKey: "inputMessage")
                let transform = CGAffineTransform(scaleX: 10.0, y: 10.0)
                
                guard let output = qrFilter.outputImage?.transformed(by: transform) else {return}
                
                self.qrImageView.image = self.convert(cmage: output)
            }
                     
        }
        else {
            self.tutorialView.isHidden = false
            self.qrView.isHidden = true
            self.btnDownload.isHidden = true
        }
      
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }


    @IBAction func btnMenu_Click(_ sender: Any) {
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true)
    }
    
    @IBAction func btnDownload_Click(_ sender: Any) {
        
        let result = self.qrView.takeScreenshot()
        self.view.makeToast("QR saved")
    }
}

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let image1 = image, true {
            UIImageWriteToSavedPhotosAlbum(image1, nil, nil, nil)
        }
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
