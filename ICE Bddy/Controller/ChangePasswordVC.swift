//
//  ChangePasswordVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import SideMenu

class ChangePasswordVC: UIViewController,UITextFieldDelegate {

    var isFromMenu : Bool = false
    var isSecureEntry :  Bool = false
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var mianViewTopConstraint: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ChangePasswordVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
    }

    func initialConfig(){
        
        txtOldPassword.isSecureTextEntry = true
        txtNewPassword.isSecureTextEntry = true
        txtConfirmPassword.isSecureTextEntry = true
        
        
        if isFromMenu{
            
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOpacity = 1
            
            shadowView.layer.shadowOffset = CGSize.zero
            shadowView.layer.shadowRadius = 5
            
            btnBack.setImage(UIImage(named: "menu"), for: .normal)
            
            oldPasswordView.isHidden = false
            shadowView.isHidden = false
        }else{
            Utilities.dropShadow(view: mainView)
            oldPasswordView.isHidden = true
            shadowView.isHidden = true
            btnBack.setImage(UIImage(named: "back"), for: .normal)

        }
    }

    @IBAction func btnEye_Click(_ sender: Any) {
        
        if (sender as AnyObject).tag == 100 {
        
            txtOldPassword.isSecureTextEntry = !txtOldPassword.isSecureTextEntry
        }
        if (sender as AnyObject).tag == 101 {
            txtNewPassword.isSecureTextEntry = !txtNewPassword.isSecureTextEntry
        }
        if (sender as AnyObject).tag == 102 {
            txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
        }
        
    }
    
    @IBAction func btnBack_Click(_ sender: Any) {
        if isFromMenu{
            present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
        }else{
    
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtOldPassword.text == "" { // Mobile
            msg = kpasswordOld
        }
        else if txtNewPassword.text == "" { // Mobile
            msg = kpassword
        }
        else if txtNewPassword.text!.count < 6 {
            msg = "Password should be 6 characters."
        }
        else if txtConfirmPassword.text == "" { // Mobile
            msg = kConpassword
        }
        else if txtNewPassword.text! != txtConfirmPassword.text! {
            msg = kConpasswordmatch
        }
        return msg
    }
    
    @IBAction func btnChangePassword_Click(_ sender: Any) {
        
        let msg = self.validation()
        if msg == "" {
            self.callChangePasswordAPI()

        }else{
            self.showToastMessage(message: msg)
        }
        
        
    }
    
    // MARK:- UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtOldPassword {
            txtNewPassword.becomeFirstResponder()
        } else if textField == txtNewPassword {
            txtConfirmPassword.becomeFirstResponder()
        } else if textField == txtConfirmPassword {
            textField.resignFirstResponder()
            btnChangePassword_Click(UIButton())
        }
        return true
    }
    
}


//MARK:- WebService Call Method
@available(iOS 10.0, *)
extension ChangePasswordVC {
    
    func callChangePasswordAPI() {
        self.view.endEditing(true)
        
        let param : NSMutableDictionary = ["old_password":self.txtOldPassword.text!,
                                           "password":self.txtNewPassword.text!,
                                           "confirm_password":txtConfirmPassword.text!,
                                           "user_id":ModelUserData.getUserDataFromUserDefaults().id ?? "",
                                           "api_token":ModelUserData.getUserDataFromUserDefaults().apiToken ?? ""]
        
        
        HttpRequestManager.requestWithDefaultCalling(service: kchangePassword_URL, parameters: param) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callChangePasswordAPI()
                })
                return
            }
            else {
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                 
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                    
                }
                
            }
        }
    }
    
}
