//
//  LeftMenuVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import StoreKit
import MessageUI

class LeftMenuVC: UIViewController,UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var table_view: UITableView!
    
    var selectedIndex : Int?   = 0
    
    let arrImage = ["","edit","key", "crown", "QRCodeScan", "download", "share", "star", "aboutUs", "support", ""]
    let arrOption = ["Home",
                     "Edit Detail",
                     "Change Password",
                     "Pro Member",
                     "Scan QR Code",
                     "Download QR Code",
                     "Share App",
                     "Rate App",
                     "About Us",
                     "Support",
                     "Logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_view.delegate = self
        table_view.dataSource = self
        self.table_view.register(UINib(nibName: "menuCell", bundle: nil), forCellReuseIdentifier: "menuCell")
        self.table_view.separatorColor = UIColor.clear
        
        self.table_view.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! menuCell
        cell.lblMenuOption.text = arrOption[indexPath.row]
        
        cell.menuImage.image = UIImage(named: arrImage[indexPath.row])
        cell.menuImage.backgroundColor = UIColor.clear
        
        if selectedIndex == indexPath.row {
            cell.lblMenuOption.textColor = Utilities.colorWithHexString(hex: "4897F6")
            Utilities.setImageColor(imageView: cell.menuImage, color: Utilities.colorWithHexString(hex: "4897F6"))
            cell.bgView.backgroundColor  = UIColor(named: "PrimaryColor")
        }else{
            cell.lblMenuOption.textColor = UIColor.black
            cell.bgView.backgroundColor  = Utilities.colorWithHexString(hex: "FFFFFF")
            Utilities.setImageColor(imageView: cell.menuImage, color: UIColor.black)
        }
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex  = indexPath.row
        
        if arrOption[indexPath.row] == "Home" {
            let vc = HomeVC.initVC()
            vc.isFromMenu = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if arrOption[indexPath.row] == "Edit Detail" {
            let vc = HomeVC.initVC()
            vc.isFromMenu = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if arrOption[indexPath.row] == "Change Password" {
            let vc = ChangePasswordVC.initVC()
            vc.isFromMenu = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if arrOption[indexPath.row] == "Pro Member" {
            let vc = ProMemberVC1.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }else if arrOption[indexPath.row] == "Scan QR Code" {
            let vc = ScanQrVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }else if arrOption[indexPath.row] == "Download QR Code" {
            let vc = DownQrVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if arrOption[indexPath.row] == "Share App" {
            
            self.ShareApp()
            
        }else if arrOption[indexPath.row] == "Rate App" {
            
            self.rateApp()
            
        }else if arrOption[indexPath.row] == "About Us" {
            
            let vc = AboutUSVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if arrOption[indexPath.row] == "Support" {
            
            if !MFMailComposeViewController.canSendMail() {
                print("Mail services are not available")
                return
            }
            else {
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                
                // Configure the fields of the interface.
                composeVC.setToRecipients(["support@iceb.app"])
                composeVC.setSubject("Message Subject")
                composeVC.setMessageBody("Message content.", isHTML: false)
                
                // Present the view controller modally.
                self.present(composeVC, animated: true, completion: nil)
            }
            
            
        }else if arrOption[indexPath.row] == "Logout" {
            self.callLogoutAPI()
        }
        
        tableView.reloadData()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.

        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
    //Mark: Share App
    func ShareApp() {
        //share
        let url = URL(string: "https://apps.apple.com/in/app/instagram/id389801252")
        
        let text = "Hey, this app helps to scan QR code. Download free app now."
        let link = "\(url!)" // application link
        
        let act = UIActivityViewController(activityItems: [text,link], applicationActivities: nil)
        act.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.postToFacebook]
        
        if let popoverController = act.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(act, animated: true, completion: nil)
    }
    
    func rateApp() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()

        } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + kAppId) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)

            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //Mark : Rate App
     func RateApp(){
        
        if !Reachabilities.isConnectedToNetwork(){
            self.view.makeToast("No Internet Connection")
        }else{
            
            if #available( iOS 10.3,*){
                SKStoreReviewController.requestReview()
            }
            else
            {
                let message = "If you enjoying this app,should you mind talking a moment to rate it? It won't take more than a minute. Thanks for your support!"
                
                let alert = UIAlertController(title: "Rate this app!", message: message, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Maybe Later", style: .default, handler: nil))
                
                alert.addAction(UIAlertAction(title: "No, thanks", style: .default, handler: nil))
                
                alert.addAction(UIAlertAction(title: "Sure, take me there", style: .cancel, handler: {
                    act in
                    
                    UserDefaults.standard.set(true, forKey: "ratingok")
                    
                    let link = "https://itunes.apple.com/us/app/\(kAppId)?action=write-review"
                    
                    let appstoreurl = URL(string: link)
                    
                    if UIApplication.shared.canOpenURL(appstoreurl!)
                    {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(appstoreurl!, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(appstoreurl!)
                        }
                    }
                    else
                    {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(appstoreurl!, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(appstoreurl!)
                        }
                    }
                    
                }))
                
                var messageMutableString = NSMutableAttributedString()
                messageMutableString = NSMutableAttributedString(string: message as String, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15)])
                
                alert.setValue(messageMutableString, forKey: "attributedMessage")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }

}

extension LeftMenuVC {
    
    func callLogoutAPI() {
        self.view.endEditing(true)
        
        USERDEFAULTS.removeObject(forKey: kModelUserData)
        USERDEFAULTS.removeObject(forKey: kModelUserQRData)
        USERDEFAULTS.synchronize()
        
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "LoginVC") as! LoginVC
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: false)
        kAppDelegate.window?.rootViewController = nav
        kAppDelegate.window?.makeKeyAndVisible()
        
//        let param : NSMutableDictionary = [ "user_id":ModelUserData.getUserDataFromUserDefaults().id ?? "",
//                                            "api_token":ModelUserData.getUserDataFromUserDefaults().apiToken ?? ""]
//
//
//        HttpRequestManager.requestWithDefaultCalling(service: klogout_URL, parameters: param) { (response, error) in
//            if error != nil
//            {
//                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
//                    hideBanner()
//                    self.callLogoutAPI()
//                })
//                return
//            }
//            else {
//                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
//
//                    USERDEFAULTS.removeObject(forKey: kModelUserData)
//                    USERDEFAULTS.removeObject(forKey: kModelUserQRData)
//                    USERDEFAULTS.synchronize()
//
//                    let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "LoginVC") as! LoginVC
//                    let nav = UINavigationController(rootViewController: vc)
//                    nav.setNavigationBarHidden(true, animated: false)
//                    kAppDelegate.window?.rootViewController = nav
//                    kAppDelegate.window?.makeKeyAndVisible()
//
//
//                }
//                else {
//                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
//
//                }
//
//            }
//        }
    }
    
}

