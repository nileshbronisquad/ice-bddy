//
//  otpVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import OTPFieldView


class otpVC: UIViewController,OTPFieldViewDelegate {
    
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var otpView: OTPFieldView!
    var email = ""
    var strOTP = String()
    var userData = NSMutableDictionary()
    
    // MARK:- Create self View Controller object
    class func initVC() -> otpVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "otpVC") as! otpVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.lblEmail.text = email
        self.otpView.fieldsCount = 4
        self.otpView.fieldBorderWidth = 2
        self.otpView.defaultBorderColor = ColorConstants.GrayColor
        self.otpView.filledBorderColor = #colorLiteral(red: 0.2823529412, green: 0.5921568627, blue: 0.9647058824, alpha: 1)
        self.otpView.fontColor = #colorLiteral(red: 0.2823529412, green: 0.5921568627, blue: 0.9647058824, alpha: 1)
        self.otpView.otpInputType = .numeric
        self.otpView.cursorColor = UIColor.black
        self.otpView.displayType = .underlinedBottom
        self.otpView.fieldSize = 40
        self.otpView.separatorSpace = 8
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.initializeUI()
    }


    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        print("OTPString: \(otp)")
        self.strOTP = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        if hasEnteredAll {
            //API CALLING
            self.callVerificationAPI()
        }
        else {
            return false
        }
        return false
    }

    @IBAction func btnBack_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfim_Click(_ sender: Any) {
        let vc = ChangePasswordVC.initVC()
        vc.isFromMenu = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnClickHere_Click(_ sender: Any) {
        self.view.endEditing(true)
       
    }
    
}

//MARK:- WebService Call Method
@available(iOS 10.0, *)
extension otpVC {
    
    func callVerificationAPI() {
        self.view.endEditing(true)
        
        let param : NSMutableDictionary = ["email":self.email,
              "otp": self.strOTP]
        
        
        HttpRequestManager.requestWithDefaultCalling(service: kVerification_URL, parameters: param) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callVerificationAPI()
                })
                return
            }
            else {
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: self.userData as! [String : Any])
                    model.saveToUserDefaults()
                    let vc = HomeVC.initVC()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
            }
        }
    }
    
}




