//
//  AboutUSVC.swift
//  ICE Bddy
//
//  Created by iMac on 26/02/21.
//

import UIKit
import SideMenu

class AboutUSVC: UIViewController {
    
    // MARK:- Create self View Controller object
    class func initVC() -> AboutUSVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack_Click(_ sender: Any) {
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true)
    }
   
}
