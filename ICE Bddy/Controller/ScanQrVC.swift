//
//  ScanQrVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import SideMenu
import MercariQRScanner
import AVFoundation

class ScanQrVC: UIViewController,AVCaptureMetadataOutputObjectsDelegate { //QRScannerViewDelegate
    
    
    var qrScannerView = QRScannerView()
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var scannigView: UIView!
    @IBOutlet weak var shadowView: UIView!
    var code = ""
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ScanQrVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ScanQrVC") as! ScanQrVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let id = ModelQrCodeData.getUserDataFromUserDefaults().randomid
        
        print(id)

        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 5
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        scannigView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
   
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//
//        self.customView.isHidden = true
//        //startScan()
//
//
//    }
    
    func failed() {
           let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
           ac.addAction(UIAlertAction(title: "OK", style: .default))
           present(ac, animated: true)
           captureSession = nil
       }

       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)

           if (captureSession?.isRunning == false) {
               captureSession.startRunning()
           }
       }

       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)

           if (captureSession?.isRunning == true) {
               captureSession.stopRunning()
           }
       }

       func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
           captureSession.stopRunning()

           if let metadataObject = metadataObjects.first {
               guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
               guard let stringValue = readableObject.stringValue else { return }
               AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
               found(code: stringValue)
           }

           dismiss(animated: true)
       }

    func found(code: String) {
        print(code)
        let vc = TermsVC.initVC()
        vc.Link = code
        self.navigationController?.pushViewController(vc, animated: true)
    }

       override var prefersStatusBarHidden: Bool {
           return true
       }

       override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
           return .portrait
       }


    @IBAction func btnMenu_Click(_ sender: Any) {
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true)
    }
    
    @IBAction func btnShare(_ sender: Any) {
        if let name = URL(string: self.code), !name.absoluteString.isEmpty {
          let objectsToShare = [name]
          let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
          self.present(activityVC, animated: true, completion: nil)
        } else {
          // show alert for not available
        }
    }
    
    @IBAction func btnCopy(_ sender: Any) {
        UIPasteboard.general.string = code
    }
    
    @IBAction func BTNSEARCH(_ sender: Any) {
        let vc = TermsVC.initVC()
        vc.Link = code
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        
        self.code = code
        
        let vc = TermsVC.initVC()
        vc.Link = code
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
//    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
//        print(error.localizedDescription)
//        startScan()
//    }
//
//
//    func startScan(){
//        qrScannerView = QRScannerView(frame: view.bounds)
//        qrScannerView.focusImage = UIImage(named: "QrScanFrame")
//        qrScannerView.focusImagePadding = 8.0
//        qrScannerView.animationDuration = 0.5
//        qrScannerView.configure(delegate: self,input: .init(isBlurEffectEnabled: true))
//        scannigView.addSubview(qrScannerView)
//        qrScannerView.startRunning()
//    }

}
