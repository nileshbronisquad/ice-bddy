//
//  PagerVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit



class PagerVC: UIViewController ,UIScrollViewDelegate {
    
    
    @IBOutlet weak var lblTutorial4: UILabel!
    @IBOutlet weak var lblTutorial3: UILabel!
    @IBOutlet weak var lblTutorial2: UILabel!
    @IBOutlet weak var lblTutorial1: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageController: UIPageControl!
    var index : Int = 0
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var imgHeader: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let x = 0 * self.scrollView.frame.size.width
        
        scrollView.delegate = self
        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
        scrollView.contentSize = CGSize(width: (self.view.frame.width * 4), height: self.view.frame.height)
        index = 0
        pageController.currentPage = index
    }

    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                self.lblTutorial1.font = self.lblTutorial1.font.withSize(22)
                self.lblTutorial2.font = self.lblTutorial2.font.withSize(22)
                self.lblTutorial3.font = self.lblTutorial3.font.withSize(22)
                self.lblTutorial4.font = self.lblTutorial4.font.withSize(22)
            }
            else if UIDevice.current.userInterfaceIdiom == .phone {
                self.lblTutorial1.font = self.lblTutorial1.font.withSize(18)
                self.lblTutorial2.font = self.lblTutorial2.font.withSize(18)
                self.lblTutorial3.font = self.lblTutorial3.font.withSize(18)
                self.lblTutorial4.font = self.lblTutorial4.font.withSize(18)
            }
            
            
            self.scrollView.contentSize = CGSize(width: (self.scrollView.frame.width * 4), height: self.scrollView.frame.height)
            super.viewDidAppear(animated)
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        let value = Int(pageNumber)
        pageController.currentPage = Int(pageNumber)
       
    }
    
    @IBAction func btnSkip_Click(_ sender: Any) {
        USERDEFAULTS.set("No", forKey:"isFirstTime")
        let vc = LoginVC.initVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNext_Click(_ sender: Any) {
        if index == 0 {
            let x = 1 * self.scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
            index += 1
            pageController.currentPage = index
            return
        }else if index == 1 {
            self.imgHeader.image = UIImage(named: "Group 1589")
            index += 1
            pageController.currentPage = index
            return
        }else if index == 2 {
            self.imgHeader.image = UIImage(named: "Group 2840")
            index += 1
            pageController.currentPage = index
            return
        }else if index == 3 {
            self.imgHeader.image = UIImage(named: "Group 2740")
            USERDEFAULTS.set("No", forKey:"isFirstTime")
            let vc = LoginVC.initVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
