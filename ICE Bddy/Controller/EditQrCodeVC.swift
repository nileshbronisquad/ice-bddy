//
//  EditQrCodeVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import AMColorPicker
import AVFoundation
import Toast_Swift
import Photos


class EditQrCodeVC: UIViewController,UIImagePickerControllerDelegate & UINavigationControllerDelegate, AMColorPickerDelegate{
   
    @IBOutlet var superViewBg: UIView!
    @IBOutlet weak var mainBG: UIView!
    let imageView = UIImageView()
    var pinchGesture  = UIPinchGestureRecognizer()
    var panGesture  = UIPanGestureRecognizer()
    
    @IBOutlet weak var btnAddBgImage: UIButton!
    @IBOutlet weak var QrCodeImage: UIImageView!
    
    @IBOutlet weak var lblQrText: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnActivePicker: UIButton!
    
    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var btnDownload: UIButton!
    
    @IBOutlet weak var btnpicker: UIButton!
    @IBOutlet weak var qrCodeBackGroundView: UIView!
    
    
    @IBOutlet weak var btnGallryOpen: UIButton!
    let imagePicker = UIImagePickerController()
    var selectedImage:UIImage!
    var img_pick : Bool = false
    var isActiveColorPicker = Bool()
    private var magnifyView: MagnifyView?
    
    @IBOutlet weak var imgGallary: UIImageView!
    var gotQRId = ""
    var images:[UIImage] = []
    var lastScale = CGFloat()
    
    // MARK:- Create self View Controller object
    class func initVC() -> EditQrCodeVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "EditQrCodeVC") as! EditQrCodeVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnInfo.isHidden = false
        btnDownload.isHidden = true
        btnpicker.isHidden = true
        btnActivePicker.isHidden = true
        btnClose.isHidden = false
        btnAddBgImage.isHidden = true
        
        if isActiveColorPicker{
            btnActivePicker.isHidden = false
        }else{
            btnActivePicker.isHidden = true
        }
        
        self.openCamera()
        
    }
    
    func fetchPhotos () {
           // Sort the images by descending creation date and fetch the first 3
           let fetchOptions = PHFetchOptions()
           fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
           fetchOptions.fetchLimit = 3

           // Fetch the image assets
           let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)

           // If the fetch result isn't empty,
           // proceed with the image request
           if fetchResult.count > 0 {
               let totalImageCountNeeded = 3 // <-- The number of images to fetch
               fetchPhotoAtIndex(0, totalImageCountNeeded, fetchResult)
           }
       }

      
       func fetchPhotoAtIndex(_ index:Int, _ totalImageCountNeeded: Int, _ fetchResult: PHFetchResult<PHAsset>) {

           let requestOptions = PHImageRequestOptions()
           requestOptions.isSynchronous = true

           // Perform the image request
           PHImageManager.default().requestImage(for: fetchResult.object(at: index) as PHAsset, targetSize: view.frame.size, contentMode: PHImageContentMode.aspectFill, options: requestOptions, resultHandler: { (image, _) in
               if let image = image {
                   // Add the returned image to your array
                   self.images += [image]
               }
              
               if index + 1 < fetchResult.count && self.images.count < totalImageCountNeeded {
                   self.fetchPhotoAtIndex(index + 1, totalImageCountNeeded, fetchResult)
                
               } else {
                   // Else you have completed creating your array
                   print("Completed array: \(self.images)")
                
                self.imgGallary.image = self.images[0]
                
               }
           })
    }
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let myString = Constant.ServerAPI.QR_CODE_URL + gotQRId
        
        let data = myString.data(using: String.Encoding.ascii)
        
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        
        qrFilter.setValue(data, forKey: "inputMessage")
        let transform = CGAffineTransform(scaleX: 10.0, y: 10.0)
        
        guard let output = qrFilter.outputImage?.transformed(by: transform) else {return}
        
        self.QrCodeImage.image = self.convert(cmage: output)
          
        self.setUpGestures()
        
        self.fetchPhotos()
        
    }
    
    @objc func pincheGestureHandler(recognizer:UIPinchGestureRecognizer){
        
        if isActiveColorPicker{
            self.isActiveColorPicker = false
            self.btnActivePicker.setImage(UIImage(named: "colorPicker"), for: .normal)
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
        }
        self.view.bringSubviewToFront(self.imageView)
        
        recognizer.view?.transform = (recognizer.view?.transform)!.scaledBy(x: recognizer.scale, y: recognizer.scale)
        
        
        recognizer.scale = 1.0
    }
    
    @objc func dragImg(_ sender:UIPanGestureRecognizer){
        if isActiveColorPicker{
            self.isActiveColorPicker = false
            self.btnActivePicker.setImage(UIImage(named: "colorPicker"), for: .normal)
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
        }
        let translation = sender.translation(in: self.view)
        imageView.center = CGPoint(x: imageView.center.x + translation.x, y: imageView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func setUpGestures () {

        let panGesture = UIPanGestureRecognizer(target: self, action:#selector(self.handlePanGesture(gesture:)))
        
        self.qrCodeBackGroundView.addGestureRecognizer(panGesture)

        let rotationGesture = UIRotationGestureRecognizer(target: self, action:#selector(self.handleRotationGesture(gesture:)))
        self.qrCodeBackGroundView.addGestureRecognizer(rotationGesture)

        let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(self.handlePinchGesture(gesture:)))
        self.qrCodeBackGroundView.addGestureRecognizer(pinchGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.qrCodeBackGroundView.addGestureRecognizer(tapGesture)
        
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        if isActiveColorPicker{
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
        }
        
    }
    
    @objc func handlePinchGesture(gesture: UIPinchGestureRecognizer) {
        
        if isActiveColorPicker{
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
        }
        
        if gesture.state == UIGestureRecognizer.State.began || gesture.state == UIGestureRecognizer.State.changed{
            
            gesture.view?.transform = (gesture.view?.transform)!.scaledBy(x: gesture.scale, y: gesture.scale)
            gesture.scale = 1.0
            
//            self.lastScale = gesture.scale
//
//            let currentWidth = self.qrCodeBackGroundView.frame.size.width
//            let currentHeight = self.qrCodeBackGroundView.frame.size.height
//
//            print(currentWidth)
//            print(currentHeight)
//
//            let fixWidth = (self.view.frame.size.width * 25) / 100
//            let fixHeight = (self.view.frame.size.height * 25) / 100
//
//            print(fixWidth)
//            print(fixHeight)
//
//            if (currentWidth > fixWidth)  {
//                gesture.view?.transform = (gesture.view?.transform)!.scaledBy(x: gesture.scale, y: gesture.scale)
//                gesture.scale = 1.0
//            }else {
//
//                if (currentHeight < fixHeight) {
//                    gesture.view?.transform = (gesture.view?.transform)!.scaledBy(x: gesture.scale, y: gesture.scale)
//                    gesture.scale = 1.0
//                }
//                else {
//
//                }
//            }
            
            
            
        }
    }

    @objc func handleRotationGesture(gesture: UIRotationGestureRecognizer) {
        if isActiveColorPicker{
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
            
        }
        if gesture.state == UIGestureRecognizer.State.began || gesture.state == UIGestureRecognizer.State.changed{
            gesture.view?.transform = (gesture.view?.transform)!.rotated(by: gesture.rotation)
            gesture.rotation = 0
        }
    }

    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        if isActiveColorPicker{
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
            
        }
        if gesture.state == UIGestureRecognizer.State.began || gesture.state == UIGestureRecognizer.State.changed{
            let translation = gesture.translation(in: view)
            gesture.view?.transform = (gesture.view?.transform)!.translatedBy(x: translation.x, y: translation.y)
            gesture.setTranslation(CGPoint(x: 0, y: 0), in: view)
        }
    }
    
    func image(with view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            return image
        }
        return nil
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    @IBAction func btnPhotoGallaryOpen(_ sender: Any) {
        self.openGallary()
    }
    
    @IBAction func btnAddBgImage_Click(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Image:", message: "", preferredStyle: .actionSheet)
        
        let cemeraButton = UIAlertAction(title: "Take photo", style: .default, handler: { (action) -> Void in
            
            self.openCamera()
        })
        
        let  galleryButton = UIAlertAction(title: "Pick picture from library", style: .default, handler: { (action) -> Void in
            self.openGallary()
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        alertController.addAction(cemeraButton)
        alertController.addAction(galleryButton)
        alertController.addAction(cancelButton)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    
    func openGallary()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    func openCamera()
    {
        if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerController.CameraDevice.front){
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        img_pick = true
        selectedImage = (info[UIImagePickerController.InfoKey.originalImage.rawValue] as! UIImage)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.imageView.image = selectedImage
        imageView.contentMode = .scaleAspectFill
		mainBG.addSubview(imageView)
        
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        imageView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1).isActive = true
        
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pincheGestureHandler))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(pinchGesture)
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.dragImg(_:)))
        imageView.isUserInteractionEnabled = true
        panGesture.minimumNumberOfTouches = 2
        imageView.addGestureRecognizer(panGesture)
        
		mainBG.sendSubviewToBack(imageView)
        self.view.layoutIfNeeded()
        
        btnAddBgImage.isHidden = true
        btnInfo.isHidden = false
        btnDownload.isHidden = false
        btnpicker.isHidden = false
        btnActivePicker.isHidden = true
        btnClose.isHidden = false
        btnAddBgImage.isHidden = true
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOpenColorPicker_Click(_ sender: Any) {
		self.imageView.isUserInteractionEnabled = false
        let colorPickerViewController = AMColorPickerViewController()
        colorPickerViewController.selectedColor = .red
        colorPickerViewController.delegate = self
        present(colorPickerViewController, animated: true, completion: nil)
    }
    
    func colorPicker(_ colorPicker: AMColorPicker, didSelect color: UIColor) {
        Utilities.setImageColor(imageView: QrCodeImage, color: color)
    }
    
    @IBAction func btnInfo_Click(_ sender: Any) {
        
        let vc = QRInfoVC.initVC()
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnDownload_Click(_ sender: Any) {
        
        btnInfo.isHidden = true
        btnDownload.isHidden = true
        btnpicker.isHidden = true
        btnActivePicker.isHidden = true
        btnClose.isHidden = true
        btnAddBgImage.isHidden = true
        self.imgGallary.isHidden = true
        self.btnGallryOpen.isHidden = true
        
        
        Utilities.takeScreenShot(success: {
            self.btnInfo.isHidden = false
            self.btnDownload.isHidden = true
            self.btnpicker.isHidden = true
            self.btnActivePicker.isHidden = true
            self.btnClose.isHidden = false
            self.btnAddBgImage.isHidden = true
            self.imageView.image = UIImage(named: "")
            self.imgGallary.isHidden = false
            self.btnGallryOpen.isHidden = false
            
            self.lblQrText.textColor = UIColor.black
            let swiftLeeOrangeColor = UIColor.black
            let qrURLImage = URL(string:Constant.ServerAPI.QR_CODE_URL + self.gotQRId)?.qrImage(using: swiftLeeOrangeColor)
            
            self.view.makeToast("Image hase been saved successfully")
            self.QrCodeImage.image = self.convert(cmage: qrURLImage!)
            self.qrCodeBackGroundView.backgroundColor = UIColor.white
            
        })
        
    }
    
    @IBAction func btnPicker_Click(_ sender: Any) {
        btnActivePicker.isHidden = false
    }
    
    @IBAction func btnClose_Click(_ sender: Any) {
        print("frdert")
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnActivePicker_Click(_ sender: Any) {
        
        if isActiveColorPicker == false {
            self.isActiveColorPicker = true
            self.btnActivePicker.setImage(UIImage(named: "colorPicker (1)"), for: .normal)
        }
        else {
            self.isActiveColorPicker = false
            self.btnActivePicker.setImage(UIImage(named: "colorPicker"), for: .normal)
        }
    }
    
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard event?.allTouches?.count == 1 else {
//            return
//        }
//
//        if isActiveColorPicker{
//            let point = touches.first?.location(in: self.view)
//
//            if magnifyView == nil
//            {
//                magnifyView = MagnifyView.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
//                magnifyView?.viewToMagnify = self.view
//                magnifyView?.setTouchPoint(pt: point!)
//                self.view.addSubview(magnifyView!)
//            }
//
//            if let touch = touches.first {
//
//                if touch.view == self.imageView {
//                    if let point = touches.first?.location(in: self.imageView) {
//                        let myImage = self.imageView
//                        var color = myImage.getPixelColorAt(point: point)
//                        print(color)
//
//                        color = myImage.getPixelColorAt(point: point)
//                        qrCodeBackGroundView.backgroundColor = color
//                        mainBG.backgroundColor = color
//                        superViewBg.backgroundColor = color
//
//                        let result = self.isLightColor(color: color)
//                        print(result)
//
//                        if result {
//                            self.lblQrText.textColor = UIColor.black
//                            let swiftLeeOrangeColor = UIColor.black
//                            let qrURLImage = URL(string:Constant.ServerAPI.QR_CODE_URL + gotQRId)?.qrImage(using: swiftLeeOrangeColor)
//
//                            self.QrCodeImage.image = self.convert(cmage: qrURLImage!)
//                        }
//                        else {
//                            self.lblQrText.textColor = UIColor.white
//                            let swiftLeeOrangeColor = UIColor.white
//                            let qrURLImage = URL(string:Constant.ServerAPI.QR_CODE_URL + gotQRId)?.qrImage(using: swiftLeeOrangeColor)
//
//                            self.QrCodeImage.image = self.convert(cmage: qrURLImage!)
//                        }
//                    }
//                }
//                else {
//                    print("View")
//                }
//            }
//        }
//
//    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isActiveColorPicker{
            if magnifyView != nil
            {
                magnifyView?.removeFromSuperview()
                magnifyView = nil
            }
        }
        
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard event?.allTouches?.count == 1 else {
			return
		}
        
        if isActiveColorPicker{
            
            guard let touch = touches.first else { return }
            
            let point = touches.first?.location(in: self.view)
            
            if magnifyView == nil
            {
                magnifyView = MagnifyView.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                magnifyView?.viewToMagnify = self.view
                magnifyView?.setTouchPoint(pt: point!)
                self.view.addSubview(magnifyView!)
            }
            
            let location = touch.location(in: self.imageView)
            print("x = \(location.x), y = \(location.y)")
            magnifyView?.setTouchPoint(pt: point!)
            magnifyView?.setNeedsDisplay()
            
            
            if let touch = touches.first {
                
                if touch.view == self.imageView {
                    
                    if let point = touches.first?.location(in: self.imageView) {
                        let myImage = self.imageView
                        var color = myImage.getPixelColorAt(point: point)
                        print(color)
                        color = myImage.getPixelColorAt(point: point)
                        qrCodeBackGroundView.backgroundColor = color
                        mainBG.backgroundColor = color
                        superViewBg.backgroundColor = color
                        
                        let result = self.isLightColor(color: color)
                        print(result)
                        
                        if result {
                            self.lblQrText.textColor = UIColor.black
                            let swiftLeeOrangeColor = UIColor.black
                            let qrURLImage = URL(string:Constant.ServerAPI.QR_CODE_URL + gotQRId)?.qrImage(using: swiftLeeOrangeColor)
                            
                            self.QrCodeImage.image = self.convert(cmage: qrURLImage!)
                        }
                        else {
                            self.lblQrText.textColor = UIColor.white
                            let swiftLeeOrangeColor = UIColor.white
                            let qrURLImage = URL(string:Constant.ServerAPI.QR_CODE_URL + gotQRId)?.qrImage(using: swiftLeeOrangeColor)
                            
                            self.QrCodeImage.image = self.convert(cmage: qrURLImage!)
                        }
                    }
                    
                }
                else {
                    
                }
            }
            
        }
    }
    
    func isLightColor(color: UIColor) -> Bool
    {
         var isLight = false

        let componentColors = color.cgColor.components
        
        let v1 = (componentColors![0] * 299)
        let v2 = (componentColors![1] * 587)
        let v3 = (componentColors![2] * 114)

        let colorBrightness: CGFloat = ( v1 + v2 + v3)
        let coloB = colorBrightness / 1000
        
         if (coloB >= 0.5)
         {
            isLight = true
            print("my color is light")
         }
         else
         {
            print("my color is dark")
         }
         return isLight
    }
    
    
}



extension UIImage {
    func getPixelColor(pos: CGPoint, image : UIImage) -> UIColor {

        let pixelData = image.cgImage!.dataProvider!.data
         let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

         let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4

       
         let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
         let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
         let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
         let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)

         return UIColor(red: r, green: g, blue: b, alpha: a)
        
     }
 }

extension UIImage {
    
    func getPixelColor1(atLocation location: CGPoint, withFrameSize size: CGSize) -> UIColor {
        let x: CGFloat = (self.size.width) * location.x / size.width
        let y: CGFloat = (self.size.height) * location.y / size.height
        
        let pixelPoint: CGPoint = CGPoint(x: x, y: y)
        
        let pixelData = self.cgImage!.dataProvider!.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        let pixelIndex: Int = ((Int(self.size.width) * Int(pixelPoint.y)) + Int(pixelPoint.x)) * 4
        
        let r = CGFloat(data[pixelIndex]) / CGFloat(255.0)
        let g = CGFloat(data[pixelIndex+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelIndex+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelIndex+3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
}


extension UIImageView {
    func getPixelColorAt(point:CGPoint) -> UIColor{
        
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
        context!.translateBy(x: -point.x, y: -point.y)
        layer.render(in: context!)
        let color:UIColor = UIColor(red: CGFloat(pixel[0])/255.0,
                                    green: CGFloat(pixel[1])/255.0,
                                    blue: CGFloat(pixel[2])/255.0,
                                    alpha: CGFloat(pixel[3])/255.0)
        
        pixel.deallocate()
        return color
    }
    
}


extension UIColor {
    
    func rgba() -> (red:Int, green:Int, blue:Int, alpha:Int)? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
    
    
}

extension URL {

    /// Creates a QR code for the current URL in the given color.
    func qrImage(using color: UIColor) -> CIImage? {
        return qrImage?.tinted(using: color)
    }

    /// Returns a black and white QR code for this URL.
    var qrImage: CIImage? {
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return nil }
        let qrData = absoluteString.data(using: String.Encoding.ascii)
        qrFilter.setValue(qrData, forKey: "inputMessage")

        let qrTransform = CGAffineTransform(scaleX: 12, y: 12)
        return qrFilter.outputImage?.transformed(by: qrTransform)
    }
}

extension CIImage {
    /// Inverts the colors and creates a transparent image by converting the mask to alpha.
    /// Input image should be black and white.
    var transparent: CIImage? {
        return inverted?.blackTransparent
    }

    /// Inverts the colors.
    var inverted: CIImage? {
        guard let invertedColorFilter = CIFilter(name: "CIColorInvert") else { return nil }

        invertedColorFilter.setValue(self, forKey: "inputImage")
        return invertedColorFilter.outputImage
    }

    /// Converts all black to transparent.
    var blackTransparent: CIImage? {
        guard let blackTransparentFilter = CIFilter(name: "CIMaskToAlpha") else { return nil }
        blackTransparentFilter.setValue(self, forKey: "inputImage")
        return blackTransparentFilter.outputImage
    }

    /// Applies the given color as a tint color.
    func tinted(using color: UIColor) -> CIImage?
    {
        guard
            let transparentQRImage = transparent,
            let filter = CIFilter(name: "CIMultiplyCompositing"),
            let colorFilter = CIFilter(name: "CIConstantColorGenerator") else { return nil }

        let ciColor = CIColor(color: color)
        colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
        let colorImage = colorFilter.outputImage

        filter.setValue(colorImage, forKey: kCIInputImageKey)
        filter.setValue(transparentQRImage, forKey: kCIInputBackgroundImageKey)

        return filter.outputImage!
    }
}





//if UIImagePickerController.isCameraDeviceAvailable( UIImagePickerController.CameraDevice.front) {
//    imagePicker.delegate = self
//    imagePicker.sourceType = UIImagePickerController.SourceType.camera
//    present(imagePicker, animated: true, completion: nil)
//
//}
