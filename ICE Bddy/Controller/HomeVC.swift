//
//  HomeVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import DropDown
import SideMenu
import Toast_Swift
import ContactsUI
import MessageUI
import CoreLocation


class HomeVC: UIViewController,CNContactPickerDelegate,MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    let store = CNContactStore()
    
    var isContact1 = false
    var isContact2 = false
    var isContact3 = false
    var GotLat = ""
    var GotLog = ""
    var LocationSms = ""
    
   
    @IBOutlet weak var bgAlertView: UIView!
    @IBOutlet weak var bgContact3NewHeight: NSLayoutConstraint!
    @IBOutlet weak var bgContact2newHeight: NSLayoutConstraint!
    @IBOutlet weak var lblConatct3Name: UILabel!
    @IBOutlet weak var lblContact3Title: UILabel!
    @IBOutlet weak var bgContact3New: UIView!
    @IBOutlet weak var lblContact2Name: UILabel!
    @IBOutlet weak var lblContact2Title: UILabel!
    @IBOutlet weak var bgContact2New: UIView!
    @IBOutlet weak var bgContact1Height: NSLayoutConstraint!
    @IBOutlet weak var lblContact1Name: UILabel!
    @IBOutlet weak var lblContact1Title: UILabel!
    @IBOutlet weak var bgContact1New: UIView!
    @IBOutlet weak var btnContact1: UIButton!
    
    @IBOutlet weak var btnContact3Ph: UIButton!
    @IBOutlet weak var btnContact3: UIButton!
    @IBOutlet weak var btnContactPh: UIButton!
    @IBOutlet weak var btnContact2: UIButton!
    @IBOutlet weak var btnContact1Ph: UIButton!
    @IBOutlet weak var DOBBottom2: NSLayoutConstraint!
    @IBOutlet weak var DOBBottom1: NSLayoutConstraint!
    @IBOutlet weak var bgMedicationHeight: NSLayoutConstraint!
    @IBOutlet weak var bgMedicationView: UIView!
    @IBOutlet weak var lblMedicationBottom: NSLayoutConstraint!
    @IBOutlet weak var lblmedicationHeight: NSLayoutConstraint!
    @IBOutlet weak var lblMedicationTop: NSLayoutConstraint!
    @IBOutlet weak var lblOtherTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var lblOtherBottom: NSLayoutConstraint!
    @IBOutlet weak var lblOtherTop: NSLayoutConstraint!
    @IBOutlet weak var lblOtherTitle: UILabel!
    @IBOutlet weak var OtherViewheight: NSLayoutConstraint!
    @IBOutlet weak var bgOtherView: UIView!
    @IBOutlet weak var lblDurgAllergyHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDurgAllergyBottom: NSLayoutConstraint!
    @IBOutlet weak var lblDrugAllergyTop: NSLayoutConstraint!
    @IBOutlet weak var lblDrugAllergyTitle: UILabel!
    @IBOutlet weak var durgAllergyheight: NSLayoutConstraint!
    @IBOutlet weak var bgDurgAllergy: UIView!
    @IBOutlet weak var stackDOB: UIStackView!
    @IBOutlet weak var imgDOBDrop3: UIImageView!
    @IBOutlet weak var imgDOBDrop2: UIImageView!
    @IBOutlet weak var imgDOBDrop1: UIImageView!
    @IBOutlet weak var stackDOBHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDOBBottom: NSLayoutConstraint!
    @IBOutlet weak var lblDOBHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDOBTop: NSLayoutConstraint!
    @IBOutlet weak var stackPrimayHeight: NSLayoutConstraint!
    @IBOutlet weak var bgPrimayPh: UIView!
    @IBOutlet weak var bgPrimaryViewname: UIView!
    @IBOutlet weak var stckPrimary: UIStackView!
    @IBOutlet weak var bgPrimaryPhHeight: NSLayoutConstraint!
    @IBOutlet weak var bgPrimaryNameHeight: NSLayoutConstraint!
    @IBOutlet weak var lblPrimaryTitleBottom: NSLayoutConstraint!
    @IBOutlet weak var lblPrimaryTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var lblPrimaryTitleTop: NSLayoutConstraint!
    @IBOutlet weak var txtAddressView: UITextView!
    @IBOutlet weak var btnCamera1: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEmeTitle3Height: NSLayoutConstraint!
    @IBOutlet weak var lblEmeTitle2Height: NSLayoutConstraint!
    @IBOutlet weak var lblEmeContact3Top: NSLayoutConstraint!
    @IBOutlet weak var lblEmeContact2Top: NSLayoutConstraint!
    @IBOutlet weak var bgHealthInsuOld: UIView!
    @IBOutlet weak var lblHealthInsuOld: UILabel!
    @IBOutlet weak var lblOrganDonorTitleOld: UILabel!
    @IBOutlet weak var bgOrganDonorOld: UIView!
    @IBOutlet weak var bgHeartProblemOld: UIView!
    @IBOutlet weak var lblHeartProblemTitleOld: UILabel!
    var isFromMenu : Bool = false
    @IBOutlet weak var imgDiabetes: UIImageView!
    @IBOutlet weak var btnDiabetes: UIButton!
    @IBOutlet weak var btnBloodPresure: UIButton!
    @IBOutlet weak var imgBloodPressure: UIImageView!
    @IBOutlet weak var btnSubmitHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var bgHealthInsuHeight: NSLayoutConstraint!
    @IBOutlet weak var lblHealthInsuranceNew: UILabel!
    @IBOutlet weak var bgHealthInsuNew: UIView!
    @IBOutlet weak var bgOrganDonorHeight: NSLayoutConstraint!
    @IBOutlet weak var bgOrganDonorNew: UIView!
    @IBOutlet weak var lblOrganDonorNew: UILabel!
    @IBOutlet weak var bgHeartProblemHeight: NSLayoutConstraint!
    @IBOutlet weak var bgHeartProblemNew: UIView!
    @IBOutlet weak var lblHeartProblemNew: UILabel!
    @IBOutlet weak var bgBloodGroupOldHeight: NSLayoutConstraint!
    @IBOutlet weak var bgBloodGroupOld: UIView!
    @IBOutlet weak var lblBloodGroupNew: UILabel!
    @IBOutlet weak var bgBloodGroupNewHeight: NSLayoutConstraint!
    @IBOutlet weak var bgBloodGroupNew: UIView!
    @IBOutlet weak var bgWeightHeight: NSLayoutConstraint!
    @IBOutlet weak var bgWeightNew: UIView!
    @IBOutlet weak var lblWeightNew: UILabel!
    @IBOutlet weak var lblHeightNew: UILabel!
    @IBOutlet weak var bgHeightNewHeight: NSLayoutConstraint!
    @IBOutlet weak var bgHeightNew: UIView!
    @IBOutlet weak var lblAgeTitle: UILabel!
    @IBOutlet weak var btnDOB: UIButton!
    @IBOutlet weak var lblYearnew: UILabel!
    @IBOutlet weak var lblMonthNew: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblDayNew: UILabel!
    @IBOutlet weak var bgdateOfBirthNew: UIView!
    @IBOutlet weak var lblEmergencyContactTop: NSLayoutConstraint!
    @IBOutlet weak var lblPrimaryTop: NSLayoutConstraint!
    @IBOutlet weak var lblEmergencyContact_3_Title: UILabel!
    @IBOutlet weak var lblEmergencyContact_2_Title: UILabel!
    @IBOutlet weak var healthInsuranceHeight: NSLayoutConstraint!
    @IBOutlet weak var bgHealthInsurance: UIView!
    @IBOutlet weak var lbl10: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
    @IBOutlet weak var v3: UIView!
    @IBOutlet weak var v4: UIView!
    @IBOutlet weak var v5: UIView!
    
    @IBOutlet weak var v10: UIView!
    @IBOutlet weak var v9: UIView!
    @IBOutlet weak var v8: UIView!
    @IBOutlet weak var v7: UIView!
    @IBOutlet weak var v6: UIView!
    
    
    @IBOutlet weak var bgTutorial: UIView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var txtName: UITextField!
    
    // txtEmergency contact 1
    @IBOutlet weak var btnEmergencyExpand_1: UIButton!
    @IBOutlet weak var heightConstraintEmergencyContact_1: NSLayoutConstraint!
    @IBOutlet weak var emergenctContactView_1: UIView!
    @IBOutlet weak var txtEmergencyName_1: UITextField!
    @IBOutlet weak var txtPhoneNo_1: UITextField!
    @IBOutlet weak var txtEmergencyRelative_1: UITextField!
    
    
    // txtEmergency contact 2
    @IBOutlet weak var btnEmergencyExpand_2: UIButton!
    @IBOutlet weak var heightConstraintEmergencyContact_2: NSLayoutConstraint!
    @IBOutlet weak var emergenctContactView_2: UIView!
    @IBOutlet weak var txtEmergencyName_2: UITextField!
    @IBOutlet weak var txtPhoneNo_2: UITextField!
    @IBOutlet weak var txtEmergencyRelative_2: UITextField!
    
    // txtEmergency contact 3
    @IBOutlet weak var btnEmergencyExpand_3: UIButton!
    
    @IBOutlet weak var heightConstraintEmergencyContact_3: NSLayoutConstraint!
    @IBOutlet weak var emergenctContactView_3: UIView!
    
    @IBOutlet weak var txtEmergencyName_3: UITextField!
    @IBOutlet weak var txtPhoneNo_3: UITextField!
    @IBOutlet weak var txtEmergencyRelative_3: UITextField!
    
    @IBOutlet weak var txtPhysicianName: UITextField!
    @IBOutlet weak var txtPhysicianPhoneNo: UITextField!
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    @IBOutlet weak var heightSlider: UISlider!
    @IBOutlet weak var weightSlider: UISlider!
    
    @IBOutlet weak var txtBloodPressure: UITextField!
    
    @IBOutlet weak var txtDiabetes: UITextField!
    
    // Heart Problem Buttons
    @IBOutlet weak var radioHeartYesImage: UIImageView!
    @IBOutlet weak var radioHeartNoImage: UIImageView!
    @IBOutlet weak var radioHeartPreferSayNoImage: UIImageView!
    
    @IBOutlet weak var txtDrugAlergy: UITextField!
    @IBOutlet weak var txtOtherAlergy: UITextField!
    @IBOutlet weak var txtMedication: UITextField!
    
    
    // Organ Donner Buttons
    @IBOutlet weak var radioDonnerYesImage: UIImageView!
    @IBOutlet weak var radioDonnertNoImage: UIImageView!
    @IBOutlet weak var radioDonnerPreferSayNoImage: UIImageView!
    
    // Have Insurance Buttons
    @IBOutlet weak var radioInsuranceYesImage: UIImageView!
    @IBOutlet weak var radioInsuranceNoImage: UIImageView!
    @IBOutlet weak var radioInsurancePreferSayNoImage: UIImageView!
    
    
    @IBOutlet weak var txtCompnyName: UITextField!
    @IBOutlet weak var txtPolicyNumber: UITextField!
    @IBOutlet weak var txtOtherDetails: UITextField!
    
    @IBOutlet weak var shadowView: UIView!
    
    // MARK: Variable
    
    var isExpand_1 : Bool = false
    var isExpand_2 : Bool = false
    var isExpand_3 : Bool = false
    var arrBloodGroup = ["A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-", "Hh", "Don't Know"]
    var selectedGroup : Int = 0
    
    var dateOfBirth = ""
    var bloodGroup = ""
    var userData = [ModelUserData]()
    var isOrgan = ""
    var isHealth = ""
    var isheartProblem = ""
    
    var QRId = ""
    
    let minColor : UIImage = {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(Utilities.colorWithHexString(hex: "4897F6").cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }()
    
    
    // MARK:- Create self View Controller object
    class func initVC() -> HomeVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgAlertView.isHidden = true
        
        self.checkForLocationService()
        
        let uId = ModelUserData.getUserDataFromUserDefaults().id ?? ""
        let EId = ModelQrCodeData.getUserDataFromUserDefaults().userId ?? ""
        
        if let UDData = USERDEFAULTS.value(forKey: kModelUserQRData) as? Data,
            let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelQrCodeData {
                
            if isFromMenu {
                
                self.lblTitle.text = "Edit Detail"
                self.lblTitle1.text = "Edit Detail"
                
                if model.address == nil {
                    self.bgTutorial.isHidden = false
                    self.mainView.isHidden = true
                    self.btnCamera.isHidden = true
                    self.btnCamera1.isHidden = true
                }
                else {
                    self.btnCamera.isHidden = false
                    self.btnCamera1.isHidden = false
                    setEditData()
                }
                   
            }
            else {
                
                self.lblTitle.text = "Home"
                self.lblTitle1.text = "Home"
                
                if model.address == nil {
                    self.bgTutorial.isHidden = false
                    self.mainView.isHidden = true
                    self.btnCamera.isHidden = true
                    self.btnCamera1.isHidden = true
                    
                    if let UDData = USERDEFAULTS.value(forKey: kModelUserData) as? Data,
                        let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelUserData {
                        
                        self.txtName.text = model.name ?? ""
                        
                    }
                }
                else {
                    self.btnCamera.isHidden = false
                    self.btnCamera1.isHidden = false
                    self.setData()
                }
                
            }
        }
        else {
            self.lblTitle.text = "Home"
            self.lblTitle1.text = "Home"
            self.btn1((UIButton).self)
            DispatchQueue.main.async {
                self.initialConfig()
            }
            self.bgTutorial.isHidden = false
            self.mainView.isHidden = true
            self.btnCamera.isHidden = true
            self.btnCamera1.isHidden = true
            
            if let UDData = USERDEFAULTS.value(forKey: kModelUserData) as? Data,
                let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelUserData {
                
                self.txtName.text = model.name ?? ""
            }
            
            
        }
           
    }
    
    // MARK:- Check for location services
    func checkForLocationService() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                let okAction = UIAlertAction(title: "Setting", style: .default, handler: { (alert: UIAlertAction!) in
                    
                    if let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\("app.globalpraise.com")") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                    
                    self.getLocationFromLatLog()
                    

                })
                let cancelAction = UIAlertAction(title:  "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) in
                    
                })
                Utility.showAlert("Allow Location Access", message: "ICE Buddy  shows the user location for that emergency contact require access your location.", actions: [cancelAction,okAction])
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("Granted")
                
                self.getLocationFromLatLog()
                
            }
        } else {
            
            print("Location services are not enabled")
            
        }
    }
    
    func getLocationFromLatLog() {
        
        LocationHandler.shared.getLocationUpdates { (locationManager, location) -> (Bool) in
            locationManager.distanceFilter = 50
            locationManager.startUpdatingLocation()
            Constant.location = location
            self.GotLat = String(location.coordinate.latitude)
            self.GotLog = String(location.coordinate.longitude)
            
            print(self.GotLat)
            print(self.GotLog)
            
            self.LocationSms = "http://maps.google.com/maps?q=" + "\(self.GotLat)," + "\(self.GotLog)"
            
            return true
        }
    }
    
    func setEditData() {
        if let UDData = USERDEFAULTS.value(forKey: kModelUserQRData) as? Data,
            let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelQrCodeData {
            
            self.lblTitle.text = "Edit Detail"
            self.lblTitle1.text = "Edit Detail"
            
            self.bgTutorial.isHidden = true
            self.mainView.isHidden = false
            self.btnCamera.isHidden = false
            
            self.heightSlider.isHidden = false
            self.weightSlider.isHidden = false
            
            self.txtName.text = model.name ?? ""
            self.txtAddressView.text = model.address ?? ""
            
            self.QRId = model.id ?? ""
            
            if model.emergName != "" {
                self.isExpand_1 = true
                emergenctContactView_1.isHidden = false
                heightConstraintEmergencyContact_1.constant = 240
                btnEmergencyExpand_1.setImage(UIImage(named: "dropUp"), for: .normal)
            }
            else {
                self.isExpand_1 = false
                emergenctContactView_1.isHidden = true
                heightConstraintEmergencyContact_1.constant = 0
                btnEmergencyExpand_1.setImage(UIImage(named: "dropDown"), for: .normal)
            }
            
            
            self.lblContact1Title.text = model.emergName ?? ""
            self.lblContact1Name.text = model.emergRelationship ?? ""
            self.bgContact1New.isHidden = true
            self.bgContact1Height.constant = 240
            
            self.txtEmergencyName_1.text = model.emergName ?? ""
            self.txtPhoneNo_1.text = model.emergPhone ?? ""
            self.txtEmergencyRelative_1.text = model.emergRelationship ?? ""
            
            
            
            if model.emergName2 != "" {
                self.isExpand_2 = true
                emergenctContactView_2.isHidden = false
                heightConstraintEmergencyContact_2.constant = 240
                btnEmergencyExpand_2.setImage(UIImage(named: "dropUp"), for: .normal)
                
                self.lblContact2Title.text = model.emergName2 ?? ""
                self.lblContact2Name.text = model.emergRelationship2 ?? ""
                self.bgContact2New.isHidden = true
                self.bgContact2newHeight.constant = 240
            }
            else {
                self.isExpand_2 = false
                emergenctContactView_2.isHidden = true
                heightConstraintEmergencyContact_2.constant = 0
                btnEmergencyExpand_2.setImage(UIImage(named: "dropDown"), for: .normal)
                
                self.lblContact2Title.text = model.emergName2 ?? ""
                self.lblContact2Name.text = model.emergRelationship2 ?? ""
                self.bgContact2New.isHidden = true
                self.bgContact2newHeight.constant = 0
            }
            
            
            
            self.txtEmergencyName_2.text = model.emergName2 ?? ""
            self.txtPhoneNo_2.text = model.emergPhone2 ?? ""
            self.txtEmergencyRelative_2.text = model.emergRelationship2 ?? ""
            
            
            if model.emergName3 != "" {
                self.isExpand_3 = true
                emergenctContactView_3.isHidden = false
                heightConstraintEmergencyContact_3.constant = 240
                btnEmergencyExpand_3.setImage(UIImage(named: "dropUp"), for: .normal)
                
                self.lblContact3Title.text = model.emergName3 ?? ""
                self.lblConatct3Name.text = model.emergRelationship3 ?? ""
                self.bgContact3New.isHidden = true
                self.bgContact3NewHeight.constant = 240
            }
            else {
                self.isExpand_3 = false
                emergenctContactView_3.isHidden = true
                heightConstraintEmergencyContact_3.constant = 0
                btnEmergencyExpand_3.setImage(UIImage(named: "dropDown"), for: .normal)
                
                self.lblContact3Title.text = model.emergName3 ?? ""
                self.lblConatct3Name.text = model.emergRelationship3 ?? ""
                self.bgContact3New.isHidden = true
                self.bgContact3NewHeight.constant = 0
            }
            
            
            
            self.txtEmergencyName_3.text = model.emergName3 ?? ""
            self.txtPhoneNo_3.text = model.emergPhone3 ?? ""
            self.txtEmergencyRelative_3.text = model.emergRelationship3 ?? ""
            
            self.txtPhysicianName.text = model.physicianName ?? ""
            self.txtPhysicianPhoneNo.text = model.physicianPhone ?? ""
            
            self.txtPhysicianName.text = model.physicianName ?? ""
            self.stackPrimayHeight.constant = 115
            self.bgPrimaryNameHeight.constant = 50
            self.bgPrimaryPhHeight.constant = 50
            self.stckPrimary.isHidden = false
            
            
            if model.dateOfBirth == "" {
                self.lblAgeTitle.isHidden = true
                self.lblDOBTop.constant = 35
                self.lblDOBBottom.constant = 15
                self.lblDOBHeight.constant = 25
                self.stackDOBHeight.constant = 50
                self.stackDOB.isHidden = false
                self.bgdateOfBirthNew.isHidden = true
                self.DOBBottom1.constant = 35
                self.DOBBottom2.constant = 35
            }
                    
            
            if self.dateOfBirth != "" {
                
                self.dateOfBirth = model.dateOfBirth ?? ""
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "YYYY-MM-dd"
                let date = dateFormatter1.date(from:self.dateOfBirth)!
                
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "YYYY-MM-dd"
                let showDate = inputFormatter.date(from: self.dateOfBirth)
                inputFormatter.dateFormat = "MM/dd/yyyy"
                let resultString = inputFormatter.string(from: showDate!)
               
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                let year: String = dateFormatter.string(from: date)
                self.lblYear.text = year
                dateFormatter.dateFormat = "MM"
                let month: String = dateFormatter.string(from: date)
                self.lblMonth.text = month
                dateFormatter.dateFormat = "dd"
                let day: String = dateFormatter.string(from: date)
                self.lblDay.text = day
                
                self.bgdateOfBirthNew.isHidden = true
                self.lblAgeTitle.isHidden = true
                self.lblDayNew.text = day
                self.lblYearnew.text = year
                
                let now = date
                let dateFormatternow = DateFormatter()
                dateFormatternow.dateFormat = "LLLL"
                let nameOfMonth = dateFormatter.string(from: now)
                print(nameOfMonth)
                
                
                let nameFormatter = DateFormatter()
                nameFormatter.dateFormat = "MMMM"

                let name1 = nameFormatter.string(from: date)
                let index = Calendar.current.component(.month, from: date)

                print(name1)  // April
                print(index) // 4
                
                self.lblMonthNew.text = name1
                
                self.btnDOB.isHidden = false
                self.lblAge.text = "\(self.calcAge(birthday: resultString))"
                
            }
            
           
            
            
            self.heightSlider.value = Float(model.height) ?? 1
            self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.normal)
            self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.selected)
            
            self.bgHeightNew.isHidden = true
            self.lblHeightNew.text = model.height ?? ""
            self.bgHeightNewHeight.constant = 0
            
            self.weightSlider.value = Float(model.weight) ?? 1
            self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.normal)
            self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.selected)
            
            self.bgWeightNew.isHidden = true
            self.lblWeightNew.text = model.weight ?? ""
            self.bgWeightHeight.constant = 0
            
            self.bloodGroup = model.bloodGroup ?? ""
            self.lblBloodGroupNew.text = model.bloodGroup ?? ""
            
            self.bgBloodGroupNew.isHidden = true
            self.bgBloodGroupNewHeight.constant = 0
            self.bgBloodGroupOld.isHidden = false
            self.bgBloodGroupOldHeight.constant = 120
            
            if bloodGroup == "A+" {
                self.btn1((UIButton).self)
            }
            else if bloodGroup == "A-" {
                self.btn2((UIButton).self)
            }
            else if bloodGroup == "B+" {
                self.btn3((UIButton).self)
            }
            else if bloodGroup == "B-" {
                self.btn4((UIButton).self)
            }
            else if bloodGroup == "O+" {
                self.btn5((UIButton).self)
            }
            else if bloodGroup == "O-" {
                self.btn6((UIButton).self)
            }
            else if bloodGroup == "AB+" {
                self.btn7((UIButton).self)
            }
            else if bloodGroup == "AB-" {
                self.btn8((UIButton).self)
            }
            else if bloodGroup == "Hh" {
                self.btn9((UIButton).self)
            }
            else if bloodGroup == "Don't Know" {
                self.btn10((UIButton).self)
            }
           
            
            self.txtBloodPressure.text = model.bloodPressure ?? ""
            self.txtDiabetes.text = model.diabetes ?? ""
            
            self.bgHeartProblemNew.isHidden = true
            self.bgHeartProblemHeight.constant = 50
            self.lblHeartProblemTitleOld.isHidden = false
            self.bgHeartProblemOld.isHidden = false
            
            if model.heartProblem == "1" {
                radioHeartYesImage.image = UIImage(named: "radioFill")
                radioHeartNoImage.image = UIImage(named: "radio")
                radioHeartPreferSayNoImage.image = UIImage(named: "radio")
                self.isheartProblem = "1"
                self.lblHeartProblemNew.text = "YES"
            }
            else if model.heartProblem == "2" {
                radioHeartYesImage.image = UIImage(named: "radio")
                radioHeartNoImage.image = UIImage(named: "radio")
                radioHeartPreferSayNoImage.image = UIImage(named: "radioFill")
                self.isheartProblem = "2"
                self.lblHeartProblemNew.text = "Prefer not to say"
            }
            else {
                self.isheartProblem = "0"
                self.lblHeartProblemNew.text = "NO"
                radioHeartYesImage.image = UIImage(named: "radio")
                radioHeartNoImage.image = UIImage(named: "radioFill")
                radioHeartPreferSayNoImage.image = UIImage(named: "radio")
            }
            
            self.txtDrugAlergy.text = model.drugAllergy ?? ""
            self.bgDurgAllergy.isHidden = false
            self.durgAllergyheight.constant = 70
            self.lblDrugAllergyTop.constant = 20
            self.lblDurgAllergyBottom.constant = 20
            self.lblDurgAllergyHeight.constant = 25
            
            
            self.txtOtherAlergy.text = model.otherAllergy ?? ""
            self.bgOtherView.isHidden = false
            self.OtherViewheight.constant = 70
            self.lblOtherTop.constant = 20
            self.lblOtherBottom.constant = 20
            self.lblOtherTitleHeight.constant = 25
            
            
            self.txtMedication.text = model.medication ?? ""
            self.bgMedicationView.isHidden = false
            self.bgMedicationHeight.constant = 70
            self.lblMedicationTop.constant = 20
            self.lblMedicationBottom.constant = 20
            self.lblmedicationHeight.constant = 25
           
            
            self.isOrgan = model.isOrganDonor ?? ""
            self.bgOrganDonorNew.isHidden = true
            self.bgOrganDonorHeight.constant = 50
            self.bgOrganDonorOld.isHidden = false
            self.lblOrganDonorTitleOld.isHidden = false
            
            
            if model.isOrganDonor == "1" {
                radioDonnerYesImage.image = UIImage(named: "radioFill")
                radioDonnertNoImage.image = UIImage(named: "radio")
                self.lblOrganDonorNew.text = "YES"
            }
            else if model.isOrganDonor == "2" {
                radioDonnerYesImage.image = UIImage(named: "radio")
                radioDonnertNoImage.image = UIImage(named: "radio")
                radioDonnerPreferSayNoImage.image = UIImage(named: "radioFill")
                self.lblOrganDonorNew.text = "Prefer not to say"
            }
            else {
                self.lblOrganDonorNew.text = "NO"
                radioDonnerYesImage.image = UIImage(named: "radio")
                radioDonnertNoImage.image = UIImage(named: "radioFill")
            }
            
            
            self.isHealth = model.isHealthInsurance ?? ""
            self.bgHealthInsuNew.isHidden = true
            self.bgHealthInsuHeight.constant = 50
            self.bgHealthInsuOld.isHidden = false
            self.lblHeartProblemTitleOld.isHidden = false
            
            if self.isHealth == "1" {
                self.bgHealthInsurance.isHidden = false
                self.healthInsuranceHeight.constant = 180
                self.txtCompnyName.text = model.companyName ?? ""
                self.txtPolicyNumber.text = model.policyNumber ?? ""
                self.txtOtherDetails.text = model.otherDetails ?? ""
                radioInsuranceYesImage.image = UIImage(named: "radioFill")
                radioInsuranceNoImage.image = UIImage(named: "radio")
                self.lblHealthInsuranceNew.text = "YES"
            }
            else if self.isHealth == "2" {
                self.txtCompnyName.text = ""
                self.txtPolicyNumber.text = ""
                self.txtOtherDetails.text = ""
                self.bgHealthInsurance.isHidden = true
                self.healthInsuranceHeight.constant = 0
                radioInsuranceYesImage.image = UIImage(named: "radio")
                radioInsuranceNoImage.image = UIImage(named: "radio")
                radioInsurancePreferSayNoImage.image = UIImage(named: "radioFill")
                self.lblHealthInsuranceNew.text = "Prefer not to say"
            }
            else {
                self.txtCompnyName.text = ""
                self.txtPolicyNumber.text = ""
                self.txtOtherDetails.text = ""
                self.bgHealthInsurance.isHidden = true
                self.healthInsuranceHeight.constant = 0
                radioInsuranceYesImage.image = UIImage(named: "radio")
                radioInsuranceNoImage.image = UIImage(named: "radioFill")
                self.lblHealthInsuranceNew.text = "NO"
            }
              
            self.btnSubmit.isHidden = false
            self.btnSubmitHeight.constant = 50
            imgDiabetes.isHidden = false
            btnDiabetes.isHidden = false
            btnBloodPresure.isHidden = false
            imgBloodPressure.isHidden = false
            
            self.txtName.isUserInteractionEnabled = true
            self.txtAddressView.isUserInteractionEnabled = true
            self.txtEmergencyRelative_1.isUserInteractionEnabled = true
            self.txtEmergencyRelative_2.isUserInteractionEnabled = true
            self.txtEmergencyRelative_3.isUserInteractionEnabled = true
            self.btnContact1.isUserInteractionEnabled = true
            self.btnContact1Ph.isUserInteractionEnabled = true
            self.btnContact2.isUserInteractionEnabled = true
            self.btnContactPh.isUserInteractionEnabled = true
            self.btnContact3.isUserInteractionEnabled = true
            self.btnContact3Ph.isUserInteractionEnabled = true
            self.txtPhysicianPhoneNo.isUserInteractionEnabled = true
            self.txtPhysicianName.isUserInteractionEnabled = true
            self.txtBloodPressure.isUserInteractionEnabled = true
            self.txtDiabetes.isUserInteractionEnabled = true
            self.txtEmergencyName_1.isUserInteractionEnabled = true
            self.txtPhoneNo_1.isUserInteractionEnabled = true
            self.txtEmergencyName_2.isUserInteractionEnabled = true
            self.txtPhoneNo_2.isUserInteractionEnabled = true
            self.txtEmergencyName_3.isUserInteractionEnabled = true
            self.txtPhoneNo_3.isUserInteractionEnabled = true
            self.txtDrugAlergy.isUserInteractionEnabled = true
            self.txtMedication.isUserInteractionEnabled = true
            self.txtCompnyName.isUserInteractionEnabled = true
            self.txtPolicyNumber.isUserInteractionEnabled = true
            self.txtOtherDetails.isUserInteractionEnabled = true
            self.txtOtherAlergy.isUserInteractionEnabled = true
        }
    }
    
    func setData() {
        if let UDData = USERDEFAULTS.value(forKey: kModelUserQRData) as? Data,
            let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelQrCodeData {
            
            self.lblTitle.text = "Home"
            self.lblTitle1.text = "Home"
            
            self.bgTutorial.isHidden = true
            self.mainView.isHidden = false
            self.heightSlider.isHidden = true
            self.weightSlider.isHidden = true
            self.btnCamera.isHidden = false
            
            self.txtName.text = model.name ?? ""
            self.txtAddressView.text = model.address ?? ""
            
            self.QRId = model.id ?? ""
            
            if model.emergName != "" {
                self.isExpand_1 = true
                emergenctContactView_1.isHidden = false
                heightConstraintEmergencyContact_1.constant = 240
                btnEmergencyExpand_1.setImage(UIImage(named: "dropUp"), for: .normal)
                self.btnEmergencyExpand_1.isHidden = true
            }
            else {
                self.isExpand_1 = false
                emergenctContactView_1.isHidden = true
                heightConstraintEmergencyContact_1.constant = 0
                btnEmergencyExpand_1.setImage(UIImage(named: "dropDown"), for: .normal)
                self.btnEmergencyExpand_1.isHidden = false
            }
            
            emergenctContactView_1.isHidden = true
            heightConstraintEmergencyContact_1.constant = 70
            self.lblContact1Title.text = model.emergName ?? ""
            self.lblContact1Name.text = model.emergRelationship ?? ""
            self.bgContact1New.isHidden = false
            self.bgContact1Height.constant = 70
           
            
            self.txtEmergencyName_1.text = model.emergName ?? ""
            self.txtPhoneNo_1.text = model.emergPhone ?? ""
            self.txtEmergencyRelative_1.text = model.emergRelationship ?? ""
            
            
            
            if model.emergName2 != "" {
                self.isExpand_2 = true
                emergenctContactView_2.isHidden = false
                heightConstraintEmergencyContact_2.constant = 240
                btnEmergencyExpand_2.setImage(UIImage(named: "dropUp"), for: .normal)
                self.btnEmergencyExpand_2.isHidden = true
                self.lblEmergencyContact_2_Title.isHidden = false
                
                emergenctContactView_2.isHidden = true
                heightConstraintEmergencyContact_2.constant = 70
                self.lblContact2Title.text = model.emergName2 ?? ""
                self.lblContact2Name.text = model.emergRelationship2 ?? ""
                self.bgContact2New.isHidden = false
                self.bgContact2newHeight.constant = 70
            }
            else {
                self.isExpand_2 = false
                emergenctContactView_2.isHidden = true
                heightConstraintEmergencyContact_2.constant = 0
                self.lblEmeContact2Top.constant = 0
                self.lblEmeTitle2Height.constant = 0
                btnEmergencyExpand_2.setImage(UIImage(named: "dropDown"), for: .normal)
                self.btnEmergencyExpand_2.isHidden = true
                self.lblEmergencyContact_2_Title.isHidden = true
                
                emergenctContactView_2.isHidden = true
                heightConstraintEmergencyContact_2.constant = 0
                self.lblContact2Title.text = model.emergName2 ?? ""
                self.lblContact2Name.text = model.emergRelationship2 ?? ""
                self.bgContact2New.isHidden = true
                self.bgContact2newHeight.constant = 0
            }
            
            
            
            self.txtEmergencyName_2.text = model.emergName2 ?? ""
            self.txtPhoneNo_2.text = model.emergPhone2 ?? ""
            self.txtEmergencyRelative_2.text = model.emergRelationship2 ?? ""
            
            
            if model.emergName3 != "" {
                self.isExpand_3 = true
                emergenctContactView_3.isHidden = false
                heightConstraintEmergencyContact_3.constant = 240
                btnEmergencyExpand_3.setImage(UIImage(named: "dropUp"), for: .normal)
                self.btnEmergencyExpand_3.isHidden = true
                self.lblEmergencyContact_3_Title.isHidden = false
                
                emergenctContactView_3.isHidden = true
                heightConstraintEmergencyContact_3.constant = 70
                self.lblContact3Title.text = model.emergName3 ?? ""
                self.lblConatct3Name.text = model.emergRelationship3 ?? ""
                self.bgContact3New.isHidden = false
                self.bgContact3NewHeight.constant = 70
            }
            else {
                self.isExpand_3 = false
                emergenctContactView_3.isHidden = true
                heightConstraintEmergencyContact_3.constant = 0
                self.lblEmeContact3Top.constant = 0
                self.lblEmeTitle3Height.constant = 0
                btnEmergencyExpand_3.setImage(UIImage(named: "dropDown"), for: .normal)
                self.btnEmergencyExpand_3.isHidden = true
                self.lblEmergencyContact_3_Title.isHidden = true
                self.lblPrimaryTop.constant = 0
                self.lblEmergencyContactTop.constant = 0
                
                emergenctContactView_3.isHidden = true
                heightConstraintEmergencyContact_3.constant = 0
                self.lblContact3Title.text = model.emergName3 ?? ""
                self.lblConatct3Name.text = model.emergRelationship3 ?? ""
                self.bgContact3New.isHidden = true
                self.bgContact3NewHeight.constant = 0
            }
            
            
            
            self.txtEmergencyName_3.text = model.emergName3 ?? ""
            self.txtPhoneNo_3.text = model.emergPhone3 ?? ""
            self.txtEmergencyRelative_3.text = model.emergRelationship3 ?? ""
            
            let name = model.physicianName ?? ""
            let ph = model.physicianPhone ?? ""
            
           
            
            if name == "" && ph == "" {
                self.lblPrimaryTop.constant = 0
                self.lblPrimaryTitleHeight.constant = 0
                self.lblPrimaryTitleBottom.constant = 0
                self.bgPrimaryPhHeight.constant = 0
                self.bgPrimaryNameHeight.constant = 0
                self.stckPrimary.isHidden = true
                self.stackPrimayHeight.constant = 0
            }
            else {
                
                if name != "" {
                    self.txtPhysicianName.text = model.physicianName ?? ""
                    self.stackPrimayHeight.constant = 115
                    self.bgPrimaryNameHeight.constant = 50
                    self.bgPrimaryPhHeight.constant = 50
                }
                if ph != "" {
                    self.txtPhysicianPhoneNo.text = model.physicianPhone ?? ""
                    self.stackPrimayHeight.constant = 115
                    self.bgPrimaryNameHeight.constant = 50
                    self.bgPrimaryPhHeight.constant = 50
                }
                 
            }
            
            if model.dateOfBirth == "" {
                self.lblAgeTitle.isHidden = true
                self.lblDOBTop.constant = 0
                self.lblDOBBottom.constant = 0
                self.lblDOBHeight.constant = 0
                self.stackDOBHeight.constant = 0
                self.stackDOB.isHidden = true
                self.bgdateOfBirthNew.isHidden = true
               
            }
            else {
                
                self.lblDOBTop.constant = 35
                self.lblDOBBottom.constant = 15
                self.lblDOBHeight.constant = 25
                self.stackDOBHeight.constant = 50
                self.dateOfBirth = model.dateOfBirth ?? ""
            }
            
            
            if self.dateOfBirth != "" {
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "YYYY-MM-dd"
                let date = dateFormatter1.date(from:self.dateOfBirth)!
                
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "YYYY-MM-dd"
                let showDate = inputFormatter.date(from: self.dateOfBirth)
                inputFormatter.dateFormat = "MM/dd/yyyy"
                let resultString = inputFormatter.string(from: showDate!)
               
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                let year: String = dateFormatter.string(from: date)
                self.lblYear.text = year
                dateFormatter.dateFormat = "MM"
                let month: String = dateFormatter.string(from: date)
                self.lblMonth.text = month
                dateFormatter.dateFormat = "dd"
                let day: String = dateFormatter.string(from: date)
                self.lblDay.text = day
                
                self.bgdateOfBirthNew.isHidden = false
                self.lblAgeTitle.isHidden = false
                self.lblDayNew.text = day
                self.lblYearnew.text = year
                
                let now = date
                let dateFormatternow = DateFormatter()
                dateFormatternow.dateFormat = "LLLL"
                let nameOfMonth = dateFormatter.string(from: now)
                print(nameOfMonth)
                
                
                let nameFormatter = DateFormatter()
                nameFormatter.dateFormat = "MMMM"

                let name = nameFormatter.string(from: date)
                let index = Calendar.current.component(.month, from: date)

                print(name)  // April
                print(index) // 4
                
                self.lblMonthNew.text = name
                
                self.btnDOB.isHidden = true
                self.lblAge.text = "\(self.calcAge(birthday: resultString))"
            }
            
           
            self.heightSlider.value = Float(model.height) ?? 1
            self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.normal)
            self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.selected)
            
            self.bgHeightNew.isHidden = false
            self.lblHeightNew.text = model.height ?? ""
            self.bgHeightNewHeight.constant = 50
            
            self.weightSlider.value = Float(model.weight) ?? 1
            self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.normal)
            self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.selected)
            
            self.bgWeightNew.isHidden = false
            self.lblWeightNew.text = model.weight ?? ""
            self.bgWeightHeight.constant = 50
            
            self.bloodGroup = model.bloodGroup ?? ""
            self.lblBloodGroupNew.text = model.bloodGroup ?? ""
            
            self.bgBloodGroupNew.isHidden = false
            self.bgBloodGroupNewHeight.constant = 50
            self.bgBloodGroupOld.isHidden = true
            self.bgBloodGroupOldHeight.constant = 0
            
            if bloodGroup == "A+" {
                self.btn1((UIButton).self)
            }
            else if bloodGroup == "A-" {
                self.btn2((UIButton).self)
            }
            else if bloodGroup == "B+" {
                self.btn3((UIButton).self)
            }
            else if bloodGroup == "B-" {
                self.btn4((UIButton).self)
            }
            else if bloodGroup == "O+" {
                self.btn5((UIButton).self)
            }
            else if bloodGroup == "O-" {
                self.btn6((UIButton).self)
            }
            else if bloodGroup == "AB+" {
                self.btn7((UIButton).self)
            }
            else if bloodGroup == "AB-" {
                self.btn8((UIButton).self)
            }
            else if bloodGroup == "Hh" {
                self.btn9((UIButton).self)
            }
            else if bloodGroup == "Don't Know" {
                self.btn10((UIButton).self)
            }
           
            self.txtBloodPressure.text = model.bloodPressure ?? ""
            self.txtDiabetes.text = model.diabetes ?? ""
            
            self.bgHeartProblemNew.isHidden = false
            self.bgHeartProblemHeight.constant = 50
            self.bgHeartProblemOld.isHidden = true
            self.lblHeartProblemTitleOld.isHidden = true
            
            if model.heartProblem == "1" {
                radioHeartYesImage.image = UIImage(named: "radioFill")
                radioHeartNoImage.image = UIImage(named: "radio")
                radioHeartPreferSayNoImage.image = UIImage(named: "radio")
                self.isheartProblem = "1"
                self.lblHeartProblemNew.text = "YES"
            }
            else if model.heartProblem == "2" {
                radioHeartYesImage.image = UIImage(named: "radio")
                radioHeartNoImage.image = UIImage(named: "radio")
                radioHeartPreferSayNoImage.image = UIImage(named: "radioFill")
                self.isheartProblem = "2"
                self.lblHeartProblemNew.text = "Prefer not to say"
            }
            else {
                self.isheartProblem = "0"
                self.lblHeartProblemNew.text = "NO"
                radioHeartYesImage.image = UIImage(named: "radio")
                radioHeartNoImage.image = UIImage(named: "radioFill")
                radioHeartPreferSayNoImage.image = UIImage(named: "radio")
            }
            
            self.txtDrugAlergy.text = model.drugAllergy ?? ""
            if model.drugAllergy == "" {
                self.bgDurgAllergy.isHidden = true
                self.durgAllergyheight.constant = 0
                self.lblDrugAllergyTop.constant = 0
                self.lblDurgAllergyBottom.constant = 0
                self.lblDurgAllergyHeight.constant = 0
                
            }
            else {
                self.bgDurgAllergy.isHidden = false
                self.durgAllergyheight.constant = 70
                self.lblDrugAllergyTop.constant = 20
                self.lblDurgAllergyBottom.constant = 20
                self.lblDurgAllergyHeight.constant = 25
            }
            
            
            
            self.txtOtherAlergy.text = model.otherAllergy ?? ""
            if model.otherAllergy == "" {
                self.bgOtherView.isHidden = true
                self.OtherViewheight.constant = 0
                self.lblOtherTop.constant = 0
                self.lblOtherTitleHeight.constant = 0
                self.lblOtherBottom.constant = 0
                
            }
            else {
                self.bgOtherView.isHidden = false
                self.OtherViewheight.constant = 70
                self.lblOtherTop.constant = 20
                self.lblOtherBottom.constant = 20
                self.lblOtherTitleHeight.constant = 25
            }
            
            
            self.txtMedication.text = model.medication ?? ""
            if model.medication == "" {
                self.bgMedicationView.isHidden = true
                self.bgMedicationHeight.constant = 0
                self.lblMedicationTop.constant = 0
                self.lblMedicationBottom.constant = 0
                self.lblmedicationHeight.constant = 0
                
            }
            else {
                self.bgMedicationView.isHidden = false
                self.bgMedicationHeight.constant = 70
                self.lblMedicationTop.constant = 20
                self.lblMedicationBottom.constant = 20
                self.lblmedicationHeight.constant = 25
            }
           
            
            self.isOrgan = model.isOrganDonor ?? ""
            self.bgOrganDonorNew.isHidden = false
            self.bgOrganDonorHeight.constant = 50
            self.bgOrganDonorOld.isHidden = true
            self.lblOrganDonorTitleOld.isHidden = true
            
            
            if model.isOrganDonor == "1" {
                radioDonnerYesImage.image = UIImage(named: "radioFill")
                radioDonnertNoImage.image = UIImage(named: "radio")
                self.lblOrganDonorNew.text = "YES"
            }
            else if model.isOrganDonor == "2" {
                radioDonnerYesImage.image = UIImage(named: "radio")
                radioDonnertNoImage.image = UIImage(named: "radio")
                radioDonnerPreferSayNoImage.image = UIImage(named: "radioFill")
                self.lblOrganDonorNew.text = "Prefer not to say"
            }
            else {
                self.lblOrganDonorNew.text = "NO"
                radioDonnerYesImage.image = UIImage(named: "radio")
                radioDonnertNoImage.image = UIImage(named: "radioFill")
            }
            
            
            
            self.isHealth = model.isHealthInsurance ?? ""
            self.bgHealthInsuNew.isHidden = false
            self.bgHealthInsuHeight.constant = 50
            self.bgHealthInsuOld.isHidden = true
            self.lblHealthInsuOld.isHidden = true
            
            if self.isHealth == "1" {
                self.bgHealthInsurance.isHidden = false
                self.healthInsuranceHeight.constant = 180
                self.txtCompnyName.text = model.companyName ?? ""
                self.txtPolicyNumber.text = model.policyNumber ?? ""
                self.txtOtherDetails.text = model.otherDetails ?? ""
                radioInsuranceYesImage.image = UIImage(named: "radioFill")
                radioInsuranceNoImage.image = UIImage(named: "radio")
                self.lblHealthInsuranceNew.text = "YES"
            }
            else if self.isHealth == "2" {
                self.txtCompnyName.text = ""
                self.txtPolicyNumber.text = ""
                self.txtOtherDetails.text = ""
                self.bgHealthInsurance.isHidden = true
                self.healthInsuranceHeight.constant = 0
                radioInsuranceYesImage.image = UIImage(named: "radio")
                radioInsuranceNoImage.image = UIImage(named: "radio")
                radioInsurancePreferSayNoImage.image = UIImage(named: "radioFill")
                self.lblHealthInsuranceNew.text = "Prefer not to say"
            }
            else {
                self.txtCompnyName.text = ""
                self.txtPolicyNumber.text = ""
                self.txtOtherDetails.text = ""
                self.bgHealthInsurance.isHidden = true
                self.healthInsuranceHeight.constant = 0
                radioInsuranceYesImage.image = UIImage(named: "radio")
                radioInsuranceNoImage.image = UIImage(named: "radioFill")
                self.lblHealthInsuranceNew.text = "NO"
            }
              
            self.btnSubmit.isHidden = true
            self.btnSubmitHeight.constant = 0
            imgDiabetes.isHidden = true
            btnDiabetes.isHidden = true
            btnBloodPresure.isHidden = true
            imgBloodPressure.isHidden = true
            
            self.txtName.isUserInteractionEnabled = false
            self.txtAddressView.isUserInteractionEnabled = false
            self.txtEmergencyRelative_1.isUserInteractionEnabled = false
            self.txtEmergencyRelative_2.isUserInteractionEnabled = false
            self.txtEmergencyRelative_3.isUserInteractionEnabled = false
            self.txtEmergencyName_1.isUserInteractionEnabled = false
            self.txtPhoneNo_1.isUserInteractionEnabled = false
            self.txtEmergencyName_2.isUserInteractionEnabled = false
            self.txtPhoneNo_2.isUserInteractionEnabled = false
            self.txtEmergencyName_3.isUserInteractionEnabled = false
            self.txtPhoneNo_3.isUserInteractionEnabled = false
            self.btnContact1.isUserInteractionEnabled = false
            self.btnContact1Ph.isUserInteractionEnabled = false
            self.btnContact2.isUserInteractionEnabled = false
            self.btnContactPh.isUserInteractionEnabled = false
            self.btnContact3.isUserInteractionEnabled = false
            self.btnContact3Ph.isUserInteractionEnabled = false
            self.txtPhysicianPhoneNo.isUserInteractionEnabled = false
            self.txtPhysicianName.isUserInteractionEnabled = false
            self.txtBloodPressure.isUserInteractionEnabled = false
            self.txtDiabetes.isUserInteractionEnabled = false
            self.txtDrugAlergy.isUserInteractionEnabled = false
            self.txtMedication.isUserInteractionEnabled = false
            self.txtCompnyName.isUserInteractionEnabled = false
            self.txtPolicyNumber.isUserInteractionEnabled = false
            self.txtOtherDetails.isUserInteractionEnabled = false
            self.txtOtherAlergy.isUserInteractionEnabled = false
        }
    }
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        
        dateFormater.dateFormat = "MM/dd/yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    func initialConfig(){

        emergenctContactView_1.isHidden = true
        heightConstraintEmergencyContact_1.constant = 0
        btnEmergencyExpand_1.setImage(UIImage(named: "dropDown"), for: .normal)
        
        self.bgContact1New.isHidden = true
        self.bgContact1Height.constant = 0
        
        emergenctContactView_2.isHidden = true
        heightConstraintEmergencyContact_2.constant = 0
        btnEmergencyExpand_2.setImage(UIImage(named: "dropDown"), for: .normal)
        
        bgContact2New.isHidden = true
        bgContact2newHeight.constant = 0
        
        emergenctContactView_3.isHidden = true
        heightConstraintEmergencyContact_3.constant = 0
        btnEmergencyExpand_3.setImage(UIImage(named: "dropDown"), for: .normal)
        
        bgContact3New.isHidden = true
        bgContact3NewHeight.constant = 0
        
        self.bgdateOfBirthNew.isHidden = true
        self.lblAgeTitle.isHidden = true
        
        self.bgHeightNew.isHidden = true
        self.bgWeightNew.isHidden = true
        self.bgBloodGroupNew.isHidden = true
        self.bgHeartProblemNew.isHidden = true
        self.bgOrganDonorNew.isHidden = true
        self.bgHealthInsuNew.isHidden = true
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 1
        
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 5
        
        scroll_View.contentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH, height: 1000)
        
        self.heightSlider.setMinimumTrackImage(minColor, for: UIControl.State.normal)
        self.heightSlider.setMinimumTrackImage(minColor, for: UIControl.State.selected)
        self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.normal)
        self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.selected)
        
        self.weightSlider.setMinimumTrackImage(minColor, for: UIControl.State.normal)
        self.weightSlider.setMinimumTrackImage(minColor, for: UIControl.State.selected)
        self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.normal)
        self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.selected)
    }
    
    @IBAction func btnAddRecords(_ sender: Any) {
        
        self.bgTutorial.isHidden = true
        self.mainView.isHidden = false
        
        self.bloodGroup = ""
        self.v1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        DispatchQueue.main.async {
            self.initialConfig()
        }
        
        self.isFromMenu = false
        
    }
    
    
    
    
    
    @IBAction func btnCall3(_ sender: Any) {
        
        let string = self.txtPhoneNo_3.text!
        let formattedString = string.replacingOccurrences(of: " ", with: "")
        self.makeAPhoneCall(number: formattedString)
    }
    
    @IBAction func btnSms3(_ sender: Any) {
        displayMessageInterface(mobNum: self.txtPhoneNo_3.text!)
    }
    
    @IBAction func btnLocation3(_ sender: Any) {
        
        if LocationSms == "" {
            self.showToastMessage(message: "Please enable location service for send your current location")
        }
        else {
            displayMessageInterfaceLocation(mobNum: self.txtPhoneNo_3.text!, location: LocationSms)
        }
    }
    
    @IBAction func btnCall2(_ sender: Any) {
        
        let string = self.txtPhoneNo_2.text!
        let formattedString = string.replacingOccurrences(of: " ", with: "")
        self.makeAPhoneCall(number: formattedString)
    }
    
    @IBAction func btnsms2(_ sender: Any) {
        displayMessageInterface(mobNum: self.txtPhoneNo_2.text!)
    }
    @IBAction func btnLocation2(_ sender: Any) {
        
        if LocationSms == "" {
            self.showToastMessage(message: "Please enable location service for send your current location")
        }
        else {
            displayMessageInterfaceLocation(mobNum: self.txtPhoneNo_2.text!, location: LocationSms)
        }
    }
    
    @IBAction func btnLocation(_ sender: Any) {
        
        if LocationSms == "" {
            self.showToastMessage(message: "Please enable location service for send your current location")
        }
        else {
            displayMessageInterfaceLocation(mobNum: self.txtPhoneNo_1.text!, location: LocationSms)
        }
    }
    
    @IBAction func btnConatct1Call(_ sender: Any) {
        
        let string = self.txtPhoneNo_1.text!
        let formattedString = string.replacingOccurrences(of: " ", with: "")
        self.makeAPhoneCall(number: formattedString)
    }
    
    @IBAction func btnSMSContact1(_ sender: Any) {
        displayMessageInterface(mobNum: self.txtPhoneNo_1.text!)
    }
    
    @IBAction func btnCallPrimeryPhy(_ sender: Any) {
        self.makeAPhoneCall(number: self.txtPhysicianPhoneNo.text!)
    }
    
    func makeAPhoneCall(number:String) {
        let url: NSURL = URL(string: "TEL://\(number)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
      }
    
    func displayMessageInterface(mobNum : String) {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = [mobNum]
        composeVC.body = "I have added you as my emergency contact in ICE Buddy App. You should also try this lifesaving app: https://iceb.app"
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func displayMessageInterfaceLocation(mobNum : String,location:String) {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = [mobNum]
        composeVC.body = location
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    
    @IBAction func btnNoNeedAlert(_ sender: Any) {
        
        if isFromMenu {
            self.bgAlertView.isHidden = true
            self.callEditQRDataAPI()
        }
        else {
            self.bgAlertView.isHidden = true
            self.callAddQRDataAPI()
        }
    }
    
    @IBAction func btnOkAlert(_ sender: Any) {
        
        if isFromMenu {
            displayMessageInterface(mobNum: self.txtPhoneNo_1.text!)
            self.bgAlertView.isHidden = true
            self.callEditQRDataAPI()
        }
        else {
            displayMessageInterface(mobNum: self.txtPhoneNo_1.text!)
            self.bgAlertView.isHidden = true
            self.callAddQRDataAPI()
        }
        
    }
    
    @IBAction func btn1(_ sender: Any) {
        
        self.bloodGroup = "A+"
        self.v1.backgroundColor = #colorLiteral(red: 0.3411764706, green: 0.6666666667, blue: 0.9725490196, alpha: 1)
        
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        
        self.lbl1.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    @IBAction func btn2(_ sender: Any) {
        self.bloodGroup = "A-"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 0.3411764706, green: 0.6666666667, blue: 0.9725490196, alpha: 1)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn3(_ sender: Any) {
        self.bloodGroup = "B+"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn4(_ sender: Any) {
        self.bloodGroup = "B-"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn5(_ sender: Any) {
        self.bloodGroup = "O+"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn6(_ sender: Any) {
        self.bloodGroup = "O-"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn7(_ sender: Any) {
        self.bloodGroup = "AB+"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    @IBAction func btn8(_ sender: Any) {
        self.bloodGroup = "AB-"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn9(_ sender: Any) {
        self.bloodGroup = "Hh"
        self.v1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        self.v10.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func btn10(_ sender: Any) {
        self.bloodGroup = "Don't Know"
        self.v1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.v2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v4.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v5.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v6.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v7.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v8.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v9.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.v10.backgroundColor = #colorLiteral(red: 0.3426056504, green: 0.6664619446, blue: 0.9729854465, alpha: 1)
        
        self.lbl1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl3.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl4.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl5.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl6.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl7.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl8.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl9.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.lbl10.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    }
    
    
    @IBAction func btnMenu_Click(_ sender: Any) {
        
        present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    @IBAction func btnCamera_Click(_ sender: Any) {
        let vc = EditQrCodeVC.initVC()
        vc.gotQRId = QRId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnEmergencyExpand_1_Click(_ sender: Any) {
        
        isExpand_1 = !isExpand_1
       
        if isExpand_1{
            emergenctContactView_1.isHidden = false
            heightConstraintEmergencyContact_1.constant = 240
            btnEmergencyExpand_1.setImage(UIImage(named: "dropUp"), for: .normal)
            
            self.bgContact1New.isHidden = true
            self.bgContact1Height.constant = 240
        }else{
            emergenctContactView_1.isHidden = true
            heightConstraintEmergencyContact_1.constant = 0
            btnEmergencyExpand_1.setImage(UIImage(named: "dropDown"), for: .normal)
            
            self.bgContact1New.isHidden = true
            self.bgContact1Height.constant = 0
        }
        
    }
    
    
    @IBAction func btnEmergencyExpand_2_Click(_ sender: Any) {
        
        isExpand_2 = !isExpand_2
        
        if isExpand_2{
            emergenctContactView_2.isHidden = false
            heightConstraintEmergencyContact_2.constant = 240
            btnEmergencyExpand_2.setImage(UIImage(named: "dropUp"), for: .normal)
            self.bgContact2New.isHidden = true
            self.bgContact2newHeight.constant = 240
        }else{
            emergenctContactView_2.isHidden = true
            heightConstraintEmergencyContact_2.constant = 0
            btnEmergencyExpand_2.setImage(UIImage(named: "dropDown"), for: .normal)
            self.bgContact2New.isHidden = true
            self.bgContact2newHeight.constant = 0
        }
        
    }
    
    @IBAction func btnEmergencyExpand_3_Click(_ sender: Any) {
        
        isExpand_3 = !isExpand_3
        
        if isExpand_3{
            emergenctContactView_3.isHidden = false
            heightConstraintEmergencyContact_3.constant = 240
            btnEmergencyExpand_3.setImage(UIImage(named: "dropUp"), for: .normal)
            self.bgContact3New.isHidden = true
            self.bgContact3NewHeight.constant = 240
        }else{
            emergenctContactView_3.isHidden = true
            heightConstraintEmergencyContact_3.constant = 0
            btnEmergencyExpand_3.setImage(UIImage(named: "dropDown"), for: .normal)
            self.bgContact3New.isHidden = true
            self.bgContact3NewHeight.constant = 0
        }
        
    }
    
    
    @IBAction func btnDOB_Click(_ sender: Any) {
        
        let vc = StartdatePickerVC.initVC()
        vc.isFromDOB = true
        vc.modalPresentationStyle = .overCurrentContext
        
        vc.completionHandler = {(str) in
            self.dateOfBirth = str
        }
        vc.completionHandler1 = {(str) in
            self.lblDay.text = str
        }
        vc.completionHandler2 = {(str) in
            self.lblMonth.text = str
        }
        vc.completionHandler3 = {(str) in
            self.lblYear.text = str
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: Slider
    
    @IBAction func changeHeightSlider(_ sender: Any) {
        
        self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.normal)
        self.heightSlider.setThumbImage(self.progressImage(with: self.heightSlider.value), for: UIControl.State.selected)
        
        print(heightSlider.value)
        
    }
    
    @IBAction func changeWeightSlider(_ sender: Any) {
        
        self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.normal)
        self.weightSlider.setThumbImage(self.progressImage1(with: self.weightSlider.value), for: UIControl.State.selected)
    }
    
    func progressImage(with progress : Float) -> UIImage {
        
        let layer = CALayer()
        layer.backgroundColor = Utilities.colorWithHexString(hex: "4897F6").cgColor
        layer.frame = CGRect(x: 0, y: 0, width: 75, height: 30)
        layer.cornerRadius = 15

        let label = UILabel(frame: layer.frame)
        label.text = "\(Int(progress)) cm"
        label.textColor = UIColor.white
        layer.addSublayer(label.layer)
        label.textAlignment = .center
        label.tag = 100

        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func progressImage1(with progress : Float) -> UIImage {
        
        let layer = CALayer()
        layer.backgroundColor = Utilities.colorWithHexString(hex: "4897F6").cgColor
        layer.frame = CGRect(x: 0, y: 0, width: 75, height: 30)
        layer.cornerRadius = 15

        let label = UILabel(frame: layer.frame)
        label.text = "\(Int(progress)) kg"
        label.textColor = UIColor.white
        layer.addSublayer(label.layer)
        label.textAlignment = .center
        label.tag = 100

        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
        
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        @unknown default:
            print("Error")
        }
    }

    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alertController = UIAlertController (title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)

            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in

                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }

                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)

            present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnContact2(_ sender: Any) {
        self.isContact1 = false
        self.isContact2 = true
        self.isContact3 = false
        let contacVC = CNContactPickerViewController()
        contacVC.delegate = self
        self.present(contacVC, animated: true, completion: nil)
    }
    @IBAction func btnContact3(_ sender: Any) {
        self.isContact1 = false
        self.isContact2 = false
        self.isContact3 = true
        let contacVC = CNContactPickerViewController()
        contacVC.delegate = self
        self.present(contacVC, animated: true, completion: nil)
    }
    @IBAction func btnContact1(_ sender: Any) {
        
        self.isContact1 = true
        self.isContact2 = false
        self.isContact3 = false
        
        let contacVC = CNContactPickerViewController()
        contacVC.delegate = self
        self.present(contacVC, animated: true, completion: nil)
        
    }
    
    // MARK: Delegate method CNContectPickerDelegate
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        let numbers = contact.phoneNumbers.first
        let fname = contact.givenName
        let lname = contact.familyName
        
        if isContact1 {
            
            self.txtPhoneNo_1.text = numbers?.value.stringValue
            self.txtEmergencyName_1.text = fname + lname
        }
        else if isContact2 {
            
            self.txtPhoneNo_2.text = numbers?.value.stringValue
            self.txtEmergencyName_2.text = fname + lname
        }
        else {
            
            self.txtPhoneNo_3.text = numbers?.value.stringValue
            self.txtEmergencyName_3.text = fname + lname
        }
        
        
    }

      func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
          self.dismiss(animated: true, completion: nil)
      }
    
    // MARK: DropDowns
    @IBAction func btnBloodPressure_Click(_ sender: Any) {

        Utilities.openDropDown(sender: sender as! AnchorView, arrData: ["High","Low", "Normal","Prefer not to say"], width: 150, completion: { index, item in

            self.txtBloodPressure.text = item

        })
    }
    
    @IBAction func btnDiabetes_Click(_ sender: Any) {
        
        Utilities.openDropDown(sender: sender as! AnchorView, arrData: ["High","Low", "Normal","Prefer not to say"], width: 150, completion: { index, item in

            self.txtDiabetes.text = item
        })
        
    }
    
    @IBAction func btnYes_Click(_ sender: Any) {
        
        if (sender as AnyObject).tag == 100 {
            self.isheartProblem = "1"
            radioHeartYesImage.image = UIImage(named: "radioFill")
            radioHeartNoImage.image = UIImage(named: "radio")
            radioHeartPreferSayNoImage.image = UIImage(named: "radio")
        }
        
        if (sender as AnyObject).tag == 200 {
            self.isOrgan = "1"
            radioDonnerYesImage.image = UIImage(named: "radioFill")
            radioDonnertNoImage.image = UIImage(named: "radio")
            radioDonnerPreferSayNoImage.image = UIImage(named: "radio")
        }
        
        
        if (sender as AnyObject).tag == 300 {
            self.isHealth = "1"
            self.bgHealthInsurance.isHidden = false
            self.healthInsuranceHeight.constant = 180
            radioInsuranceYesImage.image = UIImage(named: "radioFill")
            radioInsuranceNoImage.image = UIImage(named: "radio")
            radioInsurancePreferSayNoImage.image = UIImage(named: "radio")
        }
       
        
    }
    
    @IBAction func btnNo_Click(_ sender: Any) {
        
        if (sender as AnyObject).tag == 101 {
            self.isheartProblem = "0"
            radioHeartYesImage.image = UIImage(named: "radio")
            radioHeartNoImage.image = UIImage(named: "radioFill")
            radioHeartPreferSayNoImage.image = UIImage(named: "radio")
        }
        
        if (sender as AnyObject).tag == 201 {
            self.isOrgan = "0"
            radioDonnerYesImage.image = UIImage(named: "radio")
            radioDonnertNoImage.image = UIImage(named: "radioFill")
            radioDonnerPreferSayNoImage.image = UIImage(named: "radio")
        }
        
        
        if (sender as AnyObject).tag == 301 {
            self.isHealth = "0"
            self.bgHealthInsurance.isHidden = true
            self.healthInsuranceHeight.constant = 0
            radioInsuranceYesImage.image = UIImage(named: "radio")
            radioInsuranceNoImage.image = UIImage(named: "radioFill")
            radioInsurancePreferSayNoImage.image = UIImage(named: "radio")
        }
        
    }
    
    @IBAction func btnPreferNotSay_Click(_ sender: Any) {
       
        if (sender as AnyObject).tag == 102 {
            self.isheartProblem = "2"
            radioHeartYesImage.image = UIImage(named: "radio")
            radioHeartNoImage.image = UIImage(named: "radio")
            radioHeartPreferSayNoImage.image = UIImage(named: "radioFill")
        }
        
        if (sender as AnyObject).tag == 202 {
            self.isOrgan = "2"
            radioDonnerYesImage.image = UIImage(named: "radio")
            radioDonnertNoImage.image =  UIImage(named: "radio")
            radioDonnerPreferSayNoImage.image = UIImage(named: "radioFill")
        }
        
        
        if (sender as AnyObject).tag == 302 {
            self.isHealth = "2"
            radioInsuranceYesImage.image = UIImage(named: "radio")
            radioInsuranceNoImage.image = UIImage(named: "radio")
            radioInsurancePreferSayNoImage.image = UIImage(named: "radioFill")
        }
    }

    @IBAction func btnSubmit(_ sender: Any) {
        
        let msg = self.validation()
        if msg == "" {
            
            self.bgAlertView.isHidden = false
            
        }else{
            self.showToastMessage(message: msg)
        }
        
    }
    
    func validation() -> String {
        
        self.view.endEditing(true)
        var msg = String()
        if txtName.text == "" {
            msg = kEmptyUserName
        }
        else if txtAddressView.text  == "" {
            msg = kAddress
        }
        else if txtEmergencyName_1.text  == "" {
            msg = kEname
        }
        else if txtPhoneNo_1.text  == "" {
            msg = kENum
        }
        else if txtPhoneNo_1.text! != "" && txtPhoneNo_1.text!.count < 10 {
            txtPhoneNo_1.becomeFirstResponder()
            msg = "Phone number length should be 10-15 characters."
        }
        else if txtEmergencyRelative_1.text  == "" {
            msg = kERel
        }
        else if txtEmergencyName_2.text  != "" {
            
            if txtEmergencyRelative_2.text  == "" {
                msg = kERel
            }
            else {
               
               if txtEmergencyName_3.text  != "" {
                    
                    if txtEmergencyRelative_3.text  == "" {
                        msg = kERel
                    }
                    else {
                        if self.bloodGroup  == "" {
                            msg = kBlood
                        }
                        else if self.txtBloodPressure.text!  == "" {
                            msg = kBloodPressure
                        }
                        else if txtDiabetes.text  == "" {
                            msg = kDiabites
                        }
                        else if self.isheartProblem == "" {
                            msg = kHeartProblem
                        }
                        else if self.isOrgan == "" {
                            msg = kOrgan
                        }
                        else if self.isHealth == "" {
                            msg = kHealth
                        }
                        else if self.isHealth == "1" {
                            
                            if self.txtCompnyName.text == "" {
                                msg = kcompanyname
                            }
                            else if self.txtPolicyNumber.text == "" {
                                msg = kpolicynumber
                            }
                        }
                    }
                }

            }
        }
        
        
        return msg
    }
    
}

extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrBloodGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bloodCell", for: indexPath) as! bloodCell
        
        cell.lblBloodGroup.text = arrBloodGroup[indexPath.row]
        cell.lblBloodGroup.font = UIFont(name: "Lato-Medium", size: 13)
        if selectedGroup == indexPath.row {
            cell.lblBloodGroup.textColor = UIColor.white
            cell.bgView.backgroundColor  = Utilities.colorWithHexString(hex: "4897F6")
        }else{
            cell.lblBloodGroup.textColor = UIColor.black
            cell.bgView.backgroundColor  = Utilities.colorWithHexString(hex: "FFFFFF")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        _ = collectionView.dequeueReusableCell(withReuseIdentifier: "bloodCell", for: indexPath) as! bloodCell
        selectedGroup = 0
        selectedGroup = indexPath.row
        let group = arrBloodGroup[indexPath.row]
        print(group)
        collectionView.reloadData()
    }
    
}


//MARK:- WebService Call Method
extension HomeVC {
    
    func callAddQRDataAPI() {
        self.view.endEditing(true)
        
        if self.self.isHealth == "0" {
            self.txtCompnyName.text = ""
            self.txtPolicyNumber.text = ""
            self.txtOtherDetails.text = ""
        }
        
        let param : NSMutableDictionary = ["name" : self.txtName.text!,
                                           "address":self.txtAddressView.text!,
                                           "emerg_name":self.txtEmergencyName_1.text!,
                                           "emerg_phone":self.txtPhoneNo_1.text!,
                                           "emerg_relationship":self.txtEmergencyRelative_1.text!,
                                           "emerg_name_2":self.txtEmergencyName_2.text!,
                                           "emerg_phone_2":self.txtPhoneNo_2.text!,
                                           "emerg_relationship_2":self.txtEmergencyRelative_2.text!,
                                           "emerg_name_3":self.txtEmergencyName_3.text!,
                                           "emerg_phone_3":self.txtPhoneNo_3.text!,
                                           "emerg_relationship_3":self.txtEmergencyRelative_3.text!,
                                           "physician_name":self.txtPhysicianName.text!,
                                           "physician_phone":self.txtPhysicianPhoneNo.text!,
                                           "date_of_birth":self.dateOfBirth,
                                           "height":heightSlider.value,
                                           "weight":weightSlider.value,
                                           "blood_group":self.bloodGroup,
                                           "blood_pressure":self.txtBloodPressure.text!,
                                           "diabetes":self.txtDiabetes.text!,
                                           "heart_problem":self.isheartProblem,
                                           "drug_allergy":self.txtDrugAlergy.text!,
                                           "other_allergy":self.txtOtherAlergy.text!,
                                           "medication":self.txtMedication.text!,
                                           "is_organ_donor":self.isOrgan,
                                           "is_health_insurance":self.isHealth,
                                           "company_name":self.txtCompnyName.text!,
                                           "policy_number":self.txtPolicyNumber.text!,
                                           "other_details":self.txtOtherDetails.text!,
                                           "device_type":"1",
                                           "device_token":Constant.kDeviceToken,
                                           "user_id":ModelUserData.getUserDataFromUserDefaults().id ?? "",
                                           "api_token":ModelUserData.getUserDataFromUserDefaults().apiToken ?? ""]
     
        HttpRequestManager.requestWithDefaultCalling(service: kadd_qr_code_URL, parameters: param, showLoader: true) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callAddQRDataAPI()
                })
                return
            }
            else {
                
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    
                    let QrData = response?.value(forKey: "qr_code_data") as! NSMutableDictionary
                    
                    let modelQR : ModelQrCodeData = ModelQrCodeData(fromDictionary: QrData as! [String : Any])
                    
                    modelQR.saveToUserDefaults()
                    
                    let data = response?.value(forKey: "user_data") as! NSMutableDictionary
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: data as! [String : Any])
                    model.saveToUserDefaults()
                    
                    self.setData()
                   
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
            }
        }
    }
    
    func callEditQRDataAPI() {
        
        self.view.endEditing(true)
        
        if self.self.isHealth == "0" {
            self.txtCompnyName.text = ""
            self.txtPolicyNumber.text = ""
            self.txtOtherDetails.text = ""
        }
        
        
        let param : NSMutableDictionary = ["name" : self.txtName.text!,
                                           "address":self.txtAddressView.text!,
                                           "emerg_name":self.txtEmergencyName_1.text!,
                                           "emerg_phone":self.txtPhoneNo_1.text!,
                                           "emerg_relationship":self.txtEmergencyRelative_1.text!,
                                           "emerg_name_2":self.txtEmergencyName_2.text!,
                                           "emerg_phone_2":self.txtPhoneNo_2.text!,
                                           "emerg_relationship_2":self.txtEmergencyRelative_2.text!,
                                           "emerg_name_3":self.txtEmergencyName_3.text!,
                                           "emerg_phone_3":self.txtPhoneNo_3.text!,
                                           "emerg_relationship_3":self.txtEmergencyRelative_3.text!,
                                           "physician_name":self.txtPhysicianName.text!,
                                           "physician_phone":self.txtPhysicianPhoneNo.text!,
                                           "date_of_birth":self.dateOfBirth,
                                           "height":"\(heightSlider.value)",
                                           "weight":"\(weightSlider.value)",
                                           "blood_group":self.bloodGroup,
                                           "blood_pressure":self.txtBloodPressure.text!,
                                           "diabetes":self.txtDiabetes.text!,
                                           "heart_problem":self.isheartProblem,
                                           "drug_allergy":self.txtDrugAlergy.text!,
                                           "other_allergy":self.txtOtherAlergy.text!,
                                           "medication":self.txtMedication.text!,
                                           "is_organ_donor":self.isOrgan,
                                           "is_health_insurance":self.isHealth,
                                           "company_name":self.txtCompnyName.text!,
                                           "policy_number":self.txtPolicyNumber.text!,
                                           "other_details":self.txtOtherDetails.text!,
                                           "device_type":"1",
                                           "device_token":Constant.kDeviceToken,
                                           "user_id":ModelUserData.getUserDataFromUserDefaults().id ?? "",
                                           "api_token":ModelUserData.getUserDataFromUserDefaults().apiToken ?? ""]
        
        
        let url = kedit_qr_code_URL + "/" + self.QRId
        
        HttpRequestManager.requestWithDefaultCalling(service: url, parameters: param, showLoader: true) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callEditQRDataAPI()
                })
                return
            }
            else {
                
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    
                    let QrData = response?.value(forKey: "qr_code_data") as! NSMutableDictionary
                    
                    let modelQR : ModelQrCodeData = ModelQrCodeData(fromDictionary: QrData as! [String : Any])
                    
                    modelQR.saveToUserDefaults()
                    
                    let data = response?.value(forKey: "user_data") as! NSMutableDictionary
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: data as! [String : Any])
                    model.saveToUserDefaults()
                    
                    self.setData()
                   
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
            }
        }
    }
}
