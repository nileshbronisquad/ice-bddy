//
//  ForgotPasswordVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit

class ForgotPasswordVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var forgotPasswordView: UIView!
    
    // MARK:- Create self View Controller object
    class func initVC() -> ForgotPasswordVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        forgotPasswordView.roundCorners([.topLeft, .topRight], radius: 30.0, width: AppConstants.ScreenSize.SCREEN_WIDTH)
        Utilities.dropShadow(view: mainView)
       
    }
    
    // MARK:- UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtEmail {
            textField.resignFirstResponder()
            btnResetPassword_Click(UIButton())
        }
        return true
    }
    
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtEmail.text == "" { // Mobile
            msg = kEmailempty
        }
        else if txtEmail.text?.isValidEmail() ?? false == false {
            msg = kInvalidEmail
        }
        return msg
    }
    
    

    @IBAction func btnBack_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnResetPassword_Click(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.callForgotAPI()

        }else{
            self.showToastMessage(message: msg)
        }
        
    }
    
}

//MARK:- WebService Call Method
@available(iOS 10.0, *)
extension ForgotPasswordVC {
    
    func callForgotAPI() {
        self.view.endEditing(true)
        
        let param : NSMutableDictionary = ["email":self.txtEmail.text!]
        
        
        HttpRequestManager.requestWithDefaultCalling(service: kforgotPassword_URL, parameters: param) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callForgotAPI()
                })
                return
            }
            else {
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
            }
        }
    }
    
}

