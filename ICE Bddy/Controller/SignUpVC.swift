//
//  SignUpVC.swift
//  ICE Bddy
//
//  Created by iMac on 08/02/21.
//

import UIKit
import Toast_Swift

class SignUpVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var scroll_View: UIScrollView!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var lblAppName: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirm: UITextField!
    
    
    @IBOutlet weak var btnSignUp: UIButton!
    
    @IBOutlet weak var lblAlreadyHaveAcc: UILabel!
    
    // MARK:- Create self View Controller object
    class func initVC() -> SignUpVC {
        let vc = kMainStoryBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        vc.modalPresentationStyle = .overCurrentContext
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Utilities.dropShadow(view: mainView)
        lblAppName.numberOfLines = 0

        scroll_View.layer.cornerRadius = 30
        scroll_View.contentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH, height: mainView.frame.size.height)

        signUpView.roundCorners([.topLeft, .topRight], radius: 30.0, width: AppConstants.ScreenSize.SCREEN_WIDTH)
      
        
    }
    
    // MARK:- UITextField Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            txtEmail.becomeFirstResponder()
        } else if textField == txtEmail {
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword {
            txtConfirm.becomeFirstResponder()
        }else if textField == txtConfirm {
            textField.resignFirstResponder()
            btnSignUp_Click(UIButton())
        }
        return true
    }
    
    func validation() -> String {
        self.view.endEditing(true)
        var msg = String()
        if txtName.text == "" { // Mobile
            msg = kEmptyUserName
        }
        else if txtEmail.text == "" { // Mobile
            msg = kEmailempty
        }
        else if txtEmail.text?.isValidEmail() ?? false == false {
            msg = kInvalidEmail
        }
        else if txtPassword.text == "" { // Mobile
            msg = kpassword
        }
        else if txtPassword.text!.count < 6 {
            msg = "Password should be 6 characters."
        }
        else if txtConfirm.text == "" { // Mobile
            msg = kConpassword
        }
        else if txtPassword.text! != txtConfirm.text! {
            msg = kConpasswordmatch
        }
        return msg
    }


    @IBAction func btnSignUp_Click(_ sender: Any) {
        let msg = self.validation()
        if msg == "" {
            self.callSignUpAPI()

        }else{
            self.showToastMessage(message: msg)
        }
    }
    
    @IBAction func btnSignIn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFaceBook_Click(_ sender: Any) {
    }
    
    @IBAction func btnGoogle_Click(_ sender: Any) {
    }
    
    @IBAction func btnTC_Click(_ sender: Any) {
        
    }
    
}


//MARK:- WebService Call Method
@available(iOS 10.0, *)
extension SignUpVC {
    
    func callSignUpAPI() {
        self.view.endEditing(true)
        
        let param : NSMutableDictionary = ["name" : self.txtName.text!,
                                           "email":self.txtEmail.text!,
                                           "password":self.txtPassword.text!,
                                           "confirm_password":self.txtConfirm.text!,
                                           "device_type":"1",
                                           "device_token":Constant.kDeviceToken]
        
        
        
        
     
        HttpRequestManager.requestWithDefaultCalling(service: kSignup_URL, parameters: param, showLoader: true) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callSignUpAPI()
                })
                return
            }
            else {
                
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    
                    let data = response?.value(forKey: "user_data") as! NSMutableDictionary
                
                    let vc = otpVC.initVC()
                    vc.email = self.txtEmail.text!
                    vc.userData = data
                    self.navigationController?.pushViewController(vc, animated: true)
                   
                }
                else {
                    self.showToastMessage(message: response?.value(forKey: kMessage) as? String ?? "")
                }
            }
        }
    }
}
