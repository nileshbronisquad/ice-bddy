//
//  AppDelegate.swift
//  ICE Bddy
//
//  Created by Manan on 31/12/20.
//

import UIKit
import IQKeyboardManager
import SideMenu
import UserNotifications
import FBSDKCoreKit
import GoogleSignIn
import Firebase
import CoreLocation


@available(iOS 10.0, *)
@main
    class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
        
        var window: UIWindow?
        
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            
            initialConfig()
            
            FirebaseApp.configure()
            
            GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
            
            ApplicationDelegate.shared.application(
                        application,
                        didFinishLaunchingWithOptions: launchOptions
                    )
            
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
                // For iOS 10 data message (sent via FCM
                // Messaging.messaging().delegate = self
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            
            application.registerForRemoteNotifications()
            /*parth end*/
            // Configure for Remote Notification
            self.registerForRemoteNotifications()
            application.registerForRemoteNotifications()
            
            let defaults = USERDEFAULTS
            if defaults.object(forKey: "isFirstTime") == nil {
                let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "PagerVC") as! PagerVC
                let nav = UINavigationController(rootViewController: vc)
                nav.setNavigationBarHidden(true, animated: false)
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
            else {
                
                if let UDData = USERDEFAULTS.value(forKey: kModelUserData) as? Data,
                   let model = NSKeyedUnarchiver.unarchiveObject(with: UDData) as? ModelUserData {
                    
                    if model.apiToken != "" {
                        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "HomeVC") as! HomeVC
                        let nav = UINavigationController(rootViewController: vc)
                        nav.setNavigationBarHidden(true, animated: false)
                        kAppDelegate.window?.rootViewController = nav
                        kAppDelegate.window?.makeKeyAndVisible()
                    }
                    else {
                        
                    }
                }
                else {
                    
                    let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "LoginVC") as! LoginVC
                    let nav = UINavigationController(rootViewController: vc)
                    nav.setNavigationBarHidden(true, animated: false)
                    kAppDelegate.window?.rootViewController = nav
                    kAppDelegate.window?.makeKeyAndVisible()
                    
                }
                
            }
            
            return true
        }
        
        func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            
            ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplication.OpenURLOptionsKey.annotation]
            )
            
        }
        
        // MARK:- Remote Notification Methods
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            let token = deviceToken.map { (data) -> String in return String(format: "%02.2hhx", data) }.joined()
            print(token)
            
            Constant.kDeviceToken = token
        }
        
        // MARK:-  Private Methods
        func registerForRemoteNotifications() {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
                    if let error = error {
                        print("D'oh: \(error.localizedDescription)")
                    } else {
                        DispatchQueue.main.async {
                            if granted {
                                UIApplication.shared.registerForRemoteNotifications()
                            }
                            else {
                                //Do stuff if unsuccessful...
                            }
                        }
                        
                    }
                }
            } else {
                //iOS 9
                let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
                let setting = UIUserNotificationSettings(types: type, categories: nil)
                UIApplication.shared.registerUserNotificationSettings(setting)
                UIApplication.shared.registerForRemoteNotifications()
            }
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().delegate = self
            } else {
                // Fallback on earlier versions
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
        
       
        
        func applicationWillResignActive(_ application: UIApplication) {
            // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
            // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        }
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
            // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        }
        
        //MARK: Private Methods
        func initialConfig(){
            
            IQKeyboardManager.shared().isEnabled = true
            self.window = UIWindow.init(frame: UIScreen.main.bounds)
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "PagerVC") as! PagerVC
            let root = UINavigationController(rootViewController: vc)
            root.navigationBar.isHidden = true

            let leftMenu = loadVC(strStoryboardId: SB_MAIN, strVCId: "LeftMenuVC") as! LeftMenuVC
            let rootLeft = SideMenuNavigationController(rootViewController: leftMenu)
            rootLeft.statusBarEndAlpha = 0.0
            rootLeft.navigationBar.isHidden = true
            rootLeft.menuWidth = UIScreen.main.bounds.width - 80
            rootLeft.presentationStyle = .viewSlideOutMenuIn
            rootLeft.enableSwipeToDismissGesture = false
            SideMenuManager.default.leftMenuNavigationController = rootLeft


            self.window?.rootViewController = root
            self.window?.makeKeyAndVisible()
        }
        
    }

