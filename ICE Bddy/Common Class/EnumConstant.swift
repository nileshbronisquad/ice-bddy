//
//  EnumConstant.swift
//  kiranDiamond
//
//  Created by Coruscate on 28/02/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import UIKit


//MARK: All Cell Enum
enum CellType {
    
    case filterShapeCell
    case filterCaratCell
    case filterColorCell
    case filterFancyCell
    case filterOverToneCell
    case filterIntensityCell
    case filterClarityCell
    case filterFluorescenceCell
    case filterColorShadeCell
    case filterLabCell
    case filterCutPolishSyymmetryCell
    case filterLocationCell
    case filterNewArrivalCell
    case filterDiscountPercCell
    case filterPricePrCaratCell
    case filterAmountCell
    case filterCompanyCodes
    case filterSellerStoneNumbers
    
    case filterTotalDepth
    case filterTable
    case filterDiaMin
    case filterDiaMax
    case filterGridlePer
    case filterRatio
    case filterBlackTable
    case filterKeyToSymbol
    case filterBlackCrown
    case filterWhiteTable
    case filterWhiteCrown
    case filterMilky
    case filterCrownAngle
    case filterPavilionAngle
    case filterCrownHeight
    case filterPavilionHeight
    case filterTableOpen
    case filterCrownOpen
    case filterPavilionOpen
    case filterRoughOrigin
    case filterEyeClean
    case filterHandA
    case filterStoneID
    case filterCertificateNo
    case filterShowId
    case filterMemoNo
    case filterSeparator
    case filterColorSeparator
    case filterGridle
    case filterGridleCondition
    case filterCulet
    case filterCuletCondition
    case filterSection
    case filterLength
    case filterWidth
    case filterDepth
    case filterFooter

    case filterCertImage
    case filterImage
    case filterBlockOnly
    case filterStockNo

    case filterSearchType

    case SortingCell
    case SortingDefaultCell
    case SortingSelectAllCell
    
    case BottomMenuCell
    case QuickSearchClarityCell
    case QuickSearchCountCell
    
    case DiamondKeyToSymbolCell
    case DiamondListCell
    case DiamondOHMCell
    case DiamondMeasurment
    case DiamondCutShade
    case compareTitleCell
    case compareImageCell
    case compareVideoCell
    case compareHeaderCell
    
    case signupNameCell
    case signupFirstNameCell
    case signupLastNameCell
    case signupBirthdateCell
    case signupAnniversoryDateCell
    case signupMobileNoCell
    case signupReferralMobileNoCell
    case signupTelephoneCell
    case signupProfilePicCell
    case SignupWeChatCell
    case SignupWhatsappCell
    case SignupSkypeCell
    
    case signupUserNameCell
    case signupEmailCell
    case signupPasswordCell
    case signupCurrentPasswordCell
    case signupNewPasswordCell
    case signupConfirmPasswordCell
    
    
    case signupCompanyNameCell
    case signupReferralNameCell
    case signupCompanyNameCell1
    case signupReferralNameCell1
    case signupCompanyNameCell2
    case signupReferralNameCell2
//    referral
    case signupDesignationCell
    case signupBussinessTypeCell
    case signupWebsiteCell
    
    case signupReferral
    
    case signupAddressCell
    case signupAddressCell1
    case signupAddressCell2
    case signupCountryCell
    case signupStateCell
    case signupCityCell
    case signupPincodeCell
    
    case signupFaxNoCell
    case signupHowDoYoKnowCell
    case signupThirdPartyCell
    case signupTermsCell
    
    case signupProfileImageCell
    case signupDocuemntCell
    
    case signupDocPhoto
    case signupDocID

    case guestCompanyNameCell
    case guestCompanyPersonCell
    case guestEmailCell
    case guestMobileCell
    
    //Left menu cellTypes
    case MenuHome
    case MenuLogin
    case MenuSearchDiamond
    case MenuStockList
    case MenuSearchHistory
    case MenuQuickSearch
    case MenuLucky
    case MenuStoneOfDay
    case MenuExclusiveStone
    case MenuFancyDiamond
    case MenuNewGoods
    case MenuMatchPairs
    case MenuOfflineStock
    case MenuPriceCalculator
    case MenuConcierge
    case MenuHospitality
    case MenuTransport
    case MenuQrCode
    case MenuMore
    case MenuRateUs
    case MenuContactUs
    case MenuAbout
    case MenuNews
    case MenuTermsCondition
    case MenuVirtualTour
    case MenuTrade
    case MenuSavedSearch
    case MenuTracked
    case MenuPeople
    case MenuMyDiamonds
    case MenuBuyRequest
    case MenuNotification
    case MenuEnquiry
    
    
    //Profile
    case ProfileCalculationCell
    case ProfileCartCell
    case ProfileWatchlistCell
    case ProfileEnquiryCell
    case ProfileCommentCell
    case ProfileReminderCell
    case ProfileOrderCell
    case ProfilePurchaseCell
    case ProfileOfflineTrackCell
    case ProfileOfflineOrderCell
    case ProfileSavedSearch
    case ProfileMyDemand
    case ProfileAppointmentCell
    case ProfileMessageToMdCell
    case ProfileAddressCell
    case ProfileNotificationCell
    case ProfileSuggestionCell
    case ProfileSettingCell
    case ProfileTouchIdCell
    case ProfileTakeTourCell
    case ProfileChangePasswordCell
    case ProfileLanguageCell
    case ProfileQuickSearchCell
    case ProfileOfflineMode
    case ProfileOfflineOrder
    case ProfileExtraCell
    case ProfileLogoutCell
    
    case ProfileSection
    case ProfileSectionSeperator
    case ProfilePersonalDetail
    case ProfileManageAccount
    case ProfileCompanyDetail
    case ProfileUserDetail
    case ProfileSetting
    case ProfileChangePassword
    case ProfileFeedback
    case ProfileManageDiamonds
    case ProfileMyTrack
    case ProfileMyReminder
    case ProfileMyComments
    case ProfileMyEnquiry
    case ProfileMyOrder
    case ProfileMyOnMemoDiamonds
    case ProfileMyHoldDiamonds
    case ProfileMyOffer
    case ProfileLogout

    


    //Appointment
    case AppointmemntSubjectCell
    case AppointmemntSalesPersonCell
    case AppointmemntWherToMeetCell
    case AppointmemntForCell
    case AppointmemntDateCell
    case AppointmemntCabinCell
    case AppointmemntCabinSlotsCell
    case AppointmemntDivisionCell
    case AppointmemntNoteCell
    
    //Transport
    case TransportPerson
    case TransportCustomerType
    case TransportDate
    case TransportPickup
    case TransportDrop
    case TransportMobile1
    case TransportMobile2
    case TransportAnyComment
    
    //Home
    case HomeImage
    case HomeAppoinment
    case HomeCountCell
    case HomeSearchCell
    case HomeFeaturedStoneCell
    case HomeFeaturedPairCell
    case HomePurchaseSnapshot
    case HomePortFolioCell
    case HomeContactCell
    case HomeContactIconCell
    case HomeImageSlider
    case HomeExclusiveDiamond
    case HomeTrendingDiamond
    case HomeTitle
    case HomeRecentSearchCell

    //Address
    case AddressTypeCell
    case AddressStreetCell
    case AddressZipCell
    case AddressCityCell
    case AddressStateCell
    case AddressCountryCell
    case AddressRemarkCell
    case AddressEditCell
    case AddressDeleteCell
    
    case HospitalityUserTypeCell
    case HospitalityCabinCell
    
    case DiamondDetailSection
    case DiamondDetailBasic
    case DiamondDetailCompanyDetail
    case DiamondDetailParameter
    case DiamondDetailInclusion
    case DiamondDetailOther
    case DiamondDetailNotes
    case DiamondDetailSeprator
    case DiamondDetailKeyToSymbol
    case DiamondDetailLaboratotyComment
    case DiamondDetailMemberComment
    case DiamondDetailSuppliercomment
    
    //add note color
    case AddNoteColor
    
    // Enquiry status
    case EnquiryStatusNew
    case EnquiryStatusOpen
    case EnquiryStatusWon
    case EnquiryStatusClosed
    
    // User Details
    case UDFirstName
    case UDLastName
    case UDEmail
    case UDMobile
    case UDUserName
    case UDPassword
    case UDConfirmPassword
    case UDUserType
    
    // Enquiry
    case EnquirySection
    case EnquiryDiamond
    
    //Company detail
    case CompanyName
    case CompanyBusinessType
    case CompanyAddressline1
    case CompanyAddressline2
    case CompanyCountry
    case CompanyState
    case CompanyCity
    case CompanyCode
    case CompanyZipcode
    
    //BottomBar
    case BottomTrack
    case BottomReminder
    case BottomNote
    case BottomEnquiry
    case BottomCompare
    case BottomPlaceOrder
    
    case HoldDiamondCell

}

//MARK: ClassType
enum ClassType {
    
    case TabbarVC
    case HoldDiamondVC
    case OnMemoDiamondsVC
    case Default
    case OfferVC
    case DiamondDetailVC
}

//MARK: SearchType (Selected Tab)
enum SearchType
{
    case Basic
    case Advance
    case Stone
}


//MARK: ListType
enum ListType {
    case Grid
    case List
}

//MARK: SignUp (Selected Tab)
enum SignUpType
{
    case PersonalInfo
    case SecurityInfo
    case BussinessInfo
    case AddressInfo
    case Reference
    case Document
}

//MARK: ListType
enum AppointmentListType {
    case Inbox
    case Complete
    case Cancel
}

enum SignUpPopUpType {
    case countryCode
    case country
    case state
    case city
    case designation
    case businessType
    case referenceType
    
    case salesPerson
    case appointmentFor
    case appointmentCabin
    case appointmentDivison
    
    case appointmentCabinSlot
}

enum ViewSide {
    case Left, Right, Top, Bottom
}

enum PopUpType {
    case list
    case grid
}

enum DiamondListTypes{
    case defaultList
    case ExclusiveDiamond
    case IamLucky
    case NewGoods
    case MatchPairs
    case StoneOfTheDay
}

enum ModuleType {
    
    case DiamondList
    case Cart
    case WatchList
    case Enquiry
    case Comment
    case Reminder
    case DiamondDetail
    case MatchPairs
    case IamLucky
    case ExclusiveDiamond
    case NewGoods
    case Shipment
    case StoneOfTheDay
    case PlaceOrder
    case HoldForMe
    case MakeAnOffer
    case RequestMemo

}

enum DiamondDetailType: Int {
    case image = 0
    case video = 1
    case certificate = 2
    case segoma = 3
    case roughImage = 4
    case plotting = 5
    case handA = 6
    case arrowBlack = 7
    case assetWhite = 8
    case darkFieldWhite = 9
    case heartBlack = 10
    case idealWhite = 11
    case officeLightBlack = 12

}

//MARK: Analytics
//Section
struct SectionAnalytics {
    
    static let ADVANCE_SEARCH   = "AdvanceSearch"
    static let ADD_DEMAND       = "AddDemand"
    static let SAVED_SEARCH     = "SavedSearch"
    static let UPLOAD_EXCEL     = "UploadExcel"
    static let RESET_FILTER     = "ResetFilter"
    static let THREE_EX         = "ThreeEx"
    static let TWO_EX           = "TwoEx"
    static let THREE_VG         = "ThreeVg"
    static let CARAT_SIZE       = "CaratSize"
    static let COLOR            = "Color"
    static let LIST             = "List"
    static let MODIFY           = "Modify"
    static let FILTER           = "Filter"
    static let PLACE_ORDER      = "PlaceOrder"
    static let SHIPMENT         = "Shipment"
    static let ENQUIRY          = "Enquiry"
    static let UPDATE           = "Update"
    static let EXPORT           = "Export"
    static let ACTION_HEADER    = "ActionHeader"
    static let TABLE            = "Table"
    static let EDIT             = "edit"
    static let DELETE           = "delete"
    static let CHANGE_PSWD      = "changePswd"
    static let INVANTORY        = "invantory"
    static let CALCULATOR       = "calculator"
    static let TRACK_SHIPMENT   = "trackShipment"
    static let LOCALE           = "locale"
    static let SEARCH           = "search"
    static let CART             = "cart"
    static let VIEW             = "view"
    static let SETTING          = "setting"
    static let WATCHLIST        = "watchlist"
    static let COMMENT          = "comment"
    static let REMINDER         = "reminder"
    static let DETAILS          = "details"
    
    //Todo
    static let SHARE            = "Share"
    static let MATCHPAIRS       = "MatchPairs"
    static let EXCLUSIVEDIAMOND = "ExclusiveDiamond"
    static let NEWGOODS         = "NewGoods"
    static let COMPARE          = "Compare"
    static let ADD              = "add"
}

//Page Analytics
struct PageAnalytics {
    
    static let SEARCH_RESULT    = "SearchResult"
    static let BEST_OF_HK       = "BestOfHK"
    static let UPCOMING_DIAMOND = "UpcomingDiamond"
    static let NEW_DIAMOND      = "NewDiamond"
    static let MY_CART          = "MyCart"
    static let MY_WATCHLIST     = "MyWatchlist"
    static let MY_REMINDER      = "MyReminder"
    static let MY_COMMENT       = "MyComment"
    static let MY_ENQUIRY       = "MyEnquiry"
    static let MY_DEMAND        = "MyDemand"
    static let MY_ORDER         = "MyOrder"
    static let FANCY_SEARCH     = "FancySearch"
    static let DIAMOND_SEARCH   = "DiamondSearch"
    static let MYSAVED_SEARCH   = "MySavedSearch"
    static let MY_ACCOUNT       = "MyAccount"
    static let DRAWER           = "Drawer"
    static let VOICE_SEARCH     = "VoiceSearch"
    static let TRANSPORT        = "TransPort"
    static let MY_PURCHASE      = "MyPurchase"
    
    //ToDo
    static let QRCODE           = "QrCode"
    static let HOME             = "Home"
    static let OfflineSearchHistory = "OfflineSearchHistory"
    static let NOTIFICATION     = "Notification"
    static let HOSPITALITY      = "Hospitality"
    static let EXCLUSIVE_DIAMOND = "ExclusiveDiamond"
    static let NEW_GOODS        = "NewGoods"
    static let I_AM_LUCKY       = "IamLucky"
    static let MATCH_PAIRS      = "Matchpairs"
    static let DIAMOND_DETAIL   = "DiamondDetail"
    static let COMPARE          = "Compare"
    static let APPOINTMENT      = "Appointment"
    static let PRICE_CALC       = "pricecalculator"
    static let QUICK_SERACH     = "quicksearch"
    static let SUGESSIONS       = "sugessions"
    static let ABOUT_US         = "aboutus"
    static let MESSAGE_TO_MD    = "messagetomd"
    static let CONTACT          = "contact"
    static let PROFILE          = "profile"
    static let OFFLINE_DOWNLOAD = "offlinedownload"
    static let STONE_OF_THE_DAY = "stoneoftheday"
}

//Action Analytics
struct ActionAnalytics {
    
    static let OPEN             = "open"
    static let CLOSE            = "close"
    static let CANCEL           = "cancel"
    static let CLICK            = "click"
    static let RESET            = "reset"
    static let EMAIL_EXCEL      = "emailexcel"
    static let VIDEO            = "video"
    static let PICTURE          = "picture"
    static let CERTIFICATE      = "certificate"
    static let CHANGE           = "change"
    static let DOWNLOAD         = "download"
    static let GRID             = "grid"
    static let LIST             = "list"
    static let SELECT           = "select"
    static let UNSELECT         = "unselect"
    
}

enum EnquiryType {
    
    case Sell
    case Buy
    
}

protocol descValue  {
    var desc: String { get }
}

enum CompanyType : String, descValue {
    case IndependentJeweller            = "Independent Jeweler"
    case OnlineJewelleryStore           = "Online Jewelry Store"
    case DiamondDealerBroker            = "Diamond Dealer/ Broker"
    case DiamondManuCutter              = "Diamond Manufacturer / Cutter"
    case JewelleryManufacture           = "Jewelry Manufacturer"
    case JewelleryRetailShop            = "Jewelry Retail Chain"
    case PawnShop                       = "Pawn shop"
    case Appraiser                      = "Appraiser"
    case Designer                       = "Designer"
    case NotInDiamondGold               = "Not in the Diamond / Jewelry Trade"
    case GoldBuyer                      = "Gold Buyer"
    case DiamondMining                  = "Diamond Mining"
    case Auctioneer                     = "Auctioneer"
    case TradeAssociation               = "Trade Association"
    case WatchDealer                    = "Watch Dealer"
    case FinanceBanking                 = "Finance / Banking"
    case Investor                       = "Investor"
    case JewellryAppraiser              = "Jewelry Appraiser"
    case JewelleryRetailer              = "Jewelry Retailer"
    case DiamondPearlDealerBroker       = "Diamond and Pearl Dealer / Broker"
    case ColoredStoneDealerBroker       = "Colored Stone Dealer / Broker"
    case EstateJewelleryDealer          = "Estate Jewelry Dealer / Broker"
    case Other                          = "Other"
    
    static let allValues                = [IndependentJeweller, OnlineJewelleryStore, DiamondDealerBroker, DiamondManuCutter, JewelleryManufacture, JewelleryRetailShop, PawnShop, Appraiser, Designer, NotInDiamondGold, GoldBuyer, DiamondMining, Auctioneer, TradeAssociation, WatchDealer, FinanceBanking, Investor, JewellryAppraiser, JewelleryRetailer, DiamondPearlDealerBroker, ColoredStoneDealerBroker, EstateJewelleryDealer, Other]
    
    var desc: String {
        switch self {
        case .IndependentJeweller:
            return "Independent Jeweler"
        case .OnlineJewelleryStore:
            return "Online Jewelry Store"
        case .DiamondDealerBroker:
            return "Diamond Dealer/ Broker"
        case .DiamondManuCutter:
            return "Diamond Manufacturer / Cutter"
        case .JewelleryManufacture:
            return "Jewelry Manufacturer"
        case .JewelleryRetailShop:
            return "Jewelry Retail Chain"
        case .PawnShop:
            return "Pawn Shop"
        case .Appraiser:
            return "Appraiser"
        case .Designer:
            return "Designer"
        case .NotInDiamondGold:
            return "Not in the Diamond / Jewelry Trade"
        case .GoldBuyer:
            return "Gold Buyer"
        case .DiamondMining:
            return "Diamond Mining"
        case .Auctioneer:
            return "Auctioneer"
        case .TradeAssociation:
            return "Trade Association"
        case .WatchDealer:
            return "Watch Dealer"
        case .FinanceBanking:
            return "Finance / Banking"
        case .Investor:
            return "Investor"
        case .JewellryAppraiser:
            return "Jewelry Appraiser"
        case .JewelleryRetailer:
            return "Jewelry Retailer"
        case .DiamondPearlDealerBroker:
            return "Diamond and Pearl Dealer / Broker"
        case .ColoredStoneDealerBroker:
            return "Colored Stone Dealer / Broker"
        case .EstateJewelleryDealer:
            return "Estate Jewelry Dealer / Broker"
        case .Other:
            return "Other"
        }
    }
}
