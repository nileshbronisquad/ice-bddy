//
//  Utilities.swift
//  PTE
//
//  Created by CS-Mac-Mini on 09/08/17.
//  Copyright © 2017 CS-Mac-Mini. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import MapKit
import LocalAuthentication
import MessageUI
import DropDown

struct PickerMediaType {
    
    static let image = kUTTypeImage as String
    static let video = kUTTypeMovie as String
}

class Utilities: NSObject {
    
    //Get BioMetricType
    static func biometricType() -> LABiometryType {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            default:
                return .touchID
            }
        } else {
            return .touchID
        }
    }
    
    
    //Convert To html
    class func convertToHtml(str : String) -> NSAttributedString{
        
        var attributedString: NSMutableAttributedString? = nil
        if let anEncoding = str.data(using: .unicode) {
            
            attributedString = try? NSMutableAttributedString(data: anEncoding, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        }
        
        return attributedString ?? NSAttributedString()
    }
    
    static func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    //MARK: JSON
    static func jsonToString(json: Any) -> String?{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString
        } catch let myJSONError {
            print(myJSONError)
        }
        
        return nil
    }
    
    static func objectFromJsonString(str:String) -> Any?{
        
        let jsonString = str
        let jsonData = jsonString.data(using: .utf8)
        let dict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        return dict
    }
    
    class func convertToDollar(number : Double,isSymbol : Bool? = true,digits : Int = 2) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        currencyFormatter.currencyCode = "USD"
        if isSymbol == false{
            currencyFormatter.currencySymbol = ""
        }
        
        currencyFormatter.maximumFractionDigits = digits
        currencyFormatter.minimumFractionDigits = 0
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return currencyFormatter.string(from: NSNumber(value: number))!
    }
    
    //MARK: get doouble value
    class func toDoubleString(value : Double, numberOfDigits : Int = 2) -> String{
        
        //let disValue = dis.toDouble()
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.maximumFractionDigits = numberOfDigits
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.locale = Locale(identifier: "en_US")
        let disValue =  numberFormatter.string(from: NSNumber(value:value)) ?? ""
        
        if disValue != "0.0" && disValue != "0.00" && disValue != "0" {
            
            return "\(disValue)"
        }
        
        return "0.0"
    }
    
    class func toDoubleStringWithDash(value : Double, numberOfDigits : Int = 2) -> String{
           
       //let disValue = dis.toDouble()
       let numberFormatter = NumberFormatter()
       numberFormatter.numberStyle = .decimal
       numberFormatter.usesGroupingSeparator = true
       numberFormatter.maximumFractionDigits = numberOfDigits
       numberFormatter.minimumFractionDigits = 0
       numberFormatter.locale = Locale(identifier: "en_US")
       let disValue =  numberFormatter.string(from: NSNumber(value:value)) ?? ""
       
       if disValue != "0.0" && disValue != "0.00" && disValue != "0" {
           
           return "\(disValue)"
       }
       
       return "-"
   }
    
    class func toDoubleStringWithAnyValue(value : Any?, numberOfDigits : Int = 2) -> String{
        
        let dblValue = self.getDoubleValue(value: value ?? 0)
        
        //let disValue = dis.toDouble()
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.maximumFractionDigits = numberOfDigits
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.locale = Locale(identifier: "en_US")
        let disValue =  numberFormatter.string(from: NSNumber(value:dblValue)) ?? ""
        
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue)"
        }
        
        return "0.0"
    }
    
    class func toDoubleStringWithoutComma(value : Any?, numberOfDigits : Int = 2) -> String{
        
        let dblValue = self.getDoubleValue(value: value ?? 0)
                
        let disValue =  String(format : "%0.\(numberOfDigits)f", dblValue)
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue)"
        }
        
        return "0.0"
    }
    
    
    //MARK: Read Json file
    class func getDataFromJsonFile(_ fileName : String) -> Any? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult1 = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                if let arr = jsonResult1 as? [[String : Any]] {
                    
                    return arr
                }
                
                if let dict = jsonResult1 as? [String : Any] {
                    
                    return dict
                }
                
            } catch {
                return nil
            }
        }
        return nil
    }
    
    //MARK: NavigationBar
    
    static func setNavigationBar(controller : UIViewController,isHidden : Bool, isHideBackButton : Bool = false, title : String? = nil,isStausColor:Bool? = true) {
        
        controller.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: FontScheme.kRegularFont(size: 18)
        ]
        
        if isStausColor == true{
            
            if #available(iOS 13.0, *) {

                if let frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame {
                    let statusBar1 =  UIView()
                    statusBar1.frame = frame
                    statusBar1.backgroundColor = UIColor.clear
                    UIApplication.shared.keyWindow?.addSubview(statusBar1)
                }

            } else {

               let statusBar: UIView = UIApplication.shared.statusBarView!
               if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
                   statusBar.backgroundColor = UIColor.clear
               }
            }
        }
        else{
            
            if #available(iOS 13.0, *) {

                if let frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame {
                    let statusBar1 =  UIView()
                    statusBar1.frame = frame
                    statusBar1.backgroundColor = ColorConstants.ThemeColor
                    UIApplication.shared.keyWindow?.addSubview(statusBar1)
                }

            } else {

               let statusBar: UIView = UIApplication.shared.statusBarView!
               if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
                   statusBar.backgroundColor = ColorConstants.ThemeColor
               }
            }
        }
        
        controller.navigationController?.navigationBar.titleTextAttributes = attrs
        controller.navigationController?.isNavigationBarHidden = isHidden
        controller.navigationController?.navigationBar.isHidden = isHidden

        
        controller.navigationController?.navigationBar.barTintColor = ColorConstants.NavigationBarColor
        controller.navigationController?.navigationBar.tintColor = .white
        controller.navigationController?.navigationBar.isTranslucent = false
        
        //controller.title = title
        
        
        let container = UIView(frame: CGRect(x: 0, y: 0, width: 1000, height: 22))

        let label = UILabel()
        label.text = title
        label.textColor = UIColor.white
        label.font = FontScheme.kRegularFont(size: 18)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        
        container.addSubview(label)
        
        let leftButtonWidth: CGFloat = 65 // left padding
        let rightButtonWidth: CGFloat = CGFloat((controller.navigationItem.rightBarButtonItems?.count ?? 0) * 32) // right padding
        let width = controller.view.frame.width - leftButtonWidth - rightButtonWidth
        let offset = (rightButtonWidth - leftButtonWidth) / 2
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: container.topAnchor),
            label.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            label.centerXAnchor.constraint(equalTo: container.centerXAnchor, constant: -offset),
            label.widthAnchor.constraint(equalToConstant: width)
            ])
        
        controller.navigationItem.titleView = container
        
    }
    
    
    
    class func showValueWithPer(dis : Double) -> String{
        
        let disValue = String(format: "%0.2f",dis)
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue) %"
        }
        return "-"
    }
    
    //MARK: Text Height
    class func getLabelHeight(constraintedWidth width: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    class func getLabelWidth(constraintedheight: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: constraintedheight))
        label.numberOfLines = 1
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.width
    }
    
    class func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    //MARK: Get UIColor from hex color
    
    static func colorWithHexString (hex:String) -> UIColor {
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //MARK: Strings
    static func checkStringEmptyOrNil(str : String?) -> Bool{
        let trimmedStr = str?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
        if trimmedStr?.isEmpty == nil || trimmedStr?.count == 0{
            return true
        }
        return false
    }
    
    //MARK: Show Alert View
    
    class func dropShadow(view : UIView) {
        
        view.layer.cornerRadius = 30
        view.layer.masksToBounds = false
        
        // border
        view.layer.borderWidth = 0.5
        view.layer.borderColor = ColorConstants.GrayColor.cgColor
        
        // shadow
        view.layer.shadowColor = ColorConstants.GrayColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: -2)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 1.7
        
    }
    
    // MARK: TAKE SCREEN SHOT
    
    class func takeScreenShot(success:@escaping (()->())){
        
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, true {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        success()
    }
    
    
    class func openDropDown(sender: AnchorView, arrData: [String], arrImage: [String]? = nil, height: CGFloat = 50, width: CGFloat, font : UIFont = UIFont(name:"Lato-Medium",size:16)!, isCustomCell: Bool = false, isCount: Bool = false, completion: @escaping ((Int, String)) -> ()) {
        let dropDown = DropDown()

        dropDown.anchorView = sender
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)! - 10)
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)! + 10)
        dropDown.backgroundColor = UIColor.white
        dropDown.cornerRadius = 5
        dropDown.dataSource = arrData
        dropDown.cellHeight = height
        dropDown.textFont = font

        if dropDown.dataSource.count == 0 { return }
        
        if isCustomCell {
            dropDown.cellNib = UINib(nibName: "SearchCell", bundle: nil)
            dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
               guard let cell = cell as? SearchCell else { return }
               // Setup your custom UI components
                cell.imgView.isHidden = false
                if let arrImages = arrImage {
                    if arrImages.count > 0 {
                        cell.imgView.image = UIImage.init(named: arrImage?[index] ?? "")
                        if isCount {
                            if index == arrImages.count - 1 {
                                cell.viewCount.isHidden = false
                            }
                        }
                    } else {
                        cell.imgView.image = #imageLiteral(resourceName: "AddRound")
                    }
                }
            }
        }
        
        dropDown.selectionAction = { (index: Int, item: String) in
            completion((index, item))
        }
        
        dropDown.width = width
        dropDown.show()
        dropDown.reloadAllComponents()

    }
   
    
    //MARK: Call
    class func call(phoneNumber: String)
    {
        let trimPhone = phoneNumber.replacingOccurrences(of: " ", with: "")
        if  let url = URL(string: "tel://\(trimPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK: WhatsUp
    class func openWhatsUp(number : String, message : String) {
        
        let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(number)&text=\(message)")!
        if UIApplication.shared.canOpenURL(appURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL)
            }
        }
        else {
            // Whatsapp is not installed
        }
    }
    
    //MARK: Open mail
  
    
    //MARK: Device Unique ID
    class func getDeviceUniqueID() -> String{
        
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }

    //MARK: Change Color
    class func changeStringColor(string : String, array : [String], colorArray : [UIColor],arrFont:[UIFont]) ->  NSAttributedString {
        
        let attrStr = NSMutableAttributedString(string: string)
        let inputLength = attrStr.string.count
        let searchString = array
        
        for i in 0...searchString.count-1
        {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    
                    if colorArray.count >= i{
                        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: colorArray[i], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    if arrFont.count >= i{
                        attrStr.addAttribute(NSAttributedString.Key.font, value: arrFont[i], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
                    //                    return attrStr
                }
            }
        }
        
        return attrStr
    }
    
    class func setImageColor(imageView : UIImageView, color : UIColor) {
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = color
    }
    
    class func changeStringUnderLine(attrStr : NSMutableAttributedString, array : [String]) -> NSAttributedString{
        
        let inputLength = attrStr.string.count
        let searchString = array
        for i in 0...searchString.count-1
        {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    attrStr.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: range.location, length: searchLength))
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
                    return attrStr
                }
            }
        }
        return NSAttributedString()
    }
    
    //Open apple map
    class func openMap(latitude:Double, longitude:Double) {
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!, options: [:], completionHandler: nil)
        }
        else {
            let str = "https://maps.google.com/?q=\(latitude),\(longitude)&directionsmode=driving"
            Utilities.openURL(str)
        }
        
    }
    
    //MARK: get double value
    class func getDoubleValue<T>(value : T) -> Double{
        if let val = value as? NSNumber{
            return val.doubleValue
        }
        
        if let val = value as? Float{
            return Double(val)
        }
        
        if let val = value as? Double{
            return val
        }
        
        if let val = value as? Int{
            return Double(val)
        }
        
        if let val = value as? String{
            return Double(val) ?? 0
        }
        
        return 0
    }
    
    //MARK: get double value
    class func getIntValue<T>(value : T) -> Int{
        if let val = value as? NSNumber{
            return val.intValue
        }
        
        if let val = value as? Float{
            return Int(val)
        }
        
        if let val = value as? Int{
            return Int(val)
        }
        
        if let val = value as? Int{
            return val
        }
        
        if let val = value as? String{
            return Int(val) ?? 0
        }
        
        return 0
    }
    
    //MARK: Get Month
    
    class func getMonthString(month : Int?) -> String{
        
        if month == 1 {
            return "January"
        }else if month == 2 {
            return "February"
        }else if month == 3 {
            return "March"
        }else if month == 4 {
            return "April"
        }else if month == 5 {
            return "May"
        }else if month == 6 {
            return "June"
        }else if month == 7 {
            return "July"
        }else if month == 8 {
            return "August"
        }else if month == 9 {
            return "September"
        }else if month == 10 {
            return "October"
        }else if month == 11 {
            return "November"
        }else if month == 12 {
            return "December"
        }
           return "-"
       }

   
    
    
    //MARK:- Calling number
    class func openURL(_ strUrl : String) {
        if let url = URL(string: strUrl), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK:- Notch Bottom Area
    class func getBottomSafeArea() -> CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0.0
        }
        return 0.0
    }
    
    class func getTopSafeArea() -> CGFloat {
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0.0
        }
        return 0.0
    }   
    
    class func toDouble(value : Double) -> String{
        
        //let disValue = dis.toDouble()
        let disValue = String(format: "%0.2f",value)
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue)"
        }
        
        return "-"
    }
    
  
    //MARK: Convert String To Dictionary
    class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}

//Constraint multiplier
extension NSLayoutConstraint {
    
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
