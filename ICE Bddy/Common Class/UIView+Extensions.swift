import UIKit
import Foundation
import SwiftMessages
import Toast_Swift

// MARK: - Designable Extension

@IBDesignable
extension UIView {
    
    @IBInspectable
    /// Should the corner be as circle
    public var circleCorner: Bool {
        get {
            return min(bounds.size.height, bounds.size.width) / 2 == CornerRadius
        }
        set {
            CornerRadius = newValue ? min(bounds.size.height, bounds.size.width) / 2 : CornerRadius
        }
    }
    
    @IBInspectable
    /// Corner radius of view; also inspectable from Storyboard.
    public var CornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = circleCorner ? min(bounds.size.height, bounds.size.width) / 2 : newValue
                //abs(CGFloat(Int(newValue * 100)) / 100)
        }
    }
    
    
   
    
    @IBInspectable
    /// Border color of view; also inspectable from Storyboard.
    public var borderColor: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            layer.borderColor = color.cgColor
        }
    }
    
    @IBInspectable
    /// Border width of view; also inspectable from Storyboard.
    public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    /// Shadow color of view; also inspectable from Storyboard.
    public var shadowColor: UIColor? {
        get {
            guard let color = layer.shadowColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    /// Shadow offset of view; also inspectable from Storyboard.
    public var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    /// Shadow opacity of view; also inspectable from Storyboard.
    public var shadowOpacity: Double {
        get {
            return Double(layer.shadowOpacity)
        }
        set {
            layer.shadowOpacity = Float(newValue)
        }
    }
    
    @IBInspectable
    /// Shadow radius of view; also inspectable from Storyboard.
    public var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    /// Shadow path of view; also inspectable from Storyboard.
    public var shadowPath: CGPath? {
        get {
            return layer.shadowPath
        }
        set {
            layer.shadowPath = newValue
        }
    }
    
    @IBInspectable
    /// Should shadow rasterize of view; also inspectable from Storyboard.
    /// cache the rendered shadow so that it doesn't need to be redrawn
    public var shadowShouldRasterize: Bool {
        get {
            return layer.shouldRasterize
        }
        set {
            layer.shouldRasterize = newValue
        }
    }
    
    @IBInspectable
    /// Should shadow rasterize of view; also inspectable from Storyboard.
    /// cache the rendered shadow so that it doesn't need to be redrawn
    public var shadowRasterizationScale: CGFloat {
        get {
            return layer.rasterizationScale
        }
        set {
            layer.rasterizationScale = newValue
        }
    }
    
    @IBInspectable
    /// Corner radius of view; also inspectable from Storyboard.
    public var maskToBounds: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }
}


// MARK: - Properties

public extension UIView {
    
    /// Size of view.
    public var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.width = newValue.width
            self.height = newValue.height
        }
    }
    
    /// Width of view.
    public var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    /// Height of view.
    public var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

extension UIView {
    
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview.flatMap { $0.superview(of: T.self) }
    }
    
}


// MARK: - Methods

public extension UIView {
    
    public typealias Configuration = (UIView) -> Swift.Void
    
    public func config(configurate: Configuration?) {
        configurate?(self)
    }
    
    /// Set some or all corners radiuses of view.
    ///
    /// - Parameters:
    ///   - corners: array of corners to change (example: [.bottomLeft, .topRight]).
    ///   - radius: radius for selected corners.
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
}

extension UIView {
    
    func searchVisualEffectsSubview() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let found = subview.searchVisualEffectsSubview() {
                    return found
                }
            }
        }
        return nil
    }
    
    /// This is the function to get subViews of a view of a particular type
    /// https://stackoverflow.com/a/45297466/5321670
    func subViews<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T{
                all.append(aView)
            }
        }
        return all
    }
    
    
    /// This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T
    /// https://stackoverflow.com/a/45297466/5321670
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
}

public enum PickerType {
    case photoLibrary,videoPhotoLibrary,cameraVideo,cameraPhoto,library,camera
}

public enum MediaType {
    case photo,video,both
}


extension UIViewController  {
    
    func showToastMessage(message:String) {
       self.view.makeToast(message)
    }
    
    func openPhotoSelectionOption(for imagePicker:UIImagePickerController, type : MediaType) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            if type == .photo {
                self.openUIImagePickerController(for: imagePicker, type: .cameraPhoto)
            } else if type == .video {
                self.openUIImagePickerController(for: imagePicker, type: .cameraVideo)
            } else if type == .both {
                self.openUIImagePickerController(for: imagePicker, type: .camera)
            }
        }
        
        let galeryAction = UIAlertAction(title: "Photo library", style: .default) { (action) in
            if type == .photo {
                self.openUIImagePickerController(for: imagePicker, type: .photoLibrary)
            } else if type == .video {
                self.openUIImagePickerController(for: imagePicker, type: .videoPhotoLibrary)
            } else if type == .both {
                self.openUIImagePickerController(for: imagePicker, type: .library)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            //
        }
        
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(galeryAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openPhotoSelectionOption1(for imagePicker:UIImagePickerController, type : MediaType) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            if type == .photo {
                self.openUIImagePickerController1(for: imagePicker, type: .cameraPhoto)
            } else if type == .video {
                self.openUIImagePickerController1(for: imagePicker, type: .cameraVideo)
            } else if type == .both {
                self.openUIImagePickerController1(for: imagePicker, type: .camera)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            //
        }
        
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openUIImagePickerController1(for imagePicker:UIImagePickerController, type : PickerType) {
        if type == .photoLibrary {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
               // imagePicker.allowsEditing = true
                imagePicker.sourceType = .photoLibrary
                
            } else {
                print("photoLibrary not available")
            }
        } else if type == .cameraPhoto {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                //imagePicker.allowsEditing = true
                imagePicker.sourceType = .camera
                
                imagePicker.cameraCaptureMode = .photo
            } else {
                print("Camera not available")
                self.openUIImagePickerController1(for: imagePicker, type: .photoLibrary)
                return
            }
        } else if type == .videoPhotoLibrary {
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePicker.sourceType = .photoLibrary
                imagePicker.videoMaximumDuration = TimeInterval(60.0)
                
                
            }else{
                print("photoLibrary not available")
            }
        } else if type == .cameraVideo {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.allowsEditing = true
                imagePicker.sourceType = .camera
                imagePicker.videoMaximumDuration = TimeInterval(60.0)
                
                imagePicker.cameraCaptureMode = .video
            } else {
                print("Camera not available")
                self.openUIImagePickerController1(for: imagePicker, type: .videoPhotoLibrary)
                return
            }
        }
        //imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openUIImagePickerController(for imagePicker:UIImagePickerController, type : PickerType) {
        if type == .photoLibrary {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                //imagePicker.allowsEditing = true
                imagePicker.sourceType = .photoLibrary
                
            } else {
                print("photoLibrary not available")
            }
        } else if type == .cameraPhoto {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                //imagePicker.allowsEditing = true
                imagePicker.sourceType = .camera
                
                imagePicker.cameraCaptureMode = .photo
            } else {
                print("Camera not available")
                self.openUIImagePickerController(for: imagePicker, type: .photoLibrary)
                return
            }
        } else if type == .videoPhotoLibrary {
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePicker.sourceType = .photoLibrary
                
                
            }else{
                print("photoLibrary not available")
            }
        } else if type == .cameraVideo {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.allowsEditing = true
                imagePicker.sourceType = .camera
                imagePicker.cameraCaptureMode = .video
            } else {
                print("Camera not available")
                self.openUIImagePickerController(for: imagePicker, type: .videoPhotoLibrary)
                return
            }
        }
        //imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

}

extension String {
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func isValidMobile() -> Bool {
        if self.count < 10 || self.count > 13{
            return false
        }
        return true
    }
    
    func isValidString() -> Bool {
        return self.removeSpaceAndNewLine().isEmpty
    }
    
    func removeSpaceAndNewLine() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    // encode emoji's message
    func encode() -> String {
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    // decode emoji's message
    func decode() -> String? {
        let data = self.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    func convertHtml() -> NSAttributedString {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
