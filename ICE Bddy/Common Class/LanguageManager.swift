//
//  LanguageManager.swift
//  iMotors
//
//  Created by mac on 5/13/17.
//  Copyright © 2017 Differenz System Pvt. Ltd. All rights reserved.
//

import Foundation

let kLMDefaultLanguage = "en"
//let kLMEnglish = "en"
//let kLMFrench  = "fr"

let appSupportedLanguage = ["en"]

class LanguageManager: NSObject {
    
    // A Singleton instance
    static let sharedInstance = LanguageManager()
    
    
    
    var selectedLangKey : String?

    //get current app language
    class func getCurrentLanguage() -> String
    {
        let language = "en"
        
        if language == nil || language.isEmpty {
            return kLMDefaultLanguage
        }
        return language 
    }
    
    //get localized string
    class func localizedString(_ key: String) -> String {
        let selectedLanguage: String = LanguageManager.selectedLanguage()
        // Get the corresponding bundle path.
        let path: String? = Bundle.main.path(forResource: selectedLanguage, ofType: "lproj")
        // Get the corresponding localized string.
        let languageBundle = Bundle(path: path!)
        let str = languageBundle?.localizedString(forKey: key, value: "", table: nil)
        
        if str == nil
        {
            return ""
        }else
        {
            return str!
        }
    }
    
    //check selected language supported or not
    class func isSupportedLanguage(_ language: String) -> Bool {
        return appSupportedLanguage.contains(language)
    }
    
    //store language if supported
    class func setSelectedLanguage(_ language: String) {
        // Check if desired language is supported.
        if self.isSupportedLanguage(language) {
            self.sharedInstance.selectedLangKey = language
        }
        else {
            self.sharedInstance.selectedLangKey = nil
            // if desired language is not supported, set selected language to nil.
        }
    }
    
    class func selectedLanguage() -> String {
        
        if let langKey = self.sharedInstance.selectedLangKey {
            return langKey
        }
        
        // Get selected language from user defaults.
        var selectedLanguage: String? = "en"
        // if the language is not defined in user defaults yet...
        if selectedLanguage == nil {
            selectedLanguage = kLMDefaultLanguage
            
            if self.isSupportedLanguage(selectedLanguage!) {
                self.setSelectedLanguage(selectedLanguage!)
            }
            else {
                // Set the LanguageManager default language as selected language.
                self.setSelectedLanguage(kLMDefaultLanguage)
            }
        }
        
        let newLangkey = "en"
        self.sharedInstance.selectedLangKey = newLangkey
        return newLangkey
    }
}
// Lang Change Popup

