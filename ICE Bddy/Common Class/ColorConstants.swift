//
//  ColorConstants.swift
//  kiranDiamond
//
//  Created by Coruscate on 28/02/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import Foundation
import UIKit

/** This colors struct is used to define colors for application. */

struct ColorConstants {

    static let NavigationBarColor           = #colorLiteral(red: 0.4196078431, green: 0.5568627451, blue: 0.9803921569, alpha: 1)

    static var ThemeColor: UIColor {
        get {
            if AppConstants.themeType == 1 {
                return #colorLiteral(red: 0.1921568627, green: 0.1921568627, blue: 0.1921568627, alpha: 1) //#313131
            } else {
                return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //#E413C3
            }
        }
    }
    
   
    
   
    
    
    static let PrimaryColor = #colorLiteral(red: 0.9568627477, green: 0.9764705896, blue: 0.9960784316, alpha: 1)
    static let BorderColor                     = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let GrayColor                     = #colorLiteral(red: 0.6478501558, green: 0.6478501558, blue: 0.6478501558, alpha: 1)


}

extension UIColor {
    convenience init(_ hexString: String, alpha: Double = 1.0) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (r, g, b) = ((int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (r, g, b) = (int >> 16, int >> 8 & 0xFF, int & 0xFF)
        default:
            (r, g, b) = (1, 1, 0)
        }
        self.init(displayP3Red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(255 * alpha) / 255)
    }
}
