//
//  StringConstants.swift
//  kiranDiamond
//
//  Created by Coruscate on 28/02/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

///// DO NOT CHANGE ANY CONSTANT WITHOUT DISCUSSION OF TEAM ///////////

import Foundation

struct StringConstants {
    
    ///// DO NOT CHANGE ANY CONSTANT WITHOUT DISCUSSION OF TEAM ///////////
    static let pageLimit                                = 20
    static let CountryCode                              = "+91"
    static let DownloadFolderName                       = "Downloads"
    static let Other                                    = "other"
    static let UserNameNOTAcceptChar                    = " @"
    static let NameAcceptChar                           = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    static let NumberAcceptChar                         = "0123456789"
    
    static let FMFullName                               = "Forevermark eligible"
    static let CmFullName                               = "Canadamark eligible"
    
    //Codes
    static let yes                                      = "yes"
    static let no                                       = "no"
    
    //MARK: Expandable

    //Local Notification Identifier
    struct NotificationIdentifier {
        
        static let offlineStockDownload   = "offlineStockDownload"
        static let offlineOrderSynced     = "offlineOrderSynced"
    }
    
    
    //Common
    struct Common {

        
        static var kOpenSettings : String {
            get { return LanguageManager.localizedString("kOpenSettings") }
        }
        
        static var kCameraPermission : String {
            get { return LanguageManager.localizedString("kCameraPermission") }
        }
        
        static var kFailedToGetCamera : String {
            get { return LanguageManager.localizedString("kFailedToGetCamera") }
        }
        
        static var SomethingWrong : String {
            get { return LanguageManager.localizedString("kLblSomethingWentWrong") }
        }

        static var kAccessDenied : String {
            get { return LanguageManager.localizedString("kAccessDenied") }
        }
        
        static var KLblLogoutMsg : String {
            get { return LanguageManager.localizedString("KLblLogoutMsg") }
        }
        
        static var KMsgReloadList : String {
            get { return LanguageManager.localizedString("KMsgReloadList") }
        }
    }
    
    //MARK: Button Title
    struct ButtonTitles {
        static var kClear : String {
            get { return LanguageManager.localizedString("KLblClear") }
        }
        
        static var KConfirm : String {
            get { return LanguageManager.localizedString("KLblConfirm") }
        }
        
        static var KCancel : String {
            get { return LanguageManager.localizedString("KLblCancel") }
        }
        
        static var kRemove : String {
            get { return LanguageManager.localizedString("KLblRemove") }
        }
        
        static var kYes : String {
            get { return LanguageManager.localizedString("KLblYes") }
        }
        
        static var kNo : String {
            get { return LanguageManager.localizedString("KLblNo") }
        }
        
        static var kAll : String {
            get { return LanguageManager.localizedString("KLblAll") }
        }
        
        static var kCurrent : String {
            get { return LanguageManager.localizedString("KLblCurrent") }
        }
        
        static var kClose : String {
            get { return LanguageManager.localizedString("KLblClose") }
        }
        
        static var kView : String {
            get { return LanguageManager.localizedString("KLblView") }
        }
        
        static var kRetry : String {
            get { return LanguageManager.localizedString("KLblRetry") }
        }
        
        static var kApply : String {
            get { return LanguageManager.localizedString("KLblApply") }
        }
        
        static var kDone : String {
            get { return LanguageManager.localizedString("KLblDone") }
        }
        
        
        static var kNext : String {
            get { return LanguageManager.localizedString("KLblNext") }
        }
        
        static var kSubmit : String {
            get { return LanguageManager.localizedString("KLblSubmit") }
        }
        
        static var KOk : String {
            get { return LanguageManager.localizedString("KLblOk") }
        }
        
        static var KDelete : String {
            get { return LanguageManager.localizedString("KLblDelete") }
        }
        
        static var kRefersh : String {
            get { return LanguageManager.localizedString("KLblRefresh") }
        }
        
        static var kAdd : String {
            get { return LanguageManager.localizedString("KLblAdd") }
        }
        
        static var KTryAgain : String {
            get { return LanguageManager.localizedString("KLblTryAgain") }
        }
    }
    
    //NoData
    struct Nodata {
        
        static var NoDataFound : String {
            get { return LanguageManager.localizedString("KMsgNoDataFound") }
        }
        
        static var NoFavourite : String {
            get { return LanguageManager.localizedString("KMsgNoFavouriteFound") }
        }
        
        static var NoDiamondFound : String {
            get { return LanguageManager.localizedString("KMsgNoDiamondFound") }
        }
        
        static var NoAddressFound : String {
            get { return LanguageManager.localizedString("KMsgNoAddressFound") }
        }
        
        static var kNoInternet : String {
            get { return LanguageManager.localizedString("KMsgNoInternetConnection") }
        }
        
        static var kNoSearch : String {
            get { return LanguageManager.localizedString("KMsgNoSearchFound") }
        }
        
        static var kNoOrder : String {
            get { return LanguageManager.localizedString("KMsgNoOrderFound") }
        }
        
        static var kNoAppointment : String {
            get { return LanguageManager.localizedString("KMsgNoAppointmentFound") }
        }
        
        static var kProducts : String {
            get { return LanguageManager.localizedString("KMsgNoProductsFound") }
        }
        
        static var kNoDemands : String {
            get { return LanguageManager.localizedString("KMsgNoDemandFound") }
        }
        
        static var kNoHistory : String {
            get { return LanguageManager.localizedString("KMsgNoHisoryFound") }
        }
        
        static var kNoPurchase : String {
            get { return LanguageManager.localizedString("KMsgNoPurchaseFound") }
        }
        
        static var kNoNotification : String {
            get { return LanguageManager.localizedString("KMsgNoNotificationFound") }
        }
        
        static var kNoUserFound : String {
            get { return LanguageManager.localizedString("kNoUserFound") }
        }
    }
    
    //Login
    struct Login {
        
        static var kUsernameMessage : String {
            get { return LanguageManager.localizedString("KMsgEmptyUsername") }
        }
        
        static var kEnterEmail : String {
            get { return LanguageManager.localizedString("KMsgEmptyEmail") }
        }
        
        static var kValidEmail : String {
            get { return LanguageManager.localizedString("KMsgInvalidEmail") }
        }
        
        static var kPassword : String {
            get { return LanguageManager.localizedString("KMsgEmptyPassword") }
        }
    }
    
    //SignUp
    struct SignUp {
        
        static var KLblWelcome : String {
            get { return LanguageManager.localizedString("KLblWelcome") }
        }
        
        static var KLblPersonalInformation : String {
            get { return LanguageManager.localizedString("KLblPersonalInformation") }
        }
        static var KHintFirstName : String {
            get { return LanguageManager.localizedString("KHintFirstName") }
        }
        static var KHintLastName : String {
            get { return LanguageManager.localizedString("KHintLastName") }
        }
        static var KLblBirthDate : String {
            get { return LanguageManager.localizedString("KLblBirthDate") }
        }
        static var KLblAnniversaryDate : String {
            get { return LanguageManager.localizedString("KLblAnniversaryDate") }
        }
        static var KHintMobileNo : String {
            get { return LanguageManager.localizedString("KHintMobileNo") }
        }
        static var KWeChat : String {
            get { return LanguageManager.localizedString("KWeChat") }
        }
        static var KLblTelephoneNo : String {
            get { return LanguageManager.localizedString("KLblTelephoneNo") }
        }
        static var KHintCountryCode : String {
            get { return LanguageManager.localizedString("KHintCountryCode") }
        }
        
       
        static var KLblSecurityInfo : String {
            get { return LanguageManager.localizedString("KLblSecurityInfo") }
        }
        static var KHintUsername : String {
            get { return LanguageManager.localizedString("KHintUsername") }
        }
        static var KHintEmail : String {
            get { return LanguageManager.localizedString("KHintEmail") }
        }
        static var KHintPassword : String {
            get { return LanguageManager.localizedString("KHintPassword") }
        }
        static var KHintConfirmPassword : String {
            get { return LanguageManager.localizedString("KHintConfirmPassword") }
        }
        
        
        static var KLblBusinessInfo : String {
            get { return LanguageManager.localizedString("KLblBusinessInfo") }
        }
        static var KHintCompanyName : String {
            get { return LanguageManager.localizedString("KHintCompanyName") }
        }
        static var KHintDesignation : String {
            get { return LanguageManager.localizedString("KHintDesignation") }
        }
        static var KHintBusinessType : String {
            get { return LanguageManager.localizedString("KHintBusinessType") }
        }
        static var KLblWebsite : String {
            get { return LanguageManager.localizedString("KLblWebsite") }
        }
        
        
        static var KLblAddressInfo : String {
            get { return LanguageManager.localizedString("KLblAddressInfo") }
        }
        static var KHintAddress : String {
            get { return LanguageManager.localizedString("KHintAddress") }
        }
        static var KHintCountry : String {
            get { return LanguageManager.localizedString("KHintCountry") }
        }
        static var KHintState : String {
            get { return LanguageManager.localizedString("KHintState") }
        }
        static var KHintCity : String {
            get { return LanguageManager.localizedString("KHintCity") }
        }
        static var KHintPincode : String {
            get { return LanguageManager.localizedString("KHintPincode") }
        }
        
        
        static var KLblReferenceInfo : String {
            get { return LanguageManager.localizedString("KLblReferenceInfo") }
        }
        static var KLblFax : String {
            get { return LanguageManager.localizedString("KLblFax") }
        }
        static var KLblHowYouKnow : String {
            get { return LanguageManager.localizedString("KLblHowYouKnow") }
        }
        static var KLblThirdPartyRef : String {
            get { return LanguageManager.localizedString("KLblThirdPartyRef") }
        }
        static var KLblReadTermsCondition : String {
            get { return LanguageManager.localizedString("KLblReadTermsCondition") }
        }        
        
        
        static var KLblDocumentInfo : String {
            get { return LanguageManager.localizedString("KLblDocumentInfo") }
        }
        static var KHintPhotoProof : String {
            get { return LanguageManager.localizedString("KHintPhotoProof") }
        }
        static var KHintDocumentProof : String {
            get { return LanguageManager.localizedString("KHintDocumentProof") }
        }
        
        
        //ERROR MESSAGE
        static var KMsgEmptyname : String {
            get { return LanguageManager.localizedString("KMsgEmptyname") }
        }

        static var KMsgEmptyFirstname : String {
            get { return LanguageManager.localizedString("KMsgEmptyFirstname") }
        }
        static var KMsgEmptyLastname : String {
            get { return LanguageManager.localizedString("KMsgEmptyLastname") }
        }
        static var KMsgEmptyMobile : String {
            get { return LanguageManager.localizedString("KMsgEmptyMobile") }
        }    
        static var KMsgInvalidMobile : String {
            get { return LanguageManager.localizedString("KMsgInvalidMobile") }
        }
        static var KMsgInvalidTelephone : String {
            get { return LanguageManager.localizedString("KMsgInvalidTelephone") }
        }
        static var KMsgEmptyUsername : String {
            get { return LanguageManager.localizedString("KMsgEmptyUsername") }
        }
        static var KMsgEmptyEmail : String {
            get { return LanguageManager.localizedString("KMsgEmptyEmail") }
        }
        static var KMsgInvalidEmail : String {
            get { return LanguageManager.localizedString("KMsgInvalidEmail") }
        }
        static var KMsgEmptyPassword : String {
            get { return LanguageManager.localizedString("KMsgEmptyPassword") }
        }
        static var KMsgInvalidPassword : String {
            get { return LanguageManager.localizedString("KMsgInvalidPassword") }
        }
        static var KMsgEmptyConfirmPass : String {
            get { return LanguageManager.localizedString("KMsgEmptyConfirmPass") }
        }
        static var KMsgMismatchConfirmPass : String {
            get { return LanguageManager.localizedString("KMsgMismatchConfirmPass") }
        }
        static var KMsgInvalidBirthdate : String {
            get { return LanguageManager.localizedString("KMsgInvalidBirthdate") }
        }
        static var KMsgInvalidAnniversarydate : String {
            get { return LanguageManager.localizedString("KMsgInvalidAnniversarydate") }
        }
        static var KMsgEmptyCompanyName : String {
            get { return LanguageManager.localizedString("KMsgEmptyCompanyName") }
        }
        static var KMsgSelectDesignation : String {
            get { return LanguageManager.localizedString("KMsgSelectDesignation") }
        }
        static var KMsgSelectBusinessType : String {
            get { return LanguageManager.localizedString("KMsgSelectBusinessType") }
        }
        static var KMsgCurrentPassword : String {
            get { return LanguageManager.localizedString("KMsgEmptyCurrentPassword") }
        }

        static var KMsgNewPasword : String {
            get { return LanguageManager.localizedString("KMsgEmptyNewPassword") }
        }

        static var KMsgConfirmPassword : String {
            get { return LanguageManager.localizedString("KMsgEmptyConfirmPass") }
        }

        static var KInvalidWebAddress : String {
            get { return LanguageManager.localizedString("KInvalidWebAddress") }
        }
        static var KMsgEmptyAddress : String {
            get { return LanguageManager.localizedString("KMsgEmptyAddress") }
        }
        static var KMsgEmptyAddress1 : String {
            get { return LanguageManager.localizedString("KMsgEmptyAddress1") }
        }
        static var KMsgEmptyAddress2 : String {
            get { return LanguageManager.localizedString("KMsgEmptyAddress2") }
        }
        static var KMsgSelectCountry : String {
            get { return LanguageManager.localizedString("KMsgSelectCountry") }
        }
        static var KMsgSelectState : String {
            get { return LanguageManager.localizedString("KMsgSelectState") }
        }
        static var KMsgSelectCity : String {
            get { return LanguageManager.localizedString("KMsgSelectCity") }
        }
        static var KMsgEmptyPincode : String {
            get { return LanguageManager.localizedString("KMsgEmptyPincode") }
        }
        static var KMsgInvalidPincode : String {
            get { return LanguageManager.localizedString("KMsgInvalidPincode") }
        }
        static var KLblSelectCountry : String {
            get { return LanguageManager.localizedString("KLblSelectCountry") }
        }
        static var KLblSelectDesignation : String {
            get { return LanguageManager.localizedString("KLblSelectDesignation") }
        }
        static var KLblSelectBusinessType : String {
            get { return LanguageManager.localizedString("KLblSelectBusinessType") }
        }
        static var KLblSelectState : String {
            get { return LanguageManager.localizedString("KLblSelectState") }
        }
        static var KLblSelectCity : String {
            get { return LanguageManager.localizedString("KLblSelectCity") }
        }
        static var KLblSelectReference : String {
            get { return LanguageManager.localizedString("KLblSelectReference") }
        }
        static var KMsgAcceptTerms : String {
            get { return LanguageManager.localizedString("KMsgAcceptTerms") }
        }
        static var KMsgProofProof : String {
            get { return LanguageManager.localizedString("KMsgProofProof") }
        }
        
        static var KMsgUploadPhotoID : String {
            get { return LanguageManager.localizedString("KPleaseUploadPhotoID") }
        }
        
        static var KMsgUploadBusinessID : String {
            get { return LanguageManager.localizedString("KPleaseUploadBusinessID") }
        }
        
    }
    
    struct Filter {
        
        //Filter
        
        static var kWrongFromValue : String {
            get { return LanguageManager.localizedString("kWrongFromValue") }
        }
        
        static var kWrongToValue : String {
            get { return LanguageManager.localizedString("kWrongToValue") }
        }
        
        static var kEmptySelection : String {
            get { return LanguageManager.localizedString("KMsgSelectSearch") }
        }
        
        static var kEmptySelectionDemand : String {
            get { return LanguageManager.localizedString("KMsgSelectForDemand") }
        }
        
        static var kEnterTitle : String {
            get { return LanguageManager.localizedString("KMsgEnterTitle") }
        }
        
        static var kSelectTillDate : String {
            get { return LanguageManager.localizedString("KMsgSelectTillDate") }
        }

    }
    
    struct DiamondList {
        
        static var KMsgSelectDiamond : String {
            get { return LanguageManager.localizedString("KMsgSelectDiamond") }
        }
        
        static var KLblDeleteStone : String {
            get { return LanguageManager.localizedString("KLblDeleteStone") }
        }
        
        static var KMsgSingleCannotCompare : String {
            get { return LanguageManager.localizedString("KMsgSingleCannotCompare") }
        }
        
        static var KMsgReq2StoneToCompare : String {
            get { return LanguageManager.localizedString("KMsgReq2StoneToCompare") }
        }
        
        static var KMsgToCompareMaximumDiamonds : String {
//            You can compare maximum 15 diamonds.
            get { return LanguageManager.localizedString("KMsgToCompareMaximumDiamonds") }
        }
        
        static var KLblMoveAllToEnquiry : String {
            get { return LanguageManager.localizedString("KLblMoveAllToEnquiry") }
        }
        
        static var KLblMoveSelectedToEnquiry : String {
            get { return LanguageManager.localizedString("KLblMoveSelectedToEnquiry") }
        }
        
        static var KLblViewEnquiry : String {
            get { return LanguageManager.localizedString("KLblViewEnquiry") }
        }
        
        static var KLblMoveAllToCart : String {
            get { return LanguageManager.localizedString("KLblMoveAllToCart") }
        }
        
        static var KLblMoveSelectedToCart : String {
            get { return LanguageManager.localizedString("KLblMoveSelectedToCart") }
        }
        
        static var KLblViewCart : String {
            get { return LanguageManager.localizedString("KLblViewCart") }
        }
        
        static var KLblMoveAllToWatchlist : String {
            get { return LanguageManager.localizedString("KLblMoveAllToWatchlist") }
        }
        
        static var KLblMoveSelectedToWatchlist : String {
            get { return LanguageManager.localizedString("KLblMoveSelectedToWatchlist") }
        }
        
        static var KLblMoveAllToPlaceOrder : String {
            get { return LanguageManager.localizedString("KLblMoveAllToPlaceOrder") }
        }
        
        static var KLblMoveSelectedToPlaceOrder : String {
            get { return LanguageManager.localizedString("KLblMoveSelectedToPlaceOrder") }
        }
        
        static var KLblThisStonePlaceOrder : String {
            get { return LanguageManager.localizedString("KLblThisStonePlaceOrder") }
        }
        
        static var KLblViewWatchlist : String {
            get { return LanguageManager.localizedString("KLblViewWatchlist") }
        }
        
        static var KLblViewComment : String {
            get { return LanguageManager.localizedString("KLblViewComment") }
        }
        
        static var KLblViewReminder : String {
            get { return LanguageManager.localizedString("KLblViewReminder") }
        }
        static var KLblViewOnMemo : String {
            get { return LanguageManager.localizedString("KLblViewOnMemo") }
        }
        static var KLblViewHold : String {
            get { return LanguageManager.localizedString("KLblViewHold") }
        }
        static var KLblViewOffer : String {
            get { return LanguageManager.localizedString("KLblViewOffer") }
        }
        
        static var KLblDeleteSelectedWatchList : String {
            get { return LanguageManager.localizedString("KLblDeleteSelectedWatchList") }
        }
        
        static var KLblDeleteAllWatchList : String {
            get { return LanguageManager.localizedString("KLblDeleteAllWatchList") }
        }
        
        static var KLblDeleteSelectedCart : String {
            get { return LanguageManager.localizedString("KLblDeleteSelectedCart") }
        }
        
        static var KLblDeleteAllCart : String {
            get { return LanguageManager.localizedString("KLblDeleteAllCart") }
        }
        
        static var KLblDeleteSelectedEnquiry : String {
            get { return LanguageManager.localizedString("KLblDeleteSelectedEnquiry") }
        }
        
        static var KLblDeleteAllEnquiry : String {
            get { return LanguageManager.localizedString("KLblDeleteAllEnquiry") }
        }
        
        static var KLblDeleteSelectedComment : String {
            get { return LanguageManager.localizedString("KLblDeleteSelectedComment") }
        }
        
        static var KLblDeleteAllComment : String {
            get { return LanguageManager.localizedString("KLblDeleteAllComment") }
        }
        
        static var KLblDeleteSelectedReminder: String {
            get { return LanguageManager.localizedString("KLblDeleteSelectedReminder") }
        }
        
        static var KLblDeleteAllReminder : String {
            get { return LanguageManager.localizedString("KLblDeleteAllReminder") }
        }
        
        static var KLblViewInvoice : String {
            get { return LanguageManager.localizedString("KLblViewInvoice") }
        }
        static var KLblDeleteSelectedStones : String {
            get { return LanguageManager.localizedString("KLblDeleteSelectedStones") }
        }
        
    }
    
    //MARK:- Trasnport Form
    struct TransportForm {
        
        static var KPersonName : String {
            get { return LanguageManager.localizedString("KPersonName") }
        }
        static var KCustomerType : String {
            get { return LanguageManager.localizedString("KCustomerType") }
        }
        static var KDate : String {
            get { return LanguageManager.localizedString("KDate") }
        }
        static var KPickupPoint : String {
            get { return LanguageManager.localizedString("KPickupPoint") }
        }
        static var KDropPoint : String {
            get { return LanguageManager.localizedString("KDropPoint") }
        }
        static var KMobileNo1 : String {
            get { return LanguageManager.localizedString("KMobileNo1") }
        }
        static var KMobileNo2 : String {
            get { return LanguageManager.localizedString("KMobileNo2") }
        }
        static var KAnyComment : String {
            get { return LanguageManager.localizedString("KAnyComment") }
        }
        static var Prefix : String {
            get { return LanguageManager.localizedString("KHintPrefix") }
        }
    }
    
    //MARK: Address
    struct Address {
        
        static var KHintCountry : String {
            get { return LanguageManager.localizedString("KHintCountry") }
        }
        
        static var KHintState : String {
            get { return LanguageManager.localizedString("KHintState") }
        }
        
        static var KHintCity : String {
            get { return LanguageManager.localizedString("KHintCity") }
        }
        
        static var KHintPincode : String {
            get { return LanguageManager.localizedString("KHintPincode") }
        }
        
        static var KLblRemark : String {
            get { return LanguageManager.localizedString("KLblRemark") }
        }
        
        static var KLblAddressType : String {
            get { return LanguageManager.localizedString("KLblAddressType") }
        }
        
        static var KLblStreet : String {
            get { return LanguageManager.localizedString("KLblStreet") }
        }
        
        static var KPincode : String {
            get { return LanguageManager.localizedString("KHintPincode") }
        }
        
        static var KSelectAddressType : String {
            get { return LanguageManager.localizedString("KSelectAddressType") }
        }
        
        static var KMSgStreet : String {
            get { return LanguageManager.localizedString("KMSgStreet") }
        }
        
        static var KDeleteAddress : String {
            get { return LanguageManager.localizedString("KDeleteAddress") }
        }
    }
    
    struct CutomCode {
        static let kExpand          = "EXPAND"
        static let kAllShape        = "AllShape"
    }
}
