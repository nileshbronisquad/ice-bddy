//
//  AppConstants.swift
//  kiranDiamond
//
//  Created by Coruscate on 28/02/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import Foundation
import UIKit

struct AppConstants {
    
    static let flagUrl                              = "http://flagpedia.net/data/flags/normal/"
//    static let serverURL                            = "http://60.254.95.5:7027" //Test Server
//    static let serverURL                            = "http://192.168.0.14:7027" //Local
    
    #if DEBUG
//    static let serverURL                            = "http://mvd.coruscate.work:7079" //Live
    static let serverURL                            = "http://admin.simgems.com" //Live

    #else
    static let serverURL                            = "http://admin.simgems.com" //Live
    #endif
    static let path                                 = "http://admin.simgems.com/data"
    static let webpageURL                           = "http://www.simgems.com/"
    static let sqlURL                               = "https://sync.hk.co"
    static let uploadURL                            = "http://admin.simgems.com"
    
    static let LiveAppIdOneSignal                   = "70b48ecd-4c1d-427a-8167-ef9f543442e6"
    
    static let mainImage                            = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/still.jpg";
    static let arrowBlack                           = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/Arrow_Black_BG.jpg";
    static let assetWhite                           = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/ASET_White_BG.jpg";
    static let darkFieldWhite                       = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/Dark_Field_White_BG.jpg";
    static let heartBlack                           = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/Heart_Black_BG.jpg";
    static let idealWhite                           = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/IDEAL_White_BG.jpg";
    static let officeLightBlack                     = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/Office_Light_Black_BG.jpg";

    static let imagePath                            = "https://diamanti.s3.amazonaws.com/images/diamond/";
    static let videoPath                            = "https://d2wnxtmh9j0s64.cloudfront.net/imaged/%@/video.mp4";
    static let certificatePath                      = "http://diamanti.s3.amazonaws.com/certificates/";
    static let roughImagePath                       = "http://diamanti.s3.amazonaws.com/images/RoughImage/";
    static let videoDownloadPath                    = "http://diamanti.s3.amazonaws.com/video/mp4/";
    static let QRCodeUrl                            = "http://www.simgems.com/diamond-detail/";
    static let ShapePlaceHolderImgPath              = "http://admin.simgems.com/shape/%@.jpg";
    
    static let iTunesUrl                            = "https://apps.apple.com/us/app/hk-co/id1010715749?ls=1"
    static let AppName                              = "Sim Gems"
    static let startingDate                         = "1970-01-01T00:00:00.000Z"
    static let driftId                              = "ras5fp6zfvy3"
    
    static let DeviceTypeIphone                     = 3
    
    static var themeType                            = 1
    //MARk: Social
    struct social {
        
        static let faceBook                             = "https://www.facebook.com/HKDiamonds"
        static let instaGram                            = "https://www.instagram.com/hkdiamonds"
        static let youTube                              = "https://www.youtube.com/c/DiamondByHK"
        static let twitter                              = "https://www.twitter.com/HKDiamonds"
        static let linkedIn                             = "https://www.linkedin.com/company/hkdiamonds/"
        static let google                               = "https://plus.google.com/+Diamondbyhk"
    }
    
    //MARK: Contact
    struct ContactDetail {
        
        static let mobile                             = "912243004300"
        static let email                              = "info@hk.co"
        static let salesEmail                         = "sales@hk.co"
        static let skype                              = "https://www.youtube.com/c/DiamondByHK"
    }
    
    //MARk: How to Use
    struct howToUse {
        
        static let quickSearch                           = "https://youtu.be/thTcSmllBho"
        static let offlineMode                           = "https://youtu.be/DsHXj_tQOSk"
        static let offlineOrder                          = "https://youtu.be/NWIiznjUkMg"
    }
    
    //Static Web Page
    struct webPage {
        
        static let SignUp            = "/device/signup"
        static let contactUs         = "/device/contact-us"
        static let about             = "/device/about-hk"
        static let news              = "/device/news-media"
        static let termsAndCondition = "/device/terms-and-condition"
        static let help              = "/device/help"
        static let virtualTour       = "https://melzo.com/dekho/5d41974ebf89b9000f31c5f2"
        static let feedback          = "feedback"//"/device/feedback"
        static let peopleDetail      = "/device/member-profile/"
        
    }
    
    //MARK:  API
    struct URL {
        
        static let Login                            = "/device/v1/auth/login"
        static let UpdateToken                      = "/device/v1/auth/update-token"
        static let Guest                            = "/guest/auth/login"
        static let SignUp                           = "/device/v1/user/register"
        static let ForgotPassword                   = "/auth/forgot-password"
        static let Logout                           = "/device/v1/auth/logout"
        static let GetProfile                       = "/device/v1/user/view"
        static let GetCompanyDetail                 = "/device/v1/user/profile"
        static let CompanyUpdate                    = "/device/v1/user/profile/update"
        static let ProfileUpdate                    = "/device/v1/user/update"
        static let ChangePassword                   = "/device/v1/reset-password-by-user"
        static let UploadFile                       = "/upload-file"
        static let diamondList                      = "/device/v1/diamond/paginate"
        static let masterSync                       = "/device/v1/masterSync"
        static let DiamondInquiry                   = "/device/v1/diamond-track/create"
        static let DiamondMemoHold                  = "/device/v1/diamond-block/create"
        static let DiamondMemoHoldPaginate          = "/device/v1/diamond-block/paginate"
        static let TrackUpdate                      = "/device/v1/diamond-track/update"
        static let savedSearchList                  = "/device/v1/diamond/search/list"
        static let deleteSearchList                 = "/device/v1/diamond/search/delete"
        static let saveSearch                       = "/device/v1/diamond/search/upsert"
        static let addDemand                        = "/device/v1/diamond/search/upsert"
        static let quickSearch                      = "/device/v1/diamond/quick-search"
        static let ValidateSignup                   = "/device/v1/check/credentials"
        static let GetCountryList                   = "/device/v1/country/paginate"
        static let GetStateList                     = "/device/v1/state/paginate"
        static let GetCityList                      = "/device/v1/city/paginate"
        static let DiamondBlockPaginate             = "/device/v1/diamond-block/paginate"
        static let DiamondTrackPaginate             = "/device/v1/diamond-track/paginate"
        static let MatchPair                        = "/device/v1/match-pair/diamond/filter"
        static let TrackDashboard                   = "/device/v1/track-dashboard"
        static let DownloadExcel                    = "/device/v1/diamond/excel"
        static let Comment                          = "/device/v1/diamond-comment/upsert"
        static let CommentList                      = "/device/v1/diamond-comment/by-user" 
        static let MessageToMD                      = "/front/contactus/create"
        static let Suggestion                       = "/front/contactus/create"
        static let GetSalesPersonList               = "/admin/account/sales-person"
        static let GetSalesDepartmentList           = "/admin/account/sales-department"
        static let GetAppointmentForList            = "/apis/news-event/paginate"
        static let GetCabinList                     = "/device/v1/cabin-schedule/booked"
        static let AddAppointment                   = "/device/v1/cabin-schedule/create"
        static let GetAppointmentList               = "/device/v1/cabin-schedule/list"
        static let CancelAppointment                = "/device/v1/cabin-schedule/cancel"
        static let DeleteDiamond                    = "/device/v1/diamond-track/delete"
        static let DeleteBlockDiamond               = "/device/v1/diamond-block/delete"
        static let DeleteCommentDiamond             = "/device/v1/diamond-comment/delete"
        static let PriceCalculation                 = "/device/v1/rap-price/paginate"
        static let OrderCalculation                 = "/apis/setting/3"
        static let PlaceOrder                       = "/admin/diamond-confirm/request"
        static let OrderList                        = "/admin/memo/paginate"
        static let OrderPdf                        = "/admin/order/pdf"
        static let OrderExcel                        = "/admin/order/excel"
        static let HospitalityList                  = "/device/v1/product/paginate"
        static let HospitalityPlaceOrder            = "/device/v1/product-order/create"
        static let CreateTransport                  = "/device/v1/transport/create"
        static let Analytics                        = "/apis/analytics/create"
        static let dashboard                        = "/device/v1/user/dashboard"
        static let PurchaseList                     = "/admin/hk/purchase/list"
        static let ExistingWatchListName            = "/device/v1/diamond-track/names"
        
        static let AddressList                      = "/admin/address/paginate"
        static let AddAddress                       = "/admin/address/create"
        static let DeleteAddress                    = "/admin/address/destroy"
        static let UpdateAddress                    = "/admin/address/"
        static let upsertPlayerId                   = "/device/v1/user/player"
        static let NotificationList                 = "/admin/notification/list"
        static let PurchaseGraph                    = "/admin/hk/purchase/graph"
        static let VersionUpdate                    = "/device/v1/version"
        static let Invoice                          = "/admin/memo/invoice"
        
        static let HospitalityOrders                = "/device/v1/product-order/paginate"
        static let CancelOrder                      = "/device/v1/product-order/update"
        
        static let diamondTrackGetRequest           = "/device/v1/diamond-track/get-requests"
        
        //Manage diamond
        static let ManageDiamond                    = "/device/v1/admin/vendor-diamond/paginate"
        static let UpdateDiamond                    = "/device/v1/admin/vendor-diamond/update"
        static let UpdateDiamondsStatus             = "/device/v1/admin/vendor-diamond/update-status"
        static let DeleteMyDiamond                  = "/device/v1/admin/vendor-diamond/delete"
        
        static let MemberList                       = "/device/v1/user/member-list"
        
        //Sub User Details
        static let SubUserList                      = "/device/v1/user/sub-user-list"
        static let AddSubUser                       = "/device/v1/user/add-sub-user"
        static let DeleteSubUser                    = "/device/v1/user/delete-sub-user"
        static let UpdateSubUser                    = "/device/v1/user/update-sub-user"
        
        static let SellerList                       = "/device/v1/account/paginate"
        
        static let CheckDuplicateAccount            = "/apis/account/checkDuplicate"
        
        static let CancelHoldMemoRequest            = "/admin/diamond-block/delete"
        static let OrderHoldMemoRequest             = "/admin/diamond-confirm/request"
        static let ReleaseHoldMemoRequest           = "/admin/diamond-block/change-status"
        static let CancelOfferRequest               = "/admin/diamond-track/delete"
        static let AcceptRejectOffer                = "/admin/diamond-track/status-update"
        static let xRayMail                         = "/admin/diamond/x-ray-mail"
        static let ConvertToZip                     = "/admin/covert-to-zip"

    }
    
    struct CertificateType {
        static let DIAMOND_IGI = (name: "IGI", basePath: "https://igi.org/verify.php?r=")
        static let DIAMOND_GSI = (name: "GSI", basePath: "http://wg.gemscience.net/vr/veri.aspx")
        static let DIAMOND_GIA = (name: "GIA", basePath: "https://www.gia.edu/report-check?reportno=")
    }
    
    //MARK:  User Default
    struct UserDefaultKey {
        
        static let isUserLoggedIn                    = "isUserLoggedIn"
        static let isIntroDone                       = "isIntroDone"
        static let userInfo                          = "userInfo"
        static let GuestUserId                       = "GuestUserId"
        static let tokenKey                          = "token"
        static let SkipUpdate                        = "SkipUpdate"
        static let DownloadProgress                  = "DownloadProgress"
        static let OfflineExpiry                     = "OfflineExpiry"
        static let RememberMe                        = "RememberMe"
        static let Username                          = "Username"
        static let Password                          = "Password"
        static let TouchId                           = "IsTouchId"
        static let PlayerId                          = "PlayerId"
        static let SelectedLanguageKey               = "SelectedLanguageKey"
        static let languageChange                    = "languageChange"
        
        //Take tour constants
        static let HomeScreenTakeTour                = "HomeScreenTakeTour"
        static let ProfileTakeTour                   = "ProfileTakeTour"
        static let CompareTakeTour                   = "CompareTakeTour"
        static let EnquiryTakeTour                   = "EnquiryTakeTour"
        static let FilterTakeTour                    = "FilterTakeTour"
        static let FilterSavedSearchTakeTour         = "FilterSavedSearchTakeTour"
        static let DiamondListTakeTour               = "DiamondListTakeTour"
        static let DiamondDetailTakeTour             = "DiamondDetailTakeTour"
        static let LeftMenuTakeTour                  = "LeftMenuTakeTour"
        static let DemandList                        = "DemandList"
        static let PeopleList                        = "PeopleList"
        static let TradeList                         = "TradeList"
        static let TrackScreenTakeTour               = "TrackScreenTakeTour"
        
        
        static let ShowPrice                         = "ShowPrice"
        static let AllStockDownload                  = "AllStockDownload"
        static let PlaceOrder                        = "PlaceOrder"
        static let DashBoardList                     = "DashBoardList"
        static let Seller                     = "Seller"
        static let GraphFound                        = "GraphFound"
        static let PurchaseSnapshot                  = "PurchaseSnapshot"
        static let WatchListName                     = "WatchListName"
        static let HospitalityAutoSwipe              = "HospitalityAutoSwipe"
        
    }
    
    //MARK: Hospitality Orders
    struct HospitalityOrders {
        
        static let Pending                          = 0
        static let Completed                        = 1
        static let Cancelled                        = 3
    }
    
    struct OfferStatus{
        static let Applied                          = 1
        static let Accepted                         = 2
        static let Rejected                         = 3
    }
    
    //Hospitality type
    struct CategoryType {
        static let  WATER = "Water"
        static let  TEA = "Tea"
        static let  SODA = "Soda"
        static let  BISCUITS = "Biscuits"
        static let  SPL = "Spl"
        static let  COFFEE = "Coffee"
        static let  SNACKS = "Snacks"
        static let  STATIONARY = "Stationary"
        static let  OTHER = "OTHER"
        static let  CLEANING = "CLEANING"
        static let AC = "AC"
    }
    
    //MARK: Sync Date
    struct SyncDate {
        
        static let master                           = "masterSyncDate"
        static let revisionNumber                   = "RevisionNumber"
    }
     
    struct PageLimit{
        static let page                             = 1
        static let limit                            = 100
        static let exclusiveDiamondLimit            = 100
        static let Cartlimit                        = 5000
    }
    struct analytics{
        static let MYORDER = "MyOrder"
        static let section = "Diamonds"
        static let action = "list"
        
    }
    
    //Master Code
    struct MasterCode {
        
        static let shape                    = "SHAPE"
        static let color                    = "COLOR"
        static let fancyColor               = "FANCY_COLOR"
        static let intensity                = "INTENSITY"
        static let overTone                 = "OVERTONE"
        static let clarity                  = "CLARITY"
        static let fluorescence             = "FLUORESCENCE"
        static let colorShade               = "SHADE"
        static let lab                      = "LAB"
        static let cut                      = "CUT"
        static let polish                   = "POLISH"
        static let symmetry                 = "SYMMETRY"
        static let location                 = "LOCATION"
        static let keyToSymbol              = "KEY_TO_SYMBOLS"
        static let blackTable               = "BLACK_INCLUSION"
        static let blackCrown               = "BLACK_INCLUSION_CROWN"
        static let whiteTable               = "WHITE_INCLUSION_TABLE"
        static let whiteCrown               = "WHITE_INCLUSION_CROWN"
        static let milky                    = "MILKEY"
        static let tableOpen                = "OPEN_TABLE"
        static let crownOpen                = "OPEN_CROWN"
        static let pavilionOpen             = "OPEN_PAVILION"
        static let origin                   = "ORIGIN"
        static let eyeClean                 = "EYECLEAN"
        static let hAndA                    = "H_AND_A"
        static let girdle                   = "GIRDLE"
        static let girdleCondition          = "GIRDLE_COND"
        static let culet                    = "CULET"
        static let culetCondition           = "CULET_COND"
        
    }
    
    //Address Constants
    struct AddressConstants {
        
        static let HOME         = "HOME"
        static let ACCOUNT      = "ACCOUNT"
        static let BUYER        = "BUYER"
        static let SHIPPING     = "SHIPPING"
        static let BILLING      = "BILLING"
        static let OTHER        = "OTHER"
        
    }
    
    //Diamond Status
    struct SavedSearchType{
        
        static let RESENTSEARCH    = 1
        static let SAVESEARCH      = 2
        static let DEMAND          = 3
    }
    
    //Diamond Status
    struct DiamondStatus{
        
        static let BUSINESS        = 1
        static let NEW             = 2
        static let AVAILABLE       = 3
        static let INLAB           = 4
    }
    
    //Track Status
    struct DiamondTrackStatus{
        
        static let CART        = 1
        static let WATCHLIST   = 2
        static let REQUEST     = 3
        static let OFFER       = 4
        static let REMINDER    = 5
        static let INQUIRY     = 6
        static let MEMO        = 7
        static let HOLDFORME   = 8
        static let COMMENT     = 9
        static let NOTE        = 99

        static let ORDER       = 1000
    }
    
    struct DiamondBlockType {
        static let HOLD = 1
        static let MEMO = 2
    }
    
    //Block Status
    struct BlockType{
        
        static let HOLD        = 1
        static let MEMO   = 2
    }

    
    //Purchase Graph Type
    struct PurchaseGraphType{
        
        static let oneMonth         = 1
        static let sixMonth         = 2
        static let twelveMonth      = 3
        static let twentyFourMonth  = 4
    }
    
    struct FilterRequest {
        
        static let shape                    = "shp"
        static let carat                    = "or"
        static let color                    = "col"
        static let fancyColor               = "fcCol"
        static let isFancyColor             = "isFcCol"
        static let intentsity               = "inten"
        static let overTone                 = "ovrtn"
        static let clarity                  = "clr"
        static let colorShade               = "shd"
        static let cut                      = "cut"
        static let excludeFilter            = "excludeFilter"
        static let polish                   = "pol"
        static let symmetry                 = "sym"
        static let THREEEX                  = "3EX"
        static let TWOEX                    = "2EX"
        static let THREEVG                  = "3VG+"
        static let fluorescence             = "flu"
        static let lab                      = "lb"
        static let pricePrCarat             = "ctPr"
        static let amount                   = "amt"
        static let marketingBack            = "back"
        static let newArrivalDate           = "marketingIssueDate"
        static let location                 = "counId"
        static let totalDepth               = "depPer"
        static let table                    = "tblPer"
        static let keyToSymbol              = "kToSArr"
        static let diameterMin              = "diameterMin"
        static let diameterMax              = "diameterMax"
        static let gridlePer                = "grdlPer"
        static let ratio                    = "ratio"
        static let blackTable               = "blkInc"
        static let blackCrown               = "blkCrn"
        static let whiteTable               = "wTbl"
        static let whiteCrown               = "wCrn"
        static let milky                    = "mlk"
        static let crownAngle               = "cAng"
        static let pavilionAngle            = "pAng"
        static let crownHeight              = "cHgt"
        static let pavilionHeight           = "pHgt"
        static let tableOpen                = "openTableId"
        static let crownOpen                = "openCrownId"
        static let pavilionOpen             = "openPavilionId"
        static let roughOrigin              = "roughOriginId"
        static let eyeClean                 = "eCln"
        static let hAndA                    = "hA"
        static let sizeId                   = "sizeId"
        static let lusterId                 = "lusterId"
        static let blackInclusionId         = "blackInclusionId"
        static let gridle                   = "grdl"
        static let gridleCondition          = "grdlCond"
        static let culet                    = "cult"
        static let culetCondition           = "cultCond"
        static let length                   = "length"
        static let width                    = "width"
        static let depth                    = "depth"
        static let height                   = "height"
        static let companyCods              = "vnd"
        static let sellerStoneNumber        = "vStnId"
        static let certImage                = "certImage"
        static let image                    = "image"
        static let blockedOnly              = "blocked_only"
        static let stockId                  = "stockId"
    }
    
    //Hold Status
    struct HoldMemoStatus{
        static let Pending = 1
        static let Approved = 2
        static let Reject  = 3
        static let Release = 4
    }
    
    
    //Modules
    struct Module {
        
        static let dashboard            = "dashboard"
        static let searchDiamond        = "searchDiamond"
        static let quickSearch          = "quickSearch"
        static let fancySearch          = "fancySearch"
        static let mySavedSearch        = "mySavedSearch"
        static let myDemand             = "myDemand"
        static let searchResult         = "searchResult"
        static let compare              = "compare"
        static let showSelected         = "showSelected"
        static let matchPair            = "matchPair"
        static let iAmLucky             = "iAmLucky"
        static let parcelList           = "parcelList"
        static let newGoods             = "newGoods"
        static let bestOfHK             = "bestOfHK"
        static let upcomingDiamonds     = "upcomingDiamonds"
        static let cart                 = "cart"
        static let reminder             = "reminder"
        static let watchlist            = "watchlist"
        static let comment              = "comment"
        static let enquiry              = "enquiry"
        static let onmemodiamond        = "onmemodiamond"
        
        //Hand made constants
        static let order                = "order"
        static let purchase             = "purchase"
        static let transport            = "transport"
        static let hospitality          = "hospitality"
        static let OfflineStock         = "offlineStock"
        static let PriceCalculator      = "pricecalculator"
        static let stoneoftheday        = "stoneoftheday"
        static let appointment          = "appointment"
        static let roughImage           = "RoughImage"
        
        
        static let shipment             = "shipment"
        static let diamondHistory       = "diamondHistory"
        static let inventory            = "inventory"
        static let pricing              = "pricing"
        static let XRayZip              = "XRayZip"
        
    }
    
    //MARK: My diamonds update status
    struct DiamondUpdateStatus {
        static let Availbale            = "A"
        static let Memo                 = "M"
        static let Hold                 = "H"
        static let NotAvailable         = "NA"
    }
    
    struct MemoStatus {
        static let Allow                = 1
        static let NotAllow             = 2
        static let MyMemo               = 3
        static let Another              = 4
    }

    //MARK: USer Types
    struct UserTypes {
        
        static let Customer                 = "Customer"
        static let Guest                    = "Guest"
        static let TopManagement            = "Top Management"
        static let Coordinator              = "Co-Ordinator"
        static let Employee                 = "Employee"
    }
    
    struct UserTypeConstant {
        
        static let SUPER_ADMIN              = 1
        static let ADMIN                    = 2
        static let SUB_USER                 = 3
        static let PRIMARY                  = 4
        static let PERMANENT_GUEST          = 5
        static let EMPLOYEE                 = 6
        static let API_USER                 = 7
    }
    
    //Bottom Menu Constants
    struct BottomMenuConstants {
        static let AddToCart                = 1
        static let WatchList                = 2
        static let Request                  = 3
        static let Comment                  = 4
        static let Reminder                 = 5
        static let Enquiry                  = 6
        static let Compare                  = 7
        static let PlaceOrder               = 8
        static let Reset                    = 11
        static let Delete                   = 17
        static let Share                    = 19
        static let Download                 = 20
        static let ShowSelected             = 21
        static let RequestMemo              = 22
        static let MakeAnOffer              = 23
        static let HoldForMe                = 24
        static let Dashboard                = 27
        static let Search                   = 28
        static let SavedSearch              = 29
        static let Notification             = 30
        static let Profile                  = 31
        static let UpdateNote               = 32
        static let EditSearch                    = 33
        static let Track                    = 34
        static let MakeAnOfferOnMemo              = 35


    }
    
    //MARK: Push notification constants
    struct Notification {
//        static let MODULE_TYPE_SEARCH = 1
//        static let MODULE_TYPE_DIAMOND_COMPARE = 2
        static let MODULE_TYPE_WATCHLIST = 3
//        static let MODULE_TYPE_ADD_ENQUIRY = 4
        static let MODULE_TYPE_REMINDER = 5
        static let MODULE_TYPE_LUCKY = 6
        static let MODULE_TYPE_STONE_OF_DAY = 7
        static let MODULE_TYPE_EXCLUSIVE_STONE = 8
//        static let MODULE_TYPE_FANCY_DIAMOND = 9
        static let MODULE_TYPE_NEW_GOODS = 10
        static let MODULE_TYPE_MATCH_PAIR = 11
//        static let MODULE_TYPE_OFFLINE_STOCK = 12
//        static let MODULE_TYPE_PRICE_CALC = 13
//        static let MODULE_TYPE_CONCIERGE = 14
//        static let MODULE_TYPE_QR_CODE = 15
//        static let MODULE_TYPE_MORE = 16
//        static let MODULE_TYPE_RATE_US = 17
//        static let MODULE_TYPE_CONTACT_US = 18
//        static let MODULE_TYPE_DIAMOND_DETAIL = 19
//        static let MODULE_TYPE_QUICK_SEARCH = 20
//        static let MODULE_TYPE_PROFILE = 21
        static let MODULE_TYPE_COMMENT = 22
        static let MODULE_TYPE_ENQUIRY = 23
        static let MODULE_TYPE_CART = 24
//        static let MODULE_TYPE_RECENT_SEARCH = 25
//        static let MODULE_TYPE_SAVED_SEARCH = 26
//        static let MODULE_TYPE_OFFLINE_STOCK_SEARCH = 27
//        static let MODULE_TYPE_OFFLINE_STOCK_LIST = 28
//        static let MODULE_TYPE_OFFLINE_STOCK_HISTORY = 29
//        static let MODULE_TYPE_HOSPITALITY = 30
//        static let MODULE_TYPE_TRANSPORT_ROSTER = 31
        static let MODULE_TYPE_APPOINTMENTS = 32
//        static let MODULE_TYPE_PERSONS = 33
//        static let MODULE_TYPE_ABOUT_HK = 34
//        static let MODULE_TYPE_NEWS = 35
//        static let MODULE_TYPE_TERMS_CONDITION = 36
        static let MODULE_TYPE_ORDER = 37
        static let MODULE_TYPE_MEMO = 39
        static let MODULE_TYPE_HOLD = 38
        static let MODULE_TYPE_OFFER = 97
        
        
        
    }
    
    //MARK: Device Constant
    enum UIUserInterfaceIdiom : Int {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize 
    {
        static var SCREEN_WIDTH: CGFloat {
            get {
                return UIScreen.main.bounds.size.width
            }
        }
        
        static var SCREEN_HEIGHT: CGFloat {
            get {
                return UIScreen.main.bounds.size.height
            }
        }
        
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_XS         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPHONE_XS_MAX     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static var hasSafeArea: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        
        return false
    }
    
    struct ColorConstant {
        static let ColorYellow          = "eae448"
        static let ColorRed             = "e98388"
        static let ColorBlue            = "94b1e3"
        static let ColorGreen           = "9eda96"
        static let ColorPurple          = "ad99dc"
        static let ColorOrange          = "f5a66f"
    }
    
    struct EnquiryStatus {
        static let New              = 0
        static let Open             = 1
        static let Won              = 2
        static let Closed           = 3
    }
    
    struct EnquiryGroupBy {
        static let Buyer            = 0
        static let Date             = 1
    }
    
    
}

