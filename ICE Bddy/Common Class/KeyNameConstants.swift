//
//  KeyNamesConstants.swift
//  Parth Hirpara
//

import Foundation
import UIKit



@available(iOS 10.0, *)
let APP_DELEGATE                =   UIApplication.shared.delegate as! AppDelegate
let kLoginStoryBoard            =   UIStoryboard(name: "Login", bundle: nil)

public let defaultShimmerValue  :   Int = 10

//MARK:- Alert Button

public let kOK                      :   String  =   "Ok"
public let kCancel                  :   String  =   "Cancel"
public let kDelete                  :   String  =   "Delete"
public let kEdit                    :   String  =   "Edit"


//MARK:- APP NAME
public let APPNAME :String = "SherApp"

let loadercolor = [themeBlueColor, themeGreenColor]

//MARK:- COLOR NAMES
let APP_COLOR       =   Color_Hex(hex: "#002E5D")
let themeBlueColor  =   APP_COLOR
let themeGreenColor =   Color_Hex(hex: "#5EDA75")

struct ObserverName {
    static let kcontentSize     = "contentSize"
}

//MARK:- User Defaut
public  let kUD_UserType            :   String      =   "userType"
public  let UD_Device_Token         :   String      =   "DeviceToken"
public  let kNotificationMenuSelect :   String      =   "NotificationMenuSelect"
//public  let


//MARK:- Model
public  let kModelDocumentAttachment      :   String      =   "Document"
public  let kModelPhotoAttachment         :   String      =   "Photo"

public  let kSendMailType                 :   String      =   "1"
public  let kInboxMailType                :   String      =   "2"
public  let kSendMail                     :   String      =   "SentMail"
public  let kReplayMailKey                :   String      =   "ReplayMail"
public  let kSendRecommendation           :   String      =   "SendRecommendation"
public  let kAcceptRecommendation         :   String      =   "AcceptRecommendation"
public  let kApplyPostJob                 :   String      =   "ApplyPostJob"
public  let kSendPostJobOffer             :   String      =   "SendPostJobOffer"
public  let kAcceptPostJobOffer           :   String      =   "AcceptPostJobOffer"
public  let kRejectPostJobOffer           :   String      =   "RejectPostJobOffer"




//MARK:-


//MARK:- STORY BOARD NAMES
public let SB_MAIN              :   String  =   "Main"
public let SB_LOGIN             :   String  =   "Login"
public let SB_CMS               :   String  =   "CMS"


//MARK:- Model
public  let kModelUserData          :   String      =   "kModelUserData"
public  let kModelUserQRData          :   String      =   "kModelUserQRData"
public  let kModelUserDocumentData  :   String      =   "kModelUserDocumentData"
public  let kModelUserVehicleData   :   String      =   "kModelUserVehicleData"
public  let kUserStatusData         :   String      =   "kUserStatusData"
public  let UD_LAT                  :   String  =    "LAT"
public  let UD_LOG                  :   String  =    "LOG"
//MARK:- ViewControl

//Login
public let idIntroVC                :   String  =   "IntroVC"
public let idLoginVC                :   String  =   "LoginVC"
public let idSignupVC               :   String  =   "SignupVC"
public let idVerificationVC         :   String  =   "VerificationVC"
public let idForgotPasswordVC       :   String  =   "ForgotPasswordVC"
public let idSetNewPasswordVC       :   String  =   "SetNewPasswordVC"
public let idRegisteringTypeVC      :   String  =   "RegisteringTypeVC"
public let idCompliteProfileVC      :   String  =   "CompliteProfileVC"
public let idPickerVC               :   String  =   "PickerVC"

//Main
public let idMenuVC                 :   String  =   "MenuVC"
public let idDashboardVC            :   String  =   "DashboardVC"
public let idHomeVC                 :   String  =   "HomeVC"
public let idLoanRequestVC          :   String  =   "LoanRequestVC"
public let idMyLoanVC               :   String  =   "MyLoanVC"
public let idNotificationVC         :   String  =   "NotificationVC"
public let idMyLoanSubVC            :   String  =   "MyLoanSubVC"
public let idMyLeaderVC             :   String  =   "MyLeaderVC"
public let idPaymentVC              :   String  =   "PaymentVC"
public let idMyProfileVC            :   String  =   "MyProfileVC"
public let idSecurityVC             :   String  =   "SecurityVC"
public let idNotificationSetttingVC :   String  =   "NotificationSetttingVC"
public let idMyWalletVC             :   String  =   "MyWalletVC"
public let idLoanDetailVC           :   String  =   "LoanDetailVC"
public let idLoanRequestDetailsVC   :   String  =   "LoanRequestDetailsVC"
public let idLenderProfileVC        :   String  =   "LenderProfileVC"
public let idTransactionDetailVC    :   String  =   "TransactionDetailVC"
public let idLoanRequest1VC         :   String  =   "LoanRequest1VC"
public let idLoanRequest2VC         :   String  =   "LoanRequest2VC"
public let idLoanRequest3VC         :   String  =   "LoanRequest3VC"
public let idLoanRequestCancelVC    :   String  =   "LoanRequestCancelVC"
public let idSubscriptionVC         :   String  =   "SubscriptionVC"
public let idSubscriptionHistoryVC  :   String  =   "SubscriptionHistoryVC"
public let idPaymentsBillingVC      :   String  =   "PaymentsBillingVC"
public let idAddNewCardVC           :   String  =   "AddNewCardVC"
public let idFilterVC               :   String  =   "FilterVC"
public let idTutorialVC             :   String  =   "TutorialVC"

//CMS
public let idSupportVC              :   String  =   "SupportVC"
public let idSupportTicketListVC    :   String  =   "SupportTicketListVC"
public let idSupportChatVC          :   String  =   "SupportChatVC"
public let idAddNewTicketVC         :   String  =   "AddNewTicketVC"
public let idFAQDetailVC            :   String  =   "FAQDetailVC"
public let idFeedBackVC             :   String  =   "FeedBackVC"
public let idInviteAFriendVC        :   String  =   "InviteAFriendVC"
public let idAboutUsVC              :   String  =   "AboutUsVC"
public let idResourcesVC            :   String  =   "ResourcesVC"
public let idResourcesDetailVC      :   String  =   "ResourcesDetailVC"
public let idLenderRequestFilterVC  :   String  =   "LenderRequestFilterVC"
public let idPrivacyVC              :   String  =   "PrivacyVC"


let kData       =   "user_data"
let kMessage    =   "message"
let kSuccess    =   "success"
let kToken      =   "token"
let kStatus     =   "status"
let kStatusSuccess = "1"
let kStatusFail = "0"
let kStatusNotVerify = "2"
let kAppId              = "id389801252"

// LogIn & SignUp


let kLoginProfile_URL               =   "login"
let kSignup_URL                     =   "register"
let kVerification_URL               =   "accountVerifyOtp"
let kforgotPassword_URL             =   "forgotPassword"
let kadd_qr_code_URL                =   "add_qr_code"
let kedit_qr_code_URL               =   "edit_qr_code"
let klogout_URL                     =   "logout"
let kchangePassword_URL             =   "changePassword"
let kSocialLogin_URL                =   "socialLogin"





//MARK:- Message
public let kFbIdNotFound                    :   String  =   "Facebook Id Not Found"
public let InternetNotAvailable             :   String  =   "Please connect internet."
public let RetryMessage                     :   String  =   "Something went wrong please try again..."
public let logOutConformation               :   String  =   "Are you sure to logout ?"

public let kEmptyFirstName                  :   String  =   "Please enter your first name."
public let kEmptyLastName                   :   String  =   "Please enter your last name."
public let kEmptyUserName                   :   String  =   "Please enter your user name."
public let kEmptyMobile                      :   String  =   "Please enter your mobile number."
public let kInvalidEmail                    :   String  =   "Please enter valid email address."
public let kEmailempty                           :   String  =   "Please enter email address."

public let kBirthDate                           :   String  =   "Please enter birthday."
public let kDrivingLicese                          :   String  =   "Please enter driving licence number."
public let kDrivingLiceseissue                           :   String  =   "Please enter driving licence issue number."
public let kDrivingLiceseExpire                           :   String  =   "Please enter driving licence expire date."
public let kStreet                           :   String  =   "Please enter street."
public let kLandmark                           :   String  =   "Please enter landmark."
public let kCity                           :   String  =   "Please enter city."
public let kState                           :   String  =   "Please enter state."
public let kCountry                          :   String  =   "Please enter country."
public let kAddress                          :   String  =   "Please enter address."

public let kEname                          :   String  =   "Please enter emergency name."
public let kENum                         :   String  =   "Please enter valid emergency number."
public let kANum                         :   String  =   "Please enter valid alternate number."
public let kEmptyPassword                   :   String  =   "Please enter your password."
public let kInvalidPassword                 :   String  =   "Password length must be 8-15 chacater."
public let kAcceptTerms                     :   String  =   "Please accept terms & conditions."
public let kmsgValidationCodeEmpty          :   String  =   "Please enter verification code."
public let kEmptyConfirmPassword            :   String  =   "Please enter valid confirm password."
public let kPasswordAndConfirmNotSame       :   String  =   "Confirm password not match."
public let kEmptyOldPassword                :   String  =   "Please enter old password."
public let kLogOutMsg                       :   String  =   "Are you sure to logout?"
public let kInvalidAddress                  :   String  =   "Please enter address."

public let kEmptyProfileImage               :   String  =   "Please select profile image."
public let kEmptySSN                        :   String  =   "Please enter social security number."

public let kpasswordOld                       :   String  =   "Please enter old password."
public let kpassword                        :   String  =   "Please enter password."
public let kConpassword                        :   String  =   "Please enter confirm password."
public let kConpasswordmatch                        :   String  =   "password does not match with confirm password"
public let kERel                        :   String  =   "Please enter emergency relative name"
public let kBlood                        :   String  =   "Please choose blood group"
public let kBloodPressure                        :   String  =   "Please choose blood pressure"
public let kDiabites                        :   String  =   "Please choose diabites"
public let kcompanyname                        :   String  =   "Please enter company name"
public let kpolicynumber                        :   String  =   "Please enter policy number"
public let kHeartProblem                        :   String  =   "Please choose heart problem"
public let kOrgan                        :   String  =   "Please choose organ donor"
public let kHealth                        :   String  =   "Please choose health insurance"










