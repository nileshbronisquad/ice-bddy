//
//  ColorScheme.swift
//  PTE
//
//  Created by CS-Mac-Mini on 10/08/17.
//  Copyright © 2017 CS-Mac-Mini. All rights reserved.
//

import UIKit

class ColorScheme: NSObject {
    
    static func colorFromConstant(textColorConstant: String) -> UIColor {
        
        var result = UIColor()
        
        switch textColorConstant {
            
        case "kPriamryColor":
            result = self.kPriamryColor()
            break
        case "kBorderColor":
            result = self.kPriamryColor()
            break
        default:
            result = self.kThemeColor()
        }
        
        return result
    }
    
    //MARK: Private Methods
    
    static func kThemeColor() -> UIColor{
        return ColorConstants.ThemeColor
    }
    
    static func kPriamryColor() -> UIColor{
        return ColorConstants.PrimaryColor
    }
    
    static func kBorderColor() -> UIColor{
        return ColorConstants.BorderColor
    }

   


}
