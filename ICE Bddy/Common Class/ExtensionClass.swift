//
//  ExtensionClass.swift
//  PTE
//
//  Created by CS-Mac-Mini on 10/08/17.
//  Copyright © 2017 CS-Mac-Mini. All rights reserved.
//

import Foundation
import UIKit
import MapKit

extension Data {
    
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension UIButton{
    
    @IBInspectable
    var TextColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            
            setTitleColor(ColorScheme.colorFromConstant(textColorConstant: newValue as String), for: .normal)
        }
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
}

// MARK:-
extension UIViewController  {
    
    // Display Alert Message
    func showAlert(_ title: String,
                   message: String,
                   actions:[UIAlertAction] = [UIAlertAction(title: kOK, style: .cancel, handler: nil)]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
       
    }
    
}

extension UITextField{
    
    @IBInspectable
    var PlaceHolderColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: ColorScheme.colorFromConstant(textColorConstant: newValue as String)])
        }
        
    }
    
    @IBInspectable
    var TextColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            
            textColor = ColorScheme.colorFromConstant(textColorConstant: newValue as String)
        }
    }
    
    @IBInspectable
    var placeholderText: NSString {
        
        get {
            return (placeholder ?? "") as NSString
        }
        set {
            placeholder = LanguageManager.localizedString(newValue as String)
        }
    }
    
    func textFieldLeftPadding(padding : CGFloat) {
        // Create a padding view
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: self.frame.height))
        self.leftViewMode = .always//For left side padding
    }
    
    func textFieldRightPadding(padding : CGFloat) {
        // Create a padding view
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: self.frame.height))
        self.rightViewMode = .always//For left side padding
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

// Statusbar background color
extension UIApplication {
    var statusBarView: UIView? {
        
        if #available(iOS 13.0, *) {

            if let frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame {
                let statusBar1 =  UIView()
                statusBar1.frame = frame
                UIApplication.shared.keyWindow?.addSubview(statusBar1)
                return statusBar1
            }
            
        }
        
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension Array {
    
    func groupBy<G: Hashable>(groupClosure: (Element) -> G) -> [G: [Element]] {
        var dictionary = [G: [Element]]()
        
        for element in self {
            let key = groupClosure(element)
            var array: [Element]? = dictionary[key]
            
            if (array == nil) {
                array = [Element]()
            }
            
            array!.append(element)
            dictionary[key] = array!
        }
        
        return dictionary
    }
}

extension UINavigationController{
    
    func popAllAndSwitch(to : UIViewController){
        
        self.setViewControllers([to], animated: false)
    }
}

extension UIViewController {
    
    func isPresented() -> Bool {
        return self.presentingViewController?.presentedViewController == self
            || (self.navigationController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController)
            || self.tabBarController?.presentingViewController is UITabBarController
    }
    
    
   
}

