//
//  SocialLoginManager.swift
//  iTemp
//
//  Created by Wdev3 on 09/12/20.
//  Copyright © 2020 APPLE. All rights reserved.
//

import Foundation
import AuthenticationServices
import FBSDKLoginKit
import GoogleSignIn
import Firebase


class SocialLoginManager: NSObject {
   
    static let shared = SocialLoginManager()
    
    var viewController : UIViewController = UIViewController()
    
    var locationname : String = ""
    var selectLat : Double = 0.0
    var selectLong : Double = 0.0
    
    var onGetSuccessData: ((_ data : [String:Any])-> (Void))?
    
    func loginWithApple(){
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self.viewController as? ASAuthorizationControllerPresentationContextProviding
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
   
    func loginWithFacebook() {
        
        LoginManager().logOut()
        
        LoginManager().logIn(permissions: ["public_profile","email"], from: viewController) {[unowned self] (result, error) in
            guard error == nil else {
                //self.viewController.showAlert("", message: error?.localizedDescription ?? "")
                LoginManager().logOut()
                return
            }
            guard let loginResult = result, !loginResult.isCancelled, loginResult.grantedPermissions.contains("email") else {
                LoginManager().logOut()
                return
            }
            GraphRequest(graphPath:"me", parameters: ["fields":"id, email, first_name, last_name, picture"]).start(completionHandler: {[unowned self] (connection, result, error) in
                guard let dic = result as? [String:Any], error == nil else {
                    print(error?.localizedDescription ?? "")
                    return
                }
                print(dic)
                self.loginFacebookUser(with: dic)
            })
        }
    }
    
    func loginFacebookUser(with responseDic: [String:Any]){
        let firstName = responseDic["first_name"] as? String ?? ""
        let lastName = responseDic["last_name"] as? String ?? ""
        let email = responseDic["email"] as? String ?? ""
        let pictureDic = responseDic["picture"] as? [String:Any] ?? [:]
        let dataDic = pictureDic["data"] as? [String:Any] ?? [:]
        let profileURL = dataDic["url"] as? String ?? ""
        let mob = responseDic["mobile"] as? String ?? ""
        guard let facebookId = responseDic["id"] as? String else {
            self.viewController.showAlert("", message: kFbIdNotFound)
            return
        }
        guard let emailId = responseDic["email"] as? String else {
            
            let alert = UIAlertController(title: "", message: "Enter Email id", preferredStyle: .alert)

            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.placeholder = "Email"
            }

            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0]
                
                if textField?.text != "" {
                   
                    if let profileImage = dataDic["url"] as? String, let imageURL = URL(string: profileImage) {
                        let data = NSData(contentsOf:imageURL)
                        let image = UIImage(data: data! as Data)
                        self.callLogin_SocialAPI(imageName:profileImage, firstname: firstName, lastname: lastName, email: (textField?.text)!, authProvider: "facebook", socialuserid: facebookId, mobile: mob)
                    }
                    else{
                        self.callLogin_SocialAPI(firstname: firstName, lastname: lastName, email: email, authProvider: "facebook", socialuserid: facebookId, mobile: mob)
                    }
                    
                }
                else {
                    self.viewController.showAlert("", message: "Enter your email for verification")
                }
                
                
            }))

            // 4. Present the alert.
            self.viewController.present(alert, animated: true, completion: nil)
            
            return
        }
          
    }
    
    func loginWithGoogle(){
        GIDSignIn.sharedInstance()?.presentingViewController = self.viewController
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
}



extension SocialLoginManager : GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let auth = user.authentication else { return }
        
        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
        Auth.auth().signIn(with: credentials) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {

                print("Login Successful.")
                //This is where you should add the functionality of successful login
                //i.e. dismissing this view or push the home view controller etc

                let profiledata = authResult?.additionalUserInfo?.profile
                print(authResult?.additionalUserInfo?.profile as Any)
                let firstname = profiledata?["given_name"] as? String ?? ""
                let lastname = profiledata?["family_name"] as? String ?? ""
                let email = authResult?.user.email ?? ""
                let id = authResult?.user.uid ?? ""
                let mobile = profiledata?["mobile"] as? String ?? ""
                if let profileImage = profiledata?["picture"] as? String, let imageURL = URL(string: profileImage) {
                    
                    self.callLogin_SocialAPI(imageName:profileImage,firstname: firstname, lastname: lastname, email: email, authProvider: "google", socialuserid: id, mobile: mobile)
                }
                else{
                    
                    self.callLogin_SocialAPI(firstname: firstname, lastname: lastname, email: email, authProvider: "google", socialuserid: id, mobile: mobile)
                }
            }
        }
    }
    
}

// API Calling
extension SocialLoginManager {
    
    func callLogin_SocialAPI(imageName:String = "", firstname : String,lastname : String,email : String, authProvider : String, socialuserid : String,mobile:String) {
        
        let param : NSMutableDictionary = ["firstName": firstname,
                                           "lastName": lastname,
                                           "email": email,
                                           "mobile":mobile,
                                           "profile_image": imageName,
                                           "social_id": socialuserid,
                                           "social_type": authProvider]
        
        HttpRequestManager.requestWithDefaultCalling(service: kSocialLogin_URL, parameters: param, showLoader : true) { (response, error) in
            if error != nil
            {
                showMessageWithRetry(RetryMessage,3, buttonTapHandler: { _ in
                    hideBanner()
                    self.callLogin_SocialAPI(firstname: firstname, lastname: lastname, email: email, authProvider: authProvider, socialuserid: socialuserid, mobile: mobile)
                })
                return
            }
            else {
                if response?.value(forKey: kStatus) as? String ?? "" == kStatusSuccess {
                    
                    let data = response?.value(forKey: "user_data") as! NSMutableDictionary
                    
                    let token = response?.value(forKey: "api_token") as? String ?? ""
                    
                    let model : ModelUserData = ModelUserData(fromDictionary: data as! [String : Any])
                    model.apiToken = token
                    model.saveToUserDefaults()
                    
                    let QrData = response?.value(forKey: "qr_code_data") as! NSMutableDictionary
                    
                    let modelQR : ModelQrCodeData = ModelQrCodeData(fromDictionary: QrData as! [String : Any])
                    
                    modelQR.saveToUserDefaults()
                    
                    let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "HomeVC") as! HomeVC
                    self.viewController.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else {
                    showMessage(response?.value(forKey: kMessage) as? String ?? "", themeStyle: .error)
                }
            }
        }
    }
}

@available(iOS 13.0, *)
extension SocialLoginManager: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            
            var name = String()
            if let givenName = fullName?.givenName {
                name = givenName
            }
            if let familyName = fullName?.familyName {
                name  = name + " " +  familyName
            }
            
            let appleUserFirstName = appleIDCredential.fullName?.givenName

            let appleUserLastName = appleIDCredential.fullName?.familyName

            let appleUserEmail = appleIDCredential.email
            
            
            print("First Name : \(appleUserFirstName ?? "")")
            print("Last Name : \(appleUserLastName ?? "")")
            print("Email : \(appleUserEmail ?? "")")
            
            if appleUserFirstName != nil && appleUserLastName != nil && appleUserEmail != nil && userIdentifier != "" {
                let dictionaryExample : [String:Any] = ["appleSigninEmail" : appleUserEmail ?? "",
                                                              "appleSigninFirstName" : appleUserFirstName ?? "",
                                                              "appleSigninLastName" : appleUserLastName ?? "",
                                                              "appleSigninAppUserID" : userIdentifier]
                do {
                    let dataExample : Data = try NSKeyedArchiver.archivedData(withRootObject: dictionaryExample, requiringSecureCoding: false)
                    let status = KeyChain.save(key: "appleSigninUserData", data: dataExample as Data)
                    print("Sigin Status : \(status)")
                    
                    self.callLogin_SocialAPI(imageName:"",firstname: appleUserFirstName ?? "", lastname: appleUserLastName ?? "", email: appleUserEmail ?? "", authProvider: "apple", socialuserid: userIdentifier, mobile: "")
                    
                    
                } catch {
                    
                }
                
            } else {
                if let receivedData = KeyChain.load(key: "appleSigninUserData") {
                    do{
                        let fetchValue = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(receivedData) as? NSDictionary ?? [:]
                        print(fetchValue)
                        let appuserID : String = fetchValue["appleSigninAppUserID"] as? String ?? ""
                        let userFirstName : String = fetchValue["appleSigninFirstName"] as? String ?? ""
                        let userLastName : String = fetchValue["appleSigninLastName"] as? String ?? ""
                        let userEmail : String = fetchValue["appleSigninEmail"] as? String ?? ""
                        
                        self.callLogin_SocialAPI(imageName:"",firstname: userFirstName ?? "", lastname: userLastName ?? "", email: userEmail ?? "", authProvider: "apple", socialuserid: appuserID, mobile: "")
                    }catch{
                        print("Unable to successfully convert NSData to NSDictionary")
                    }
                }
            }
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.viewController.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //print("AppleID Credential failed with error: \(error.localizedDescription)”)
        print("AppleID Credential failed with error: \(error.localizedDescription)")
        
    }
}
   
