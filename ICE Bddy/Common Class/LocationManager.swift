//
//  LocationManager.swift
//
//  Created by Developer on 10/04/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import CoreLocation

var currentLat = Double()
var currentLog = Double()

class LocationHandler: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationHandler()
    let locationManager = CLLocationManager()
    var provideLocationBlock: ((_ locationManager: CLLocationManager,_ location: CLLocation)->(Bool))?
    //var location: CLLocation?
    
    private override init() {
        super.init()
        
        locationManager.requestWhenInUseAuthorization()
    }
    
    func getLocationUpdates(completionHandler: @escaping ((_ locationManager: CLLocationManager,_ location: CLLocation)->(Bool))) {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        provideLocationBlock = completionHandler
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        if abs(location.timestamp.timeIntervalSinceNow) > 20 {
            return
        }
        
        if let block = provideLocationBlock {
            locationManager.delegate = nil
            Constant.location = location
            currentLat = location.coordinate.latitude
            currentLog = location.coordinate.longitude
            
            USERDEFAULTS.set(String(location.coordinate.latitude), forKey: UD_LAT)
            USERDEFAULTS.set(String(location.coordinate.longitude), forKey:UD_LOG)
            
            if block(locationManager,location) {
                 locationManager.stopUpdatingLocation()
            } else {
                locationManager.delegate = self
            }
        }
    }
    
    func getPlacemark(fromLocation location: CLLocation, completionBlock:@escaping ((CLPlacemark?)->())) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                completionBlock(nil)
                return
            }
            if placemarks!.count > 0 {
                if let placemark = placemarks!.first {
                    completionBlock(placemark)
                }
            }
            
        }
    }
    
    class func getPlacemark(fromLocation lcoation: CLLocation, completionBlock:@escaping ((CLPlacemark?)->())) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(lcoation) { (placemarks, error) in
            if error != nil {
                print(error!.localizedDescription)
                completionBlock(nil)
                return
            }
            if placemarks!.count > 0 {
                if let placemark = placemarks!.first {
                    completionBlock(placemark)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            print("")
            if let alertV = kRootViewController?.presentedViewController as? UIAlertController,
                alertV.message == "Location Services access is restricted. In order to get near by property list, please enable Location Services in the Settigs under Privacy, Location Service." {
                alertV.dismiss(animated: true, completion: nil)
            }
            
        case .authorizedWhenInUse :
            print("")
            if let alertV = kRootViewController?.presentedViewController as? UIAlertController,
                alertV.message == "Location Services access is restricted. In order to get near by property list, please enable Location Services in the Settigs under Privacy, Location Service." {
                alertV.dismiss(animated: true, completion: nil)
            }
            
        case .denied,.notDetermined,.restricted :
            print("")
            
        }
    }
}
