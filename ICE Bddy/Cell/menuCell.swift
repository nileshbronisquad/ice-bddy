//
//  menuCell.swift
//  ICE Bddy
//
//  Created by Manan on 02/01/21.
//

import UIKit

class menuCell: UITableViewCell {
    @IBOutlet weak var lblMenuOption: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var menuImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
