//
//  XRaySearchCell.swift
//  writi
//
//  Created by iMac on 08/10/20.
//  Copyright © 2020 MayurMacmini. All rights reserved.
//

import UIKit
import DropDown

class SearchCell: DropDownCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var viewCount: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
